import { Global, Module } from '@nestjs/common';
import { RedisService } from './redis.service';
import { redisClientFactory } from '@shared/modules/redis/redis-client.factory';
import { ConfigModule } from '@nestjs/config';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [redisClientFactory, RedisService],
  exports: [RedisService],
})
export class RedisModule {}
