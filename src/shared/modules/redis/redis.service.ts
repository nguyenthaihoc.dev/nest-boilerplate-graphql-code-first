import { Inject, Injectable, OnModuleDestroy } from '@nestjs/common';
import {
  REDIS_CLIENT,
  RedisClient,
} from '@shared/modules/redis/redis-client.type';

@Injectable()
export class RedisService implements OnModuleDestroy {
  public constructor(
    @Inject(REDIS_CLIENT) private readonly redis: RedisClient,
  ) {}

  getClient(): RedisClient {
    return this.redis;
  }

  async onModuleDestroy() {
    await this.redis.quit();
  }
}
