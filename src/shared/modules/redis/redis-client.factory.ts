import { FactoryProvider } from '@nestjs/common';
import { createClient } from 'redis';
import { RedisClient, REDIS_CLIENT } from './redis-client.type';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';

export const redisClientFactory: FactoryProvider<Promise<RedisClient>> = {
  provide: REDIS_CLIENT,
  inject: [ConfigService],
  useFactory: async (config: ConfigService) => {
    const client = createClient({
      url: config.get<string>(EEnvKey.REDIS_URL),
    });
    await client.connect();
    return client;
  },
};
