import { GraphQLException } from '@nestjs/graphql/dist/exceptions';
import { HttpStatus } from '@nestjs/common';

export class ValidationException extends GraphQLException {
  constructor(message: string) {
    super(message, {
      extensions: {
        http: {
          status: HttpStatus.OK,
        },
        code: 'VALIDATION_ERROR',
      },
    });
  }
}

export class BadRequestException extends GraphQLException {
  constructor(message: string) {
    super(message, {
      extensions: {
        http: {
          status: HttpStatus.OK,
        },
        code: 'BAD_REQUEST',
      },
    });
  }
}

export class NotFoundException extends GraphQLException {
  constructor(message: string) {
    super(message, {
      extensions: {
        http: {
          status: HttpStatus.OK,
        },
        code: 'NOT_FOUND',
      },
    });
  }
}

export class InternalServerException extends GraphQLException {
  constructor(message: string) {
    super(message, {
      extensions: {
        http: {
          status: HttpStatus.OK,
        },
        code: 'INTERNAL_SERVER_ERROR',
      },
    });
  }
}
