import {
  BaseEntity as BaseApiEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export default class BaseEntity extends BaseApiEntity {
  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updatedAt: Date;
}
