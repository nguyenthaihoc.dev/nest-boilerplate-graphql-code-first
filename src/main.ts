import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from '@shared/modules/loggers/logger.service';
import { Security } from '@infra/utils/sercurity.util';
import { useMorgan } from '@infra/utils/morgan.middleware';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';
import { UnknownExceptionsFilter } from '@infra/utils/unknown-exceptions.filter';
import { BodyValidationPipe } from '@infra/pipes/validation.pipe';

async function bootstrap() {
  const loggingService = new LoggerService();
  const app = await NestFactory.create(AppModule, {
    logger: loggingService.getLogger('Loader app'),
  });
  const configService = app.get(ConfigService<any>);
  let loggerCtg = 'Server';
  if (configService.get(EEnvKey.IS_RUN_API) === 'true') {
    app.use(useMorgan(loggingService.logger.access));
    Security(app);
    app.useGlobalFilters(
      new UnknownExceptionsFilter(loggingService, configService),
    );
    app.useGlobalPipes(new BodyValidationPipe());
  } else {
    loggerCtg = 'Crawler';
  }
  const logger = loggingService.getLogger(loggerCtg);
  await app.listen(configService.get<number>(EEnvKey.PORT));
  logger.info(`${loggerCtg} is running on: ${await app.getUrl()}`);
}
bootstrap();
