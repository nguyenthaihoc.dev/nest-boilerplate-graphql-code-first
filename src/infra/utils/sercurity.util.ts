import { json } from 'express';

export function Security(app: any) {
  app.use(json({ limit: '5mb' }));
  app.disable('x-powered-by');
}
