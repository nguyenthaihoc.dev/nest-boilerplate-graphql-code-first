import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
} from '@nestjs/common';

import { LoggerService } from '@shared/modules/loggers/logger.service';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';
import { GraphQLException } from '@nestjs/graphql/dist/exceptions';
import {
  InternalServerException,
  NotFoundException,
  ValidationException,
} from '@shared/exceptions/base.exception';

@Catch()
export class UnknownExceptionsFilter implements ExceptionFilter {
  constructor(
    private readonly loggingService: LoggerService,
    private configService: ConfigService,
  ) {}

  private logger = this.loggingService.getLogger('unknown-exception');

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  catch(exception: unknown, host: ArgumentsHost) {
    if (
      exception instanceof GraphQLException ||
      exception instanceof BadRequestException ||
      exception instanceof ValidationException ||
      exception instanceof NotFoundException
    ) {
      throw exception;
    } else {
      this.logger.error(exception);
      if (this.configService.get(EEnvKey.NODE_ENV) === 'development') {
        throw exception;
      } else {
        throw new InternalServerException('Internal Server Error');
      }
    }
  }
}
