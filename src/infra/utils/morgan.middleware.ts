// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import morgan from 'morgan';

morgan.token('operationGQL', (req) => {
  const { operationName } = req.body;
  return operationName ? `GQL ${operationName}` : '';
});

morgan.format('custom', (tokens, req, res) => {
  const frm = `:remote-addr :operationGQL :status :response-time ms`;
  const fn = morgan.compile(frm);
  return fn(tokens, req, res);
});

export function useMorgan(logger?: any) {
  if (!logger) return morgan('custom');
  return morgan('custom', {
    stream: logger,
  });
}
