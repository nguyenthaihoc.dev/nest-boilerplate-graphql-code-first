/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export enum _SubgraphErrorPolicy_ {
    allow = "allow",
    deny = "deny"
}

export enum Block_orderBy {
    id = "id",
    number = "number",
    timestamp = "timestamp",
    parentHash = "parentHash",
    author = "author",
    difficulty = "difficulty",
    totalDifficulty = "totalDifficulty",
    gasUsed = "gasUsed",
    gasLimit = "gasLimit",
    receiptsRoot = "receiptsRoot",
    transactionsRoot = "transactionsRoot",
    stateRoot = "stateRoot",
    size = "size",
    unclesHash = "unclesHash"
}

export enum OrderDirection {
    asc = "asc",
    desc = "desc"
}

export enum Asset_orderBy {
    id = "id",
    name = "name",
    symbol = "symbol",
    underlyingToken = "underlyingToken",
    underlyingToken__id = "underlyingToken__id",
    underlyingToken__symbol = "underlyingToken__symbol",
    underlyingToken__name = "underlyingToken__name",
    underlyingToken__decimals = "underlyingToken__decimals",
    underlyingToken__price = "underlyingToken__price",
    underlyingToken__priceType = "underlyingToken__priceType",
    underlyingToken__orcale = "underlyingToken__orcale",
    underlyingToken__createdTimestamp = "underlyingToken__createdTimestamp",
    underlyingToken__lastUpdate = "underlyingToken__lastUpdate",
    cash = "cash",
    cashUSD = "cashUSD",
    liability = "liability",
    liabilityUSD = "liabilityUSD",
    totalTradeVolume = "totalTradeVolume",
    totalTradeVolumeUSD = "totalTradeVolumeUSD",
    totalCollectedFee = "totalCollectedFee",
    totalCollectedFeeUSD = "totalCollectedFeeUSD",
    totalSharedFee = "totalSharedFee",
    totalSharedFeeUSD = "totalSharedFeeUSD",
    coverageRatio = "coverageRatio",
    createdTimestamp = "createdTimestamp",
    lastUpdate = "lastUpdate",
    poolAddress = "poolAddress",
    pool = "pool",
    pool__id = "pool__id",
    pool__haircute = "pool__haircute",
    pool__retentionRatio = "pool__retentionRatio",
    pool__lpDividendRatio = "pool__lpDividendRatio",
    pool__lastUpdate = "pool__lastUpdate",
    pool__createdTimestamp = "pool__createdTimestamp",
    pool__createdBlock = "pool__createdBlock",
    masterWombat = "masterWombat",
    masterWombat__id = "masterWombat__id",
    masterWombat__voter = "masterWombat__voter",
    masterWombat__wom = "masterWombat__wom",
    masterWombat__veWom = "masterWombat__veWom",
    pid = "pid",
    rewardRate = "rewardRate",
    tvl = "tvl",
    tvlUSD = "tvlUSD",
    boostedTvl = "boostedTvl",
    boostedTvlUSD = "boostedTvlUSD",
    boostedRatio = "boostedRatio",
    womBaseApr = "womBaseApr",
    avgBoostedApr = "avgBoostedApr",
    voter = "voter",
    voter__id = "voter__id",
    voter__totalAllocPoint = "voter__totalAllocPoint",
    voter__totalWeight = "voter__totalWeight",
    voter__basePartition = "voter__basePartition",
    voter__womPerSec = "voter__womPerSec",
    voter__lastUpdate = "voter__lastUpdate",
    voteWeight = "voteWeight",
    allocPoint = "allocPoint",
    bribe = "bribe",
    bribe__id = "bribe__id",
    bribe__bribeInfoCount = "bribe__bribeInfoCount",
    rewarder = "rewarder",
    rewarder__id = "rewarder__id",
    rewarder__rewardInfoLength = "rewarder__rewardInfoLength",
    totalBonusTokenApr = "totalBonusTokenApr",
    totalBoostedBonusTokenApr = "totalBoostedBonusTokenApr",
    isPaused = "isPaused"
}

export enum AssetDayData_orderBy {
    id = "id",
    asset = "asset",
    asset__id = "asset__id",
    asset__name = "asset__name",
    asset__symbol = "asset__symbol",
    asset__cash = "asset__cash",
    asset__cashUSD = "asset__cashUSD",
    asset__liability = "asset__liability",
    asset__liabilityUSD = "asset__liabilityUSD",
    asset__totalTradeVolume = "asset__totalTradeVolume",
    asset__totalTradeVolumeUSD = "asset__totalTradeVolumeUSD",
    asset__totalCollectedFee = "asset__totalCollectedFee",
    asset__totalCollectedFeeUSD = "asset__totalCollectedFeeUSD",
    asset__totalSharedFee = "asset__totalSharedFee",
    asset__totalSharedFeeUSD = "asset__totalSharedFeeUSD",
    asset__coverageRatio = "asset__coverageRatio",
    asset__createdTimestamp = "asset__createdTimestamp",
    asset__lastUpdate = "asset__lastUpdate",
    asset__poolAddress = "asset__poolAddress",
    asset__pid = "asset__pid",
    asset__rewardRate = "asset__rewardRate",
    asset__tvl = "asset__tvl",
    asset__tvlUSD = "asset__tvlUSD",
    asset__boostedTvl = "asset__boostedTvl",
    asset__boostedTvlUSD = "asset__boostedTvlUSD",
    asset__boostedRatio = "asset__boostedRatio",
    asset__womBaseApr = "asset__womBaseApr",
    asset__avgBoostedApr = "asset__avgBoostedApr",
    asset__voteWeight = "asset__voteWeight",
    asset__allocPoint = "asset__allocPoint",
    asset__totalBonusTokenApr = "asset__totalBonusTokenApr",
    asset__totalBoostedBonusTokenApr = "asset__totalBoostedBonusTokenApr",
    asset__isPaused = "asset__isPaused",
    dayID = "dayID",
    date = "date",
    dailyCash = "dailyCash",
    dailyCashUSD = "dailyCashUSD",
    dailyLiability = "dailyLiability",
    dailyLiabilityUSD = "dailyLiabilityUSD",
    dailyTradeVolume = "dailyTradeVolume",
    dailyTradeVolumeUSD = "dailyTradeVolumeUSD",
    dailyCollectedFee = "dailyCollectedFee",
    dailyCollectedFeeUSD = "dailyCollectedFeeUSD",
    dailySharedFee = "dailySharedFee",
    dailySharedFeeUSD = "dailySharedFeeUSD",
    dailyCovRatioSnapshot = "dailyCovRatioSnapshot",
    dailyBaseApr = "dailyBaseApr",
    dailyBonusApr = "dailyBonusApr",
    dailyAvgBoostedApr = "dailyAvgBoostedApr",
    dailyBoostedTvlUSD = "dailyBoostedTvlUSD",
    lastUpdate = "lastUpdate",
    blockNumber = "blockNumber"
}

export enum BoostedRewarder_orderBy {
    id = "id",
    asset = "asset",
    asset__id = "asset__id",
    asset__name = "asset__name",
    asset__symbol = "asset__symbol",
    asset__cash = "asset__cash",
    asset__cashUSD = "asset__cashUSD",
    asset__liability = "asset__liability",
    asset__liabilityUSD = "asset__liabilityUSD",
    asset__totalTradeVolume = "asset__totalTradeVolume",
    asset__totalTradeVolumeUSD = "asset__totalTradeVolumeUSD",
    asset__totalCollectedFee = "asset__totalCollectedFee",
    asset__totalCollectedFeeUSD = "asset__totalCollectedFeeUSD",
    asset__totalSharedFee = "asset__totalSharedFee",
    asset__totalSharedFeeUSD = "asset__totalSharedFeeUSD",
    asset__coverageRatio = "asset__coverageRatio",
    asset__createdTimestamp = "asset__createdTimestamp",
    asset__lastUpdate = "asset__lastUpdate",
    asset__poolAddress = "asset__poolAddress",
    asset__pid = "asset__pid",
    asset__rewardRate = "asset__rewardRate",
    asset__tvl = "asset__tvl",
    asset__tvlUSD = "asset__tvlUSD",
    asset__boostedTvl = "asset__boostedTvl",
    asset__boostedTvlUSD = "asset__boostedTvlUSD",
    asset__boostedRatio = "asset__boostedRatio",
    asset__womBaseApr = "asset__womBaseApr",
    asset__avgBoostedApr = "asset__avgBoostedApr",
    asset__voteWeight = "asset__voteWeight",
    asset__allocPoint = "asset__allocPoint",
    asset__totalBonusTokenApr = "asset__totalBonusTokenApr",
    asset__totalBoostedBonusTokenApr = "asset__totalBoostedBonusTokenApr",
    asset__isPaused = "asset__isPaused",
    rewardInfos = "rewardInfos",
    rewardLength = "rewardLength"
}

export enum BoostedRewardInfo_orderBy {
    id = "id",
    rewardToken = "rewardToken",
    rewardToken__id = "rewardToken__id",
    rewardToken__symbol = "rewardToken__symbol",
    rewardToken__name = "rewardToken__name",
    rewardToken__decimals = "rewardToken__decimals",
    rewardToken__price = "rewardToken__price",
    rewardToken__priceType = "rewardToken__priceType",
    rewardToken__orcale = "rewardToken__orcale",
    rewardToken__createdTimestamp = "rewardToken__createdTimestamp",
    rewardToken__lastUpdate = "rewardToken__lastUpdate",
    tokenPerSec = "tokenPerSec",
    apr = "apr"
}

export enum Bribe_orderBy {
    id = "id",
    lpToken = "lpToken",
    lpToken__id = "lpToken__id",
    lpToken__name = "lpToken__name",
    lpToken__symbol = "lpToken__symbol",
    lpToken__cash = "lpToken__cash",
    lpToken__cashUSD = "lpToken__cashUSD",
    lpToken__liability = "lpToken__liability",
    lpToken__liabilityUSD = "lpToken__liabilityUSD",
    lpToken__totalTradeVolume = "lpToken__totalTradeVolume",
    lpToken__totalTradeVolumeUSD = "lpToken__totalTradeVolumeUSD",
    lpToken__totalCollectedFee = "lpToken__totalCollectedFee",
    lpToken__totalCollectedFeeUSD = "lpToken__totalCollectedFeeUSD",
    lpToken__totalSharedFee = "lpToken__totalSharedFee",
    lpToken__totalSharedFeeUSD = "lpToken__totalSharedFeeUSD",
    lpToken__coverageRatio = "lpToken__coverageRatio",
    lpToken__createdTimestamp = "lpToken__createdTimestamp",
    lpToken__lastUpdate = "lpToken__lastUpdate",
    lpToken__poolAddress = "lpToken__poolAddress",
    lpToken__pid = "lpToken__pid",
    lpToken__rewardRate = "lpToken__rewardRate",
    lpToken__tvl = "lpToken__tvl",
    lpToken__tvlUSD = "lpToken__tvlUSD",
    lpToken__boostedTvl = "lpToken__boostedTvl",
    lpToken__boostedTvlUSD = "lpToken__boostedTvlUSD",
    lpToken__boostedRatio = "lpToken__boostedRatio",
    lpToken__womBaseApr = "lpToken__womBaseApr",
    lpToken__avgBoostedApr = "lpToken__avgBoostedApr",
    lpToken__voteWeight = "lpToken__voteWeight",
    lpToken__allocPoint = "lpToken__allocPoint",
    lpToken__totalBonusTokenApr = "lpToken__totalBonusTokenApr",
    lpToken__totalBoostedBonusTokenApr = "lpToken__totalBoostedBonusTokenApr",
    lpToken__isPaused = "lpToken__isPaused",
    bribeInfoCount = "bribeInfoCount",
    bribeInfos = "bribeInfos"
}

export enum BribeInfo_orderBy {
    id = "id",
    rewardToken = "rewardToken",
    rewardToken__id = "rewardToken__id",
    rewardToken__symbol = "rewardToken__symbol",
    rewardToken__name = "rewardToken__name",
    rewardToken__decimals = "rewardToken__decimals",
    rewardToken__price = "rewardToken__price",
    rewardToken__priceType = "rewardToken__priceType",
    rewardToken__orcale = "rewardToken__orcale",
    rewardToken__createdTimestamp = "rewardToken__createdTimestamp",
    rewardToken__lastUpdate = "rewardToken__lastUpdate",
    tokenPerSec = "tokenPerSec",
    distributedAmount = "distributedAmount",
    distributedAmountUSD = "distributedAmountUSD",
    lastUpdate = "lastUpdate"
}

export enum Burn_orderBy {
    id = "id",
    sender = "sender",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum Deposit_orderBy {
    id = "id",
    from = "from",
    to = "to",
    sentFrom = "sentFrom",
    sentTo = "sentTo",
    token = "token",
    token__id = "token__id",
    token__symbol = "token__symbol",
    token__name = "token__name",
    token__decimals = "token__decimals",
    token__price = "token__price",
    token__priceType = "token__priceType",
    token__orcale = "token__orcale",
    token__createdTimestamp = "token__createdTimestamp",
    token__lastUpdate = "token__lastUpdate",
    asset = "asset",
    asset__id = "asset__id",
    asset__name = "asset__name",
    asset__symbol = "asset__symbol",
    asset__cash = "asset__cash",
    asset__cashUSD = "asset__cashUSD",
    asset__liability = "asset__liability",
    asset__liabilityUSD = "asset__liabilityUSD",
    asset__totalTradeVolume = "asset__totalTradeVolume",
    asset__totalTradeVolumeUSD = "asset__totalTradeVolumeUSD",
    asset__totalCollectedFee = "asset__totalCollectedFee",
    asset__totalCollectedFeeUSD = "asset__totalCollectedFeeUSD",
    asset__totalSharedFee = "asset__totalSharedFee",
    asset__totalSharedFeeUSD = "asset__totalSharedFeeUSD",
    asset__coverageRatio = "asset__coverageRatio",
    asset__createdTimestamp = "asset__createdTimestamp",
    asset__lastUpdate = "asset__lastUpdate",
    asset__poolAddress = "asset__poolAddress",
    asset__pid = "asset__pid",
    asset__rewardRate = "asset__rewardRate",
    asset__tvl = "asset__tvl",
    asset__tvlUSD = "asset__tvlUSD",
    asset__boostedTvl = "asset__boostedTvl",
    asset__boostedTvlUSD = "asset__boostedTvlUSD",
    asset__boostedRatio = "asset__boostedRatio",
    asset__womBaseApr = "asset__womBaseApr",
    asset__avgBoostedApr = "asset__avgBoostedApr",
    asset__voteWeight = "asset__voteWeight",
    asset__allocPoint = "asset__allocPoint",
    asset__totalBonusTokenApr = "asset__totalBonusTokenApr",
    asset__totalBoostedBonusTokenApr = "asset__totalBoostedBonusTokenApr",
    asset__isPaused = "asset__isPaused",
    amount = "amount",
    amountUSD = "amountUSD",
    liquidity = "liquidity",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum Enter_orderBy {
    id = "id",
    sender = "sender",
    unlockTime = "unlockTime",
    veWomAmount = "veWomAmount",
    womAmount = "womAmount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum Exit_orderBy {
    id = "id",
    sender = "sender",
    unlockTime = "unlockTime",
    veWomAmount = "veWomAmount",
    womAmount = "womAmount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum LiquidityPosition_orderBy {
    id = "id",
    pid = "pid",
    tvl = "tvl",
    tvlUSD = "tvlUSD",
    lastUpdate = "lastUpdate"
}

export enum Lock_orderBy {
    id = "id",
    sender = "sender",
    enterTime = "enterTime",
    unlockTime = "unlockTime",
    duration = "duration",
    womAmount = "womAmount",
    veWomAmount = "veWomAmount"
}

export enum MasterWombat_orderBy {
    id = "id",
    voter = "voter",
    wom = "wom",
    veWom = "veWom"
}

export enum Mint_orderBy {
    id = "id",
    sender = "sender",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum MwDeposit_orderBy {
    id = "id",
    from = "from",
    sender = "sender",
    pid = "pid",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum MwDepositFor_orderBy {
    id = "id",
    from = "from",
    sender = "sender",
    pid = "pid",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum MwHarvest_orderBy {
    id = "id",
    from = "from",
    sender = "sender",
    pid = "pid",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum MwWithdraw_orderBy {
    id = "id",
    from = "from",
    sender = "sender",
    pid = "pid",
    amount = "amount",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum Network {
    ARBITRUM_ONE = "ARBITRUM_ONE",
    ARWEAVE_MAINNET = "ARWEAVE_MAINNET",
    AURORA = "AURORA",
    AVALANCHE = "AVALANCHE",
    BOBA = "BOBA",
    BSC = "BSC",
    CHAPEL = "CHAPEL",
    CELO = "CELO",
    COSMOS = "COSMOS",
    CRONOS = "CRONOS",
    MAINNET = "MAINNET",
    FANTOM = "FANTOM",
    FUSE = "FUSE",
    HARMONY = "HARMONY",
    JUNO = "JUNO",
    MUMBAI = "MUMBAI",
    MOONBEAM = "MOONBEAM",
    MOONRIVER = "MOONRIVER",
    NEAR_MAINNET = "NEAR_MAINNET",
    OPTIMISM = "OPTIMISM",
    OSMOSIS = "OSMOSIS",
    MATIC = "MATIC",
    SCROLL = "SCROLL",
    BASE = "BASE",
    XDAI = "XDAI"
}

export enum Pair_orderBy {
    id = "id",
    ticker_id = "ticker_id",
    base = "base",
    target = "target",
    pool_id = "pool_id",
    createdTimestamp = "createdTimestamp"
}

export enum PairSwap_orderBy {
    id = "id",
    pair = "pair",
    pair__id = "pair__id",
    pair__ticker_id = "pair__ticker_id",
    pair__base = "pair__base",
    pair__target = "pair__target",
    pair__createdTimestamp = "pair__createdTimestamp",
    sender = "sender",
    base_amount = "base_amount",
    target_amount = "target_amount",
    base_price = "base_price",
    target_price = "target_price",
    timestamp = "timestamp"
}

export enum Pool_orderBy {
    id = "id",
    assets = "assets",
    haircute = "haircute",
    retentionRatio = "retentionRatio",
    lpDividendRatio = "lpDividendRatio",
    lastUpdate = "lastUpdate",
    createdTimestamp = "createdTimestamp",
    createdBlock = "createdBlock"
}

export enum PriceType {
    CHAINLINK = "CHAINLINK",
    UNI_V2 = "UNI_V2",
    UNI_V3 = "UNI_V3",
    PROTOCOL = "PROTOCOL",
    CUSTOM = "CUSTOM",
    NOT_FOUND = "NOT_FOUND"
}

export enum Protocol_orderBy {
    id = "id",
    version = "version",
    network = "network",
    pools = "pools",
    assets = "assets",
    createdTimestamp = "createdTimestamp",
    totalTradeVolumeUSD = "totalTradeVolumeUSD",
    totalCollectedFeeUSD = "totalCollectedFeeUSD",
    totalSharedFeeUSD = "totalSharedFeeUSD",
    totalCashUSD = "totalCashUSD",
    totalLiabilityUSD = "totalLiabilityUSD",
    totalTvlUSD = "totalTvlUSD",
    totalBoostedTvlUSD = "totalBoostedTvlUSD",
    boostedRatio = "boostedRatio",
    totalBribeRevenueUSD = "totalBribeRevenueUSD",
    lastUpdate = "lastUpdate"
}

export enum ProtocolDayData_orderBy {
    id = "id",
    dayID = "dayID",
    date = "date",
    dailyTradeVolumeUSD = "dailyTradeVolumeUSD",
    dailyCollectedFeeUSD = "dailyCollectedFeeUSD",
    dailySharedFeeUSD = "dailySharedFeeUSD",
    dailyCashUSD = "dailyCashUSD",
    dailyLiabilityUSD = "dailyLiabilityUSD",
    dailyActiveUser = "dailyActiveUser",
    dailyBribeRevenueUSD = "dailyBribeRevenueUSD",
    dailyBribeRevenueUSDSnapshot = "dailyBribeRevenueUSDSnapshot",
    userLists = "userLists",
    lastUpdate = "lastUpdate",
    blockNumber = "blockNumber"
}

export enum Rewarder_orderBy {
    id = "id",
    asset = "asset",
    asset__id = "asset__id",
    asset__name = "asset__name",
    asset__symbol = "asset__symbol",
    asset__cash = "asset__cash",
    asset__cashUSD = "asset__cashUSD",
    asset__liability = "asset__liability",
    asset__liabilityUSD = "asset__liabilityUSD",
    asset__totalTradeVolume = "asset__totalTradeVolume",
    asset__totalTradeVolumeUSD = "asset__totalTradeVolumeUSD",
    asset__totalCollectedFee = "asset__totalCollectedFee",
    asset__totalCollectedFeeUSD = "asset__totalCollectedFeeUSD",
    asset__totalSharedFee = "asset__totalSharedFee",
    asset__totalSharedFeeUSD = "asset__totalSharedFeeUSD",
    asset__coverageRatio = "asset__coverageRatio",
    asset__createdTimestamp = "asset__createdTimestamp",
    asset__lastUpdate = "asset__lastUpdate",
    asset__poolAddress = "asset__poolAddress",
    asset__pid = "asset__pid",
    asset__rewardRate = "asset__rewardRate",
    asset__tvl = "asset__tvl",
    asset__tvlUSD = "asset__tvlUSD",
    asset__boostedTvl = "asset__boostedTvl",
    asset__boostedTvlUSD = "asset__boostedTvlUSD",
    asset__boostedRatio = "asset__boostedRatio",
    asset__womBaseApr = "asset__womBaseApr",
    asset__avgBoostedApr = "asset__avgBoostedApr",
    asset__voteWeight = "asset__voteWeight",
    asset__allocPoint = "asset__allocPoint",
    asset__totalBonusTokenApr = "asset__totalBonusTokenApr",
    asset__totalBoostedBonusTokenApr = "asset__totalBoostedBonusTokenApr",
    asset__isPaused = "asset__isPaused",
    rewardInfo = "rewardInfo",
    rewardInfoLength = "rewardInfoLength"
}

export enum RewardInfo_orderBy {
    id = "id",
    rewardToken = "rewardToken",
    rewardToken__id = "rewardToken__id",
    rewardToken__symbol = "rewardToken__symbol",
    rewardToken__name = "rewardToken__name",
    rewardToken__decimals = "rewardToken__decimals",
    rewardToken__price = "rewardToken__price",
    rewardToken__priceType = "rewardToken__priceType",
    rewardToken__orcale = "rewardToken__orcale",
    rewardToken__createdTimestamp = "rewardToken__createdTimestamp",
    rewardToken__lastUpdate = "rewardToken__lastUpdate",
    tokenPerSec = "tokenPerSec",
    apr = "apr"
}

export enum Swap_orderBy {
    id = "id",
    from = "from",
    to = "to",
    sentFrom = "sentFrom",
    sentTo = "sentTo",
    fromToken = "fromToken",
    fromToken__id = "fromToken__id",
    fromToken__symbol = "fromToken__symbol",
    fromToken__name = "fromToken__name",
    fromToken__decimals = "fromToken__decimals",
    fromToken__price = "fromToken__price",
    fromToken__priceType = "fromToken__priceType",
    fromToken__orcale = "fromToken__orcale",
    fromToken__createdTimestamp = "fromToken__createdTimestamp",
    fromToken__lastUpdate = "fromToken__lastUpdate",
    fromTokenFee = "fromTokenFee",
    toToken = "toToken",
    toToken__id = "toToken__id",
    toToken__symbol = "toToken__symbol",
    toToken__name = "toToken__name",
    toToken__decimals = "toToken__decimals",
    toToken__price = "toToken__price",
    toToken__priceType = "toToken__priceType",
    toToken__orcale = "toToken__orcale",
    toToken__createdTimestamp = "toToken__createdTimestamp",
    toToken__lastUpdate = "toToken__lastUpdate",
    toTokenFee = "toTokenFee",
    fromAsset = "fromAsset",
    fromAsset__id = "fromAsset__id",
    fromAsset__name = "fromAsset__name",
    fromAsset__symbol = "fromAsset__symbol",
    fromAsset__cash = "fromAsset__cash",
    fromAsset__cashUSD = "fromAsset__cashUSD",
    fromAsset__liability = "fromAsset__liability",
    fromAsset__liabilityUSD = "fromAsset__liabilityUSD",
    fromAsset__totalTradeVolume = "fromAsset__totalTradeVolume",
    fromAsset__totalTradeVolumeUSD = "fromAsset__totalTradeVolumeUSD",
    fromAsset__totalCollectedFee = "fromAsset__totalCollectedFee",
    fromAsset__totalCollectedFeeUSD = "fromAsset__totalCollectedFeeUSD",
    fromAsset__totalSharedFee = "fromAsset__totalSharedFee",
    fromAsset__totalSharedFeeUSD = "fromAsset__totalSharedFeeUSD",
    fromAsset__coverageRatio = "fromAsset__coverageRatio",
    fromAsset__createdTimestamp = "fromAsset__createdTimestamp",
    fromAsset__lastUpdate = "fromAsset__lastUpdate",
    fromAsset__poolAddress = "fromAsset__poolAddress",
    fromAsset__pid = "fromAsset__pid",
    fromAsset__rewardRate = "fromAsset__rewardRate",
    fromAsset__tvl = "fromAsset__tvl",
    fromAsset__tvlUSD = "fromAsset__tvlUSD",
    fromAsset__boostedTvl = "fromAsset__boostedTvl",
    fromAsset__boostedTvlUSD = "fromAsset__boostedTvlUSD",
    fromAsset__boostedRatio = "fromAsset__boostedRatio",
    fromAsset__womBaseApr = "fromAsset__womBaseApr",
    fromAsset__avgBoostedApr = "fromAsset__avgBoostedApr",
    fromAsset__voteWeight = "fromAsset__voteWeight",
    fromAsset__allocPoint = "fromAsset__allocPoint",
    fromAsset__totalBonusTokenApr = "fromAsset__totalBonusTokenApr",
    fromAsset__totalBoostedBonusTokenApr = "fromAsset__totalBoostedBonusTokenApr",
    fromAsset__isPaused = "fromAsset__isPaused",
    toAsset = "toAsset",
    toAsset__id = "toAsset__id",
    toAsset__name = "toAsset__name",
    toAsset__symbol = "toAsset__symbol",
    toAsset__cash = "toAsset__cash",
    toAsset__cashUSD = "toAsset__cashUSD",
    toAsset__liability = "toAsset__liability",
    toAsset__liabilityUSD = "toAsset__liabilityUSD",
    toAsset__totalTradeVolume = "toAsset__totalTradeVolume",
    toAsset__totalTradeVolumeUSD = "toAsset__totalTradeVolumeUSD",
    toAsset__totalCollectedFee = "toAsset__totalCollectedFee",
    toAsset__totalCollectedFeeUSD = "toAsset__totalCollectedFeeUSD",
    toAsset__totalSharedFee = "toAsset__totalSharedFee",
    toAsset__totalSharedFeeUSD = "toAsset__totalSharedFeeUSD",
    toAsset__coverageRatio = "toAsset__coverageRatio",
    toAsset__createdTimestamp = "toAsset__createdTimestamp",
    toAsset__lastUpdate = "toAsset__lastUpdate",
    toAsset__poolAddress = "toAsset__poolAddress",
    toAsset__pid = "toAsset__pid",
    toAsset__rewardRate = "toAsset__rewardRate",
    toAsset__tvl = "toAsset__tvl",
    toAsset__tvlUSD = "toAsset__tvlUSD",
    toAsset__boostedTvl = "toAsset__boostedTvl",
    toAsset__boostedTvlUSD = "toAsset__boostedTvlUSD",
    toAsset__boostedRatio = "toAsset__boostedRatio",
    toAsset__womBaseApr = "toAsset__womBaseApr",
    toAsset__avgBoostedApr = "toAsset__avgBoostedApr",
    toAsset__voteWeight = "toAsset__voteWeight",
    toAsset__allocPoint = "toAsset__allocPoint",
    toAsset__totalBonusTokenApr = "toAsset__totalBonusTokenApr",
    toAsset__totalBoostedBonusTokenApr = "toAsset__totalBoostedBonusTokenApr",
    toAsset__isPaused = "toAsset__isPaused",
    fromAmount = "fromAmount",
    fromAmountUSD = "fromAmountUSD",
    toAmount = "toAmount",
    toAmountUSD = "toAmountUSD",
    amount = "amount",
    amountUSD = "amountUSD",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum SwapCreditForTokens_orderBy {
    id = "id",
    from = "from",
    to = "to",
    sentTo = "sentTo",
    toToken = "toToken",
    toToken__id = "toToken__id",
    toToken__symbol = "toToken__symbol",
    toToken__name = "toToken__name",
    toToken__decimals = "toToken__decimals",
    toToken__price = "toToken__price",
    toToken__priceType = "toToken__priceType",
    toToken__orcale = "toToken__orcale",
    toToken__createdTimestamp = "toToken__createdTimestamp",
    toToken__lastUpdate = "toToken__lastUpdate",
    toTokenFee = "toTokenFee",
    toAsset = "toAsset",
    toAsset__id = "toAsset__id",
    toAsset__name = "toAsset__name",
    toAsset__symbol = "toAsset__symbol",
    toAsset__cash = "toAsset__cash",
    toAsset__cashUSD = "toAsset__cashUSD",
    toAsset__liability = "toAsset__liability",
    toAsset__liabilityUSD = "toAsset__liabilityUSD",
    toAsset__totalTradeVolume = "toAsset__totalTradeVolume",
    toAsset__totalTradeVolumeUSD = "toAsset__totalTradeVolumeUSD",
    toAsset__totalCollectedFee = "toAsset__totalCollectedFee",
    toAsset__totalCollectedFeeUSD = "toAsset__totalCollectedFeeUSD",
    toAsset__totalSharedFee = "toAsset__totalSharedFee",
    toAsset__totalSharedFeeUSD = "toAsset__totalSharedFeeUSD",
    toAsset__coverageRatio = "toAsset__coverageRatio",
    toAsset__createdTimestamp = "toAsset__createdTimestamp",
    toAsset__lastUpdate = "toAsset__lastUpdate",
    toAsset__poolAddress = "toAsset__poolAddress",
    toAsset__pid = "toAsset__pid",
    toAsset__rewardRate = "toAsset__rewardRate",
    toAsset__tvl = "toAsset__tvl",
    toAsset__tvlUSD = "toAsset__tvlUSD",
    toAsset__boostedTvl = "toAsset__boostedTvl",
    toAsset__boostedTvlUSD = "toAsset__boostedTvlUSD",
    toAsset__boostedRatio = "toAsset__boostedRatio",
    toAsset__womBaseApr = "toAsset__womBaseApr",
    toAsset__avgBoostedApr = "toAsset__avgBoostedApr",
    toAsset__voteWeight = "toAsset__voteWeight",
    toAsset__allocPoint = "toAsset__allocPoint",
    toAsset__totalBonusTokenApr = "toAsset__totalBonusTokenApr",
    toAsset__totalBoostedBonusTokenApr = "toAsset__totalBoostedBonusTokenApr",
    toAsset__isPaused = "toAsset__isPaused",
    toAmount = "toAmount",
    toAmountUSD = "toAmountUSD",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum SwapTokensForCredit_orderBy {
    id = "id",
    from = "from",
    to = "to",
    sentFrom = "sentFrom",
    fromToken = "fromToken",
    fromToken__id = "fromToken__id",
    fromToken__symbol = "fromToken__symbol",
    fromToken__name = "fromToken__name",
    fromToken__decimals = "fromToken__decimals",
    fromToken__price = "fromToken__price",
    fromToken__priceType = "fromToken__priceType",
    fromToken__orcale = "fromToken__orcale",
    fromToken__createdTimestamp = "fromToken__createdTimestamp",
    fromToken__lastUpdate = "fromToken__lastUpdate",
    fromTokenFee = "fromTokenFee",
    fromAsset = "fromAsset",
    fromAsset__id = "fromAsset__id",
    fromAsset__name = "fromAsset__name",
    fromAsset__symbol = "fromAsset__symbol",
    fromAsset__cash = "fromAsset__cash",
    fromAsset__cashUSD = "fromAsset__cashUSD",
    fromAsset__liability = "fromAsset__liability",
    fromAsset__liabilityUSD = "fromAsset__liabilityUSD",
    fromAsset__totalTradeVolume = "fromAsset__totalTradeVolume",
    fromAsset__totalTradeVolumeUSD = "fromAsset__totalTradeVolumeUSD",
    fromAsset__totalCollectedFee = "fromAsset__totalCollectedFee",
    fromAsset__totalCollectedFeeUSD = "fromAsset__totalCollectedFeeUSD",
    fromAsset__totalSharedFee = "fromAsset__totalSharedFee",
    fromAsset__totalSharedFeeUSD = "fromAsset__totalSharedFeeUSD",
    fromAsset__coverageRatio = "fromAsset__coverageRatio",
    fromAsset__createdTimestamp = "fromAsset__createdTimestamp",
    fromAsset__lastUpdate = "fromAsset__lastUpdate",
    fromAsset__poolAddress = "fromAsset__poolAddress",
    fromAsset__pid = "fromAsset__pid",
    fromAsset__rewardRate = "fromAsset__rewardRate",
    fromAsset__tvl = "fromAsset__tvl",
    fromAsset__tvlUSD = "fromAsset__tvlUSD",
    fromAsset__boostedTvl = "fromAsset__boostedTvl",
    fromAsset__boostedTvlUSD = "fromAsset__boostedTvlUSD",
    fromAsset__boostedRatio = "fromAsset__boostedRatio",
    fromAsset__womBaseApr = "fromAsset__womBaseApr",
    fromAsset__avgBoostedApr = "fromAsset__avgBoostedApr",
    fromAsset__voteWeight = "fromAsset__voteWeight",
    fromAsset__allocPoint = "fromAsset__allocPoint",
    fromAsset__totalBonusTokenApr = "fromAsset__totalBonusTokenApr",
    fromAsset__totalBoostedBonusTokenApr = "fromAsset__totalBoostedBonusTokenApr",
    fromAsset__isPaused = "fromAsset__isPaused",
    fromAmount = "fromAmount",
    fromAmountUSD = "fromAmountUSD",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export enum Ticker_orderBy {
    id = "id",
    ticker_id = "ticker_id",
    base_currency = "base_currency",
    target_currency = "target_currency",
    base_volume = "base_volume",
    target_volume = "target_volume",
    last_price = "last_price",
    pool_id = "pool_id",
    last_update = "last_update"
}

export enum Timer_orderBy {
    id = "id",
    currentTimestamp = "currentTimestamp",
    interval = "interval",
    lastUpdate = "lastUpdate"
}

export enum Token_orderBy {
    id = "id",
    symbol = "symbol",
    name = "name",
    decimals = "decimals",
    price = "price",
    priceType = "priceType",
    orcale = "orcale",
    createdTimestamp = "createdTimestamp",
    lastUpdate = "lastUpdate"
}

export enum User_orderBy {
    id = "id",
    stakedWom = "stakedWom",
    womBalance = "womBalance",
    veWomBalance = "veWomBalance",
    lockCount = "lockCount",
    boosted = "boosted",
    totalTvlUSD = "totalTvlUSD",
    locks = "locks",
    lp = "lp",
    lastUpdate = "lastUpdate"
}

export enum VeWom_orderBy {
    id = "id",
    currentTotalSupply = "currentTotalSupply",
    currentTotalWomLocked = "currentTotalWomLocked",
    cumulativeTotalWomLocked = "cumulativeTotalWomLocked",
    cumulativeTotalVeWomMinted = "cumulativeTotalVeWomMinted",
    cumulativeTotalLockedTime = "cumulativeTotalLockedTime",
    lockCount = "lockCount",
    userCount = "userCount",
    avgWomLocked = "avgWomLocked",
    avgVeWomReward = "avgVeWomReward",
    avgLockTime = "avgLockTime",
    lastUpdate = "lastUpdate"
}

export enum VeWomDayData_orderBy {
    id = "id",
    dayID = "dayID",
    date = "date",
    dailyTotalSupply = "dailyTotalSupply",
    dailyMinted = "dailyMinted",
    dailyBurned = "dailyBurned",
    dailyStaked = "dailyStaked",
    dailyUnstaked = "dailyUnstaked",
    lastUpdate = "lastUpdate",
    blockNumber = "blockNumber"
}

export enum Voter_orderBy {
    id = "id",
    totalAllocPoint = "totalAllocPoint",
    totalWeight = "totalWeight",
    basePartition = "basePartition",
    womPerSec = "womPerSec",
    lastUpdate = "lastUpdate"
}

export enum Withdraw_orderBy {
    id = "id",
    from = "from",
    to = "to",
    sentFrom = "sentFrom",
    sentTo = "sentTo",
    token = "token",
    token__id = "token__id",
    token__symbol = "token__symbol",
    token__name = "token__name",
    token__decimals = "token__decimals",
    token__price = "token__price",
    token__priceType = "token__priceType",
    token__orcale = "token__orcale",
    token__createdTimestamp = "token__createdTimestamp",
    token__lastUpdate = "token__lastUpdate",
    asset = "asset",
    asset__id = "asset__id",
    asset__name = "asset__name",
    asset__symbol = "asset__symbol",
    asset__cash = "asset__cash",
    asset__cashUSD = "asset__cashUSD",
    asset__liability = "asset__liability",
    asset__liabilityUSD = "asset__liabilityUSD",
    asset__totalTradeVolume = "asset__totalTradeVolume",
    asset__totalTradeVolumeUSD = "asset__totalTradeVolumeUSD",
    asset__totalCollectedFee = "asset__totalCollectedFee",
    asset__totalCollectedFeeUSD = "asset__totalCollectedFeeUSD",
    asset__totalSharedFee = "asset__totalSharedFee",
    asset__totalSharedFeeUSD = "asset__totalSharedFeeUSD",
    asset__coverageRatio = "asset__coverageRatio",
    asset__createdTimestamp = "asset__createdTimestamp",
    asset__lastUpdate = "asset__lastUpdate",
    asset__poolAddress = "asset__poolAddress",
    asset__pid = "asset__pid",
    asset__rewardRate = "asset__rewardRate",
    asset__tvl = "asset__tvl",
    asset__tvlUSD = "asset__tvlUSD",
    asset__boostedTvl = "asset__boostedTvl",
    asset__boostedTvlUSD = "asset__boostedTvlUSD",
    asset__boostedRatio = "asset__boostedRatio",
    asset__womBaseApr = "asset__womBaseApr",
    asset__avgBoostedApr = "asset__avgBoostedApr",
    asset__voteWeight = "asset__voteWeight",
    asset__allocPoint = "asset__allocPoint",
    asset__totalBonusTokenApr = "asset__totalBonusTokenApr",
    asset__totalBoostedBonusTokenApr = "asset__totalBoostedBonusTokenApr",
    asset__isPaused = "asset__isPaused",
    amount = "amount",
    amountUSD = "amountUSD",
    liquidity = "liquidity",
    timestamp = "timestamp",
    blockNumber = "blockNumber"
}

export class CreateCatInput {
    name?: Nullable<string>;
    age?: Nullable<number>;
}

export class Block_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    number?: Nullable<BigInt>;
    number_not?: Nullable<BigInt>;
    number_gt?: Nullable<BigInt>;
    number_lt?: Nullable<BigInt>;
    number_gte?: Nullable<BigInt>;
    number_lte?: Nullable<BigInt>;
    number_in?: Nullable<BigInt[]>;
    number_not_in?: Nullable<BigInt[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    parentHash?: Nullable<string>;
    parentHash_not?: Nullable<string>;
    parentHash_gt?: Nullable<string>;
    parentHash_lt?: Nullable<string>;
    parentHash_gte?: Nullable<string>;
    parentHash_lte?: Nullable<string>;
    parentHash_in?: Nullable<string[]>;
    parentHash_not_in?: Nullable<string[]>;
    parentHash_contains?: Nullable<string>;
    parentHash_contains_nocase?: Nullable<string>;
    parentHash_not_contains?: Nullable<string>;
    parentHash_not_contains_nocase?: Nullable<string>;
    parentHash_starts_with?: Nullable<string>;
    parentHash_starts_with_nocase?: Nullable<string>;
    parentHash_not_starts_with?: Nullable<string>;
    parentHash_not_starts_with_nocase?: Nullable<string>;
    parentHash_ends_with?: Nullable<string>;
    parentHash_ends_with_nocase?: Nullable<string>;
    parentHash_not_ends_with?: Nullable<string>;
    parentHash_not_ends_with_nocase?: Nullable<string>;
    author?: Nullable<string>;
    author_not?: Nullable<string>;
    author_gt?: Nullable<string>;
    author_lt?: Nullable<string>;
    author_gte?: Nullable<string>;
    author_lte?: Nullable<string>;
    author_in?: Nullable<string[]>;
    author_not_in?: Nullable<string[]>;
    author_contains?: Nullable<string>;
    author_contains_nocase?: Nullable<string>;
    author_not_contains?: Nullable<string>;
    author_not_contains_nocase?: Nullable<string>;
    author_starts_with?: Nullable<string>;
    author_starts_with_nocase?: Nullable<string>;
    author_not_starts_with?: Nullable<string>;
    author_not_starts_with_nocase?: Nullable<string>;
    author_ends_with?: Nullable<string>;
    author_ends_with_nocase?: Nullable<string>;
    author_not_ends_with?: Nullable<string>;
    author_not_ends_with_nocase?: Nullable<string>;
    difficulty?: Nullable<BigInt>;
    difficulty_not?: Nullable<BigInt>;
    difficulty_gt?: Nullable<BigInt>;
    difficulty_lt?: Nullable<BigInt>;
    difficulty_gte?: Nullable<BigInt>;
    difficulty_lte?: Nullable<BigInt>;
    difficulty_in?: Nullable<BigInt[]>;
    difficulty_not_in?: Nullable<BigInt[]>;
    totalDifficulty?: Nullable<BigInt>;
    totalDifficulty_not?: Nullable<BigInt>;
    totalDifficulty_gt?: Nullable<BigInt>;
    totalDifficulty_lt?: Nullable<BigInt>;
    totalDifficulty_gte?: Nullable<BigInt>;
    totalDifficulty_lte?: Nullable<BigInt>;
    totalDifficulty_in?: Nullable<BigInt[]>;
    totalDifficulty_not_in?: Nullable<BigInt[]>;
    gasUsed?: Nullable<BigInt>;
    gasUsed_not?: Nullable<BigInt>;
    gasUsed_gt?: Nullable<BigInt>;
    gasUsed_lt?: Nullable<BigInt>;
    gasUsed_gte?: Nullable<BigInt>;
    gasUsed_lte?: Nullable<BigInt>;
    gasUsed_in?: Nullable<BigInt[]>;
    gasUsed_not_in?: Nullable<BigInt[]>;
    gasLimit?: Nullable<BigInt>;
    gasLimit_not?: Nullable<BigInt>;
    gasLimit_gt?: Nullable<BigInt>;
    gasLimit_lt?: Nullable<BigInt>;
    gasLimit_gte?: Nullable<BigInt>;
    gasLimit_lte?: Nullable<BigInt>;
    gasLimit_in?: Nullable<BigInt[]>;
    gasLimit_not_in?: Nullable<BigInt[]>;
    receiptsRoot?: Nullable<string>;
    receiptsRoot_not?: Nullable<string>;
    receiptsRoot_gt?: Nullable<string>;
    receiptsRoot_lt?: Nullable<string>;
    receiptsRoot_gte?: Nullable<string>;
    receiptsRoot_lte?: Nullable<string>;
    receiptsRoot_in?: Nullable<string[]>;
    receiptsRoot_not_in?: Nullable<string[]>;
    receiptsRoot_contains?: Nullable<string>;
    receiptsRoot_contains_nocase?: Nullable<string>;
    receiptsRoot_not_contains?: Nullable<string>;
    receiptsRoot_not_contains_nocase?: Nullable<string>;
    receiptsRoot_starts_with?: Nullable<string>;
    receiptsRoot_starts_with_nocase?: Nullable<string>;
    receiptsRoot_not_starts_with?: Nullable<string>;
    receiptsRoot_not_starts_with_nocase?: Nullable<string>;
    receiptsRoot_ends_with?: Nullable<string>;
    receiptsRoot_ends_with_nocase?: Nullable<string>;
    receiptsRoot_not_ends_with?: Nullable<string>;
    receiptsRoot_not_ends_with_nocase?: Nullable<string>;
    transactionsRoot?: Nullable<string>;
    transactionsRoot_not?: Nullable<string>;
    transactionsRoot_gt?: Nullable<string>;
    transactionsRoot_lt?: Nullable<string>;
    transactionsRoot_gte?: Nullable<string>;
    transactionsRoot_lte?: Nullable<string>;
    transactionsRoot_in?: Nullable<string[]>;
    transactionsRoot_not_in?: Nullable<string[]>;
    transactionsRoot_contains?: Nullable<string>;
    transactionsRoot_contains_nocase?: Nullable<string>;
    transactionsRoot_not_contains?: Nullable<string>;
    transactionsRoot_not_contains_nocase?: Nullable<string>;
    transactionsRoot_starts_with?: Nullable<string>;
    transactionsRoot_starts_with_nocase?: Nullable<string>;
    transactionsRoot_not_starts_with?: Nullable<string>;
    transactionsRoot_not_starts_with_nocase?: Nullable<string>;
    transactionsRoot_ends_with?: Nullable<string>;
    transactionsRoot_ends_with_nocase?: Nullable<string>;
    transactionsRoot_not_ends_with?: Nullable<string>;
    transactionsRoot_not_ends_with_nocase?: Nullable<string>;
    stateRoot?: Nullable<string>;
    stateRoot_not?: Nullable<string>;
    stateRoot_gt?: Nullable<string>;
    stateRoot_lt?: Nullable<string>;
    stateRoot_gte?: Nullable<string>;
    stateRoot_lte?: Nullable<string>;
    stateRoot_in?: Nullable<string[]>;
    stateRoot_not_in?: Nullable<string[]>;
    stateRoot_contains?: Nullable<string>;
    stateRoot_contains_nocase?: Nullable<string>;
    stateRoot_not_contains?: Nullable<string>;
    stateRoot_not_contains_nocase?: Nullable<string>;
    stateRoot_starts_with?: Nullable<string>;
    stateRoot_starts_with_nocase?: Nullable<string>;
    stateRoot_not_starts_with?: Nullable<string>;
    stateRoot_not_starts_with_nocase?: Nullable<string>;
    stateRoot_ends_with?: Nullable<string>;
    stateRoot_ends_with_nocase?: Nullable<string>;
    stateRoot_not_ends_with?: Nullable<string>;
    stateRoot_not_ends_with_nocase?: Nullable<string>;
    size?: Nullable<BigInt>;
    size_not?: Nullable<BigInt>;
    size_gt?: Nullable<BigInt>;
    size_lt?: Nullable<BigInt>;
    size_gte?: Nullable<BigInt>;
    size_lte?: Nullable<BigInt>;
    size_in?: Nullable<BigInt[]>;
    size_not_in?: Nullable<BigInt[]>;
    unclesHash?: Nullable<string>;
    unclesHash_not?: Nullable<string>;
    unclesHash_gt?: Nullable<string>;
    unclesHash_lt?: Nullable<string>;
    unclesHash_gte?: Nullable<string>;
    unclesHash_lte?: Nullable<string>;
    unclesHash_in?: Nullable<string[]>;
    unclesHash_not_in?: Nullable<string[]>;
    unclesHash_contains?: Nullable<string>;
    unclesHash_contains_nocase?: Nullable<string>;
    unclesHash_not_contains?: Nullable<string>;
    unclesHash_not_contains_nocase?: Nullable<string>;
    unclesHash_starts_with?: Nullable<string>;
    unclesHash_starts_with_nocase?: Nullable<string>;
    unclesHash_not_starts_with?: Nullable<string>;
    unclesHash_not_starts_with_nocase?: Nullable<string>;
    unclesHash_ends_with?: Nullable<string>;
    unclesHash_ends_with_nocase?: Nullable<string>;
    unclesHash_not_ends_with?: Nullable<string>;
    unclesHash_not_ends_with_nocase?: Nullable<string>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Block_filter>[]>;
    or?: Nullable<Nullable<Block_filter>[]>;
}

export class Block_height {
    hash?: Nullable<Bytes>;
    number?: Nullable<number>;
    number_gte?: Nullable<number>;
}

export class BlockChangedFilter {
    number_gte: number;
}

export class Asset_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    name?: Nullable<string>;
    name_not?: Nullable<string>;
    name_gt?: Nullable<string>;
    name_lt?: Nullable<string>;
    name_gte?: Nullable<string>;
    name_lte?: Nullable<string>;
    name_in?: Nullable<string[]>;
    name_not_in?: Nullable<string[]>;
    name_contains?: Nullable<string>;
    name_contains_nocase?: Nullable<string>;
    name_not_contains?: Nullable<string>;
    name_not_contains_nocase?: Nullable<string>;
    name_starts_with?: Nullable<string>;
    name_starts_with_nocase?: Nullable<string>;
    name_not_starts_with?: Nullable<string>;
    name_not_starts_with_nocase?: Nullable<string>;
    name_ends_with?: Nullable<string>;
    name_ends_with_nocase?: Nullable<string>;
    name_not_ends_with?: Nullable<string>;
    name_not_ends_with_nocase?: Nullable<string>;
    symbol?: Nullable<string>;
    symbol_not?: Nullable<string>;
    symbol_gt?: Nullable<string>;
    symbol_lt?: Nullable<string>;
    symbol_gte?: Nullable<string>;
    symbol_lte?: Nullable<string>;
    symbol_in?: Nullable<string[]>;
    symbol_not_in?: Nullable<string[]>;
    symbol_contains?: Nullable<string>;
    symbol_contains_nocase?: Nullable<string>;
    symbol_not_contains?: Nullable<string>;
    symbol_not_contains_nocase?: Nullable<string>;
    symbol_starts_with?: Nullable<string>;
    symbol_starts_with_nocase?: Nullable<string>;
    symbol_not_starts_with?: Nullable<string>;
    symbol_not_starts_with_nocase?: Nullable<string>;
    symbol_ends_with?: Nullable<string>;
    symbol_ends_with_nocase?: Nullable<string>;
    symbol_not_ends_with?: Nullable<string>;
    symbol_not_ends_with_nocase?: Nullable<string>;
    underlyingToken?: Nullable<string>;
    underlyingToken_not?: Nullable<string>;
    underlyingToken_gt?: Nullable<string>;
    underlyingToken_lt?: Nullable<string>;
    underlyingToken_gte?: Nullable<string>;
    underlyingToken_lte?: Nullable<string>;
    underlyingToken_in?: Nullable<string[]>;
    underlyingToken_not_in?: Nullable<string[]>;
    underlyingToken_contains?: Nullable<string>;
    underlyingToken_contains_nocase?: Nullable<string>;
    underlyingToken_not_contains?: Nullable<string>;
    underlyingToken_not_contains_nocase?: Nullable<string>;
    underlyingToken_starts_with?: Nullable<string>;
    underlyingToken_starts_with_nocase?: Nullable<string>;
    underlyingToken_not_starts_with?: Nullable<string>;
    underlyingToken_not_starts_with_nocase?: Nullable<string>;
    underlyingToken_ends_with?: Nullable<string>;
    underlyingToken_ends_with_nocase?: Nullable<string>;
    underlyingToken_not_ends_with?: Nullable<string>;
    underlyingToken_not_ends_with_nocase?: Nullable<string>;
    underlyingToken_?: Nullable<Token_filter>;
    cash?: Nullable<BigDecimal>;
    cash_not?: Nullable<BigDecimal>;
    cash_gt?: Nullable<BigDecimal>;
    cash_lt?: Nullable<BigDecimal>;
    cash_gte?: Nullable<BigDecimal>;
    cash_lte?: Nullable<BigDecimal>;
    cash_in?: Nullable<BigDecimal[]>;
    cash_not_in?: Nullable<BigDecimal[]>;
    cashUSD?: Nullable<BigDecimal>;
    cashUSD_not?: Nullable<BigDecimal>;
    cashUSD_gt?: Nullable<BigDecimal>;
    cashUSD_lt?: Nullable<BigDecimal>;
    cashUSD_gte?: Nullable<BigDecimal>;
    cashUSD_lte?: Nullable<BigDecimal>;
    cashUSD_in?: Nullable<BigDecimal[]>;
    cashUSD_not_in?: Nullable<BigDecimal[]>;
    liability?: Nullable<BigDecimal>;
    liability_not?: Nullable<BigDecimal>;
    liability_gt?: Nullable<BigDecimal>;
    liability_lt?: Nullable<BigDecimal>;
    liability_gte?: Nullable<BigDecimal>;
    liability_lte?: Nullable<BigDecimal>;
    liability_in?: Nullable<BigDecimal[]>;
    liability_not_in?: Nullable<BigDecimal[]>;
    liabilityUSD?: Nullable<BigDecimal>;
    liabilityUSD_not?: Nullable<BigDecimal>;
    liabilityUSD_gt?: Nullable<BigDecimal>;
    liabilityUSD_lt?: Nullable<BigDecimal>;
    liabilityUSD_gte?: Nullable<BigDecimal>;
    liabilityUSD_lte?: Nullable<BigDecimal>;
    liabilityUSD_in?: Nullable<BigDecimal[]>;
    liabilityUSD_not_in?: Nullable<BigDecimal[]>;
    totalTradeVolume?: Nullable<BigDecimal>;
    totalTradeVolume_not?: Nullable<BigDecimal>;
    totalTradeVolume_gt?: Nullable<BigDecimal>;
    totalTradeVolume_lt?: Nullable<BigDecimal>;
    totalTradeVolume_gte?: Nullable<BigDecimal>;
    totalTradeVolume_lte?: Nullable<BigDecimal>;
    totalTradeVolume_in?: Nullable<BigDecimal[]>;
    totalTradeVolume_not_in?: Nullable<BigDecimal[]>;
    totalTradeVolumeUSD?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_not?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_gt?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_lt?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_gte?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_lte?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_in?: Nullable<BigDecimal[]>;
    totalTradeVolumeUSD_not_in?: Nullable<BigDecimal[]>;
    totalCollectedFee?: Nullable<BigDecimal>;
    totalCollectedFee_not?: Nullable<BigDecimal>;
    totalCollectedFee_gt?: Nullable<BigDecimal>;
    totalCollectedFee_lt?: Nullable<BigDecimal>;
    totalCollectedFee_gte?: Nullable<BigDecimal>;
    totalCollectedFee_lte?: Nullable<BigDecimal>;
    totalCollectedFee_in?: Nullable<BigDecimal[]>;
    totalCollectedFee_not_in?: Nullable<BigDecimal[]>;
    totalCollectedFeeUSD?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_not?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_gt?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_lt?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_gte?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_lte?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_in?: Nullable<BigDecimal[]>;
    totalCollectedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    totalSharedFee?: Nullable<BigDecimal>;
    totalSharedFee_not?: Nullable<BigDecimal>;
    totalSharedFee_gt?: Nullable<BigDecimal>;
    totalSharedFee_lt?: Nullable<BigDecimal>;
    totalSharedFee_gte?: Nullable<BigDecimal>;
    totalSharedFee_lte?: Nullable<BigDecimal>;
    totalSharedFee_in?: Nullable<BigDecimal[]>;
    totalSharedFee_not_in?: Nullable<BigDecimal[]>;
    totalSharedFeeUSD?: Nullable<BigDecimal>;
    totalSharedFeeUSD_not?: Nullable<BigDecimal>;
    totalSharedFeeUSD_gt?: Nullable<BigDecimal>;
    totalSharedFeeUSD_lt?: Nullable<BigDecimal>;
    totalSharedFeeUSD_gte?: Nullable<BigDecimal>;
    totalSharedFeeUSD_lte?: Nullable<BigDecimal>;
    totalSharedFeeUSD_in?: Nullable<BigDecimal[]>;
    totalSharedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    coverageRatio?: Nullable<BigDecimal>;
    coverageRatio_not?: Nullable<BigDecimal>;
    coverageRatio_gt?: Nullable<BigDecimal>;
    coverageRatio_lt?: Nullable<BigDecimal>;
    coverageRatio_gte?: Nullable<BigDecimal>;
    coverageRatio_lte?: Nullable<BigDecimal>;
    coverageRatio_in?: Nullable<BigDecimal[]>;
    coverageRatio_not_in?: Nullable<BigDecimal[]>;
    createdTimestamp?: Nullable<BigInt>;
    createdTimestamp_not?: Nullable<BigInt>;
    createdTimestamp_gt?: Nullable<BigInt>;
    createdTimestamp_lt?: Nullable<BigInt>;
    createdTimestamp_gte?: Nullable<BigInt>;
    createdTimestamp_lte?: Nullable<BigInt>;
    createdTimestamp_in?: Nullable<BigInt[]>;
    createdTimestamp_not_in?: Nullable<BigInt[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    poolAddress?: Nullable<string>;
    poolAddress_not?: Nullable<string>;
    poolAddress_gt?: Nullable<string>;
    poolAddress_lt?: Nullable<string>;
    poolAddress_gte?: Nullable<string>;
    poolAddress_lte?: Nullable<string>;
    poolAddress_in?: Nullable<string[]>;
    poolAddress_not_in?: Nullable<string[]>;
    poolAddress_contains?: Nullable<string>;
    poolAddress_contains_nocase?: Nullable<string>;
    poolAddress_not_contains?: Nullable<string>;
    poolAddress_not_contains_nocase?: Nullable<string>;
    poolAddress_starts_with?: Nullable<string>;
    poolAddress_starts_with_nocase?: Nullable<string>;
    poolAddress_not_starts_with?: Nullable<string>;
    poolAddress_not_starts_with_nocase?: Nullable<string>;
    poolAddress_ends_with?: Nullable<string>;
    poolAddress_ends_with_nocase?: Nullable<string>;
    poolAddress_not_ends_with?: Nullable<string>;
    poolAddress_not_ends_with_nocase?: Nullable<string>;
    pool?: Nullable<string>;
    pool_not?: Nullable<string>;
    pool_gt?: Nullable<string>;
    pool_lt?: Nullable<string>;
    pool_gte?: Nullable<string>;
    pool_lte?: Nullable<string>;
    pool_in?: Nullable<string[]>;
    pool_not_in?: Nullable<string[]>;
    pool_contains?: Nullable<string>;
    pool_contains_nocase?: Nullable<string>;
    pool_not_contains?: Nullable<string>;
    pool_not_contains_nocase?: Nullable<string>;
    pool_starts_with?: Nullable<string>;
    pool_starts_with_nocase?: Nullable<string>;
    pool_not_starts_with?: Nullable<string>;
    pool_not_starts_with_nocase?: Nullable<string>;
    pool_ends_with?: Nullable<string>;
    pool_ends_with_nocase?: Nullable<string>;
    pool_not_ends_with?: Nullable<string>;
    pool_not_ends_with_nocase?: Nullable<string>;
    pool_?: Nullable<Pool_filter>;
    masterWombat?: Nullable<string>;
    masterWombat_not?: Nullable<string>;
    masterWombat_gt?: Nullable<string>;
    masterWombat_lt?: Nullable<string>;
    masterWombat_gte?: Nullable<string>;
    masterWombat_lte?: Nullable<string>;
    masterWombat_in?: Nullable<string[]>;
    masterWombat_not_in?: Nullable<string[]>;
    masterWombat_contains?: Nullable<string>;
    masterWombat_contains_nocase?: Nullable<string>;
    masterWombat_not_contains?: Nullable<string>;
    masterWombat_not_contains_nocase?: Nullable<string>;
    masterWombat_starts_with?: Nullable<string>;
    masterWombat_starts_with_nocase?: Nullable<string>;
    masterWombat_not_starts_with?: Nullable<string>;
    masterWombat_not_starts_with_nocase?: Nullable<string>;
    masterWombat_ends_with?: Nullable<string>;
    masterWombat_ends_with_nocase?: Nullable<string>;
    masterWombat_not_ends_with?: Nullable<string>;
    masterWombat_not_ends_with_nocase?: Nullable<string>;
    masterWombat_?: Nullable<MasterWombat_filter>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    rewardRate?: Nullable<BigDecimal>;
    rewardRate_not?: Nullable<BigDecimal>;
    rewardRate_gt?: Nullable<BigDecimal>;
    rewardRate_lt?: Nullable<BigDecimal>;
    rewardRate_gte?: Nullable<BigDecimal>;
    rewardRate_lte?: Nullable<BigDecimal>;
    rewardRate_in?: Nullable<BigDecimal[]>;
    rewardRate_not_in?: Nullable<BigDecimal[]>;
    tvl?: Nullable<BigDecimal>;
    tvl_not?: Nullable<BigDecimal>;
    tvl_gt?: Nullable<BigDecimal>;
    tvl_lt?: Nullable<BigDecimal>;
    tvl_gte?: Nullable<BigDecimal>;
    tvl_lte?: Nullable<BigDecimal>;
    tvl_in?: Nullable<BigDecimal[]>;
    tvl_not_in?: Nullable<BigDecimal[]>;
    tvlUSD?: Nullable<BigDecimal>;
    tvlUSD_not?: Nullable<BigDecimal>;
    tvlUSD_gt?: Nullable<BigDecimal>;
    tvlUSD_lt?: Nullable<BigDecimal>;
    tvlUSD_gte?: Nullable<BigDecimal>;
    tvlUSD_lte?: Nullable<BigDecimal>;
    tvlUSD_in?: Nullable<BigDecimal[]>;
    tvlUSD_not_in?: Nullable<BigDecimal[]>;
    boostedTvl?: Nullable<BigDecimal>;
    boostedTvl_not?: Nullable<BigDecimal>;
    boostedTvl_gt?: Nullable<BigDecimal>;
    boostedTvl_lt?: Nullable<BigDecimal>;
    boostedTvl_gte?: Nullable<BigDecimal>;
    boostedTvl_lte?: Nullable<BigDecimal>;
    boostedTvl_in?: Nullable<BigDecimal[]>;
    boostedTvl_not_in?: Nullable<BigDecimal[]>;
    boostedTvlUSD?: Nullable<BigDecimal>;
    boostedTvlUSD_not?: Nullable<BigDecimal>;
    boostedTvlUSD_gt?: Nullable<BigDecimal>;
    boostedTvlUSD_lt?: Nullable<BigDecimal>;
    boostedTvlUSD_gte?: Nullable<BigDecimal>;
    boostedTvlUSD_lte?: Nullable<BigDecimal>;
    boostedTvlUSD_in?: Nullable<BigDecimal[]>;
    boostedTvlUSD_not_in?: Nullable<BigDecimal[]>;
    boostedRatio?: Nullable<BigDecimal>;
    boostedRatio_not?: Nullable<BigDecimal>;
    boostedRatio_gt?: Nullable<BigDecimal>;
    boostedRatio_lt?: Nullable<BigDecimal>;
    boostedRatio_gte?: Nullable<BigDecimal>;
    boostedRatio_lte?: Nullable<BigDecimal>;
    boostedRatio_in?: Nullable<BigDecimal[]>;
    boostedRatio_not_in?: Nullable<BigDecimal[]>;
    womBaseApr?: Nullable<BigDecimal>;
    womBaseApr_not?: Nullable<BigDecimal>;
    womBaseApr_gt?: Nullable<BigDecimal>;
    womBaseApr_lt?: Nullable<BigDecimal>;
    womBaseApr_gte?: Nullable<BigDecimal>;
    womBaseApr_lte?: Nullable<BigDecimal>;
    womBaseApr_in?: Nullable<BigDecimal[]>;
    womBaseApr_not_in?: Nullable<BigDecimal[]>;
    avgBoostedApr?: Nullable<BigDecimal>;
    avgBoostedApr_not?: Nullable<BigDecimal>;
    avgBoostedApr_gt?: Nullable<BigDecimal>;
    avgBoostedApr_lt?: Nullable<BigDecimal>;
    avgBoostedApr_gte?: Nullable<BigDecimal>;
    avgBoostedApr_lte?: Nullable<BigDecimal>;
    avgBoostedApr_in?: Nullable<BigDecimal[]>;
    avgBoostedApr_not_in?: Nullable<BigDecimal[]>;
    voter?: Nullable<string>;
    voter_not?: Nullable<string>;
    voter_gt?: Nullable<string>;
    voter_lt?: Nullable<string>;
    voter_gte?: Nullable<string>;
    voter_lte?: Nullable<string>;
    voter_in?: Nullable<string[]>;
    voter_not_in?: Nullable<string[]>;
    voter_contains?: Nullable<string>;
    voter_contains_nocase?: Nullable<string>;
    voter_not_contains?: Nullable<string>;
    voter_not_contains_nocase?: Nullable<string>;
    voter_starts_with?: Nullable<string>;
    voter_starts_with_nocase?: Nullable<string>;
    voter_not_starts_with?: Nullable<string>;
    voter_not_starts_with_nocase?: Nullable<string>;
    voter_ends_with?: Nullable<string>;
    voter_ends_with_nocase?: Nullable<string>;
    voter_not_ends_with?: Nullable<string>;
    voter_not_ends_with_nocase?: Nullable<string>;
    voter_?: Nullable<Voter_filter>;
    voteWeight?: Nullable<BigDecimal>;
    voteWeight_not?: Nullable<BigDecimal>;
    voteWeight_gt?: Nullable<BigDecimal>;
    voteWeight_lt?: Nullable<BigDecimal>;
    voteWeight_gte?: Nullable<BigDecimal>;
    voteWeight_lte?: Nullable<BigDecimal>;
    voteWeight_in?: Nullable<BigDecimal[]>;
    voteWeight_not_in?: Nullable<BigDecimal[]>;
    allocPoint?: Nullable<BigDecimal>;
    allocPoint_not?: Nullable<BigDecimal>;
    allocPoint_gt?: Nullable<BigDecimal>;
    allocPoint_lt?: Nullable<BigDecimal>;
    allocPoint_gte?: Nullable<BigDecimal>;
    allocPoint_lte?: Nullable<BigDecimal>;
    allocPoint_in?: Nullable<BigDecimal[]>;
    allocPoint_not_in?: Nullable<BigDecimal[]>;
    bribe?: Nullable<string>;
    bribe_not?: Nullable<string>;
    bribe_gt?: Nullable<string>;
    bribe_lt?: Nullable<string>;
    bribe_gte?: Nullable<string>;
    bribe_lte?: Nullable<string>;
    bribe_in?: Nullable<string[]>;
    bribe_not_in?: Nullable<string[]>;
    bribe_contains?: Nullable<string>;
    bribe_contains_nocase?: Nullable<string>;
    bribe_not_contains?: Nullable<string>;
    bribe_not_contains_nocase?: Nullable<string>;
    bribe_starts_with?: Nullable<string>;
    bribe_starts_with_nocase?: Nullable<string>;
    bribe_not_starts_with?: Nullable<string>;
    bribe_not_starts_with_nocase?: Nullable<string>;
    bribe_ends_with?: Nullable<string>;
    bribe_ends_with_nocase?: Nullable<string>;
    bribe_not_ends_with?: Nullable<string>;
    bribe_not_ends_with_nocase?: Nullable<string>;
    bribe_?: Nullable<Bribe_filter>;
    rewarder?: Nullable<string>;
    rewarder_not?: Nullable<string>;
    rewarder_gt?: Nullable<string>;
    rewarder_lt?: Nullable<string>;
    rewarder_gte?: Nullable<string>;
    rewarder_lte?: Nullable<string>;
    rewarder_in?: Nullable<string[]>;
    rewarder_not_in?: Nullable<string[]>;
    rewarder_contains?: Nullable<string>;
    rewarder_contains_nocase?: Nullable<string>;
    rewarder_not_contains?: Nullable<string>;
    rewarder_not_contains_nocase?: Nullable<string>;
    rewarder_starts_with?: Nullable<string>;
    rewarder_starts_with_nocase?: Nullable<string>;
    rewarder_not_starts_with?: Nullable<string>;
    rewarder_not_starts_with_nocase?: Nullable<string>;
    rewarder_ends_with?: Nullable<string>;
    rewarder_ends_with_nocase?: Nullable<string>;
    rewarder_not_ends_with?: Nullable<string>;
    rewarder_not_ends_with_nocase?: Nullable<string>;
    rewarder_?: Nullable<Rewarder_filter>;
    totalBonusTokenApr?: Nullable<BigDecimal>;
    totalBonusTokenApr_not?: Nullable<BigDecimal>;
    totalBonusTokenApr_gt?: Nullable<BigDecimal>;
    totalBonusTokenApr_lt?: Nullable<BigDecimal>;
    totalBonusTokenApr_gte?: Nullable<BigDecimal>;
    totalBonusTokenApr_lte?: Nullable<BigDecimal>;
    totalBonusTokenApr_in?: Nullable<BigDecimal[]>;
    totalBonusTokenApr_not_in?: Nullable<BigDecimal[]>;
    totalBoostedBonusTokenApr?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_not?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_gt?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_lt?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_gte?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_lte?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr_in?: Nullable<BigDecimal[]>;
    totalBoostedBonusTokenApr_not_in?: Nullable<BigDecimal[]>;
    isPaused?: Nullable<boolean>;
    isPaused_not?: Nullable<boolean>;
    isPaused_in?: Nullable<boolean[]>;
    isPaused_not_in?: Nullable<boolean[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Asset_filter>[]>;
    or?: Nullable<Nullable<Asset_filter>[]>;
}

export class AssetDayData_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    asset?: Nullable<string>;
    asset_not?: Nullable<string>;
    asset_gt?: Nullable<string>;
    asset_lt?: Nullable<string>;
    asset_gte?: Nullable<string>;
    asset_lte?: Nullable<string>;
    asset_in?: Nullable<string[]>;
    asset_not_in?: Nullable<string[]>;
    asset_contains?: Nullable<string>;
    asset_contains_nocase?: Nullable<string>;
    asset_not_contains?: Nullable<string>;
    asset_not_contains_nocase?: Nullable<string>;
    asset_starts_with?: Nullable<string>;
    asset_starts_with_nocase?: Nullable<string>;
    asset_not_starts_with?: Nullable<string>;
    asset_not_starts_with_nocase?: Nullable<string>;
    asset_ends_with?: Nullable<string>;
    asset_ends_with_nocase?: Nullable<string>;
    asset_not_ends_with?: Nullable<string>;
    asset_not_ends_with_nocase?: Nullable<string>;
    asset_?: Nullable<Asset_filter>;
    dayID?: Nullable<BigInt>;
    dayID_not?: Nullable<BigInt>;
    dayID_gt?: Nullable<BigInt>;
    dayID_lt?: Nullable<BigInt>;
    dayID_gte?: Nullable<BigInt>;
    dayID_lte?: Nullable<BigInt>;
    dayID_in?: Nullable<BigInt[]>;
    dayID_not_in?: Nullable<BigInt[]>;
    date?: Nullable<string>;
    date_not?: Nullable<string>;
    date_gt?: Nullable<string>;
    date_lt?: Nullable<string>;
    date_gte?: Nullable<string>;
    date_lte?: Nullable<string>;
    date_in?: Nullable<string[]>;
    date_not_in?: Nullable<string[]>;
    date_contains?: Nullable<string>;
    date_contains_nocase?: Nullable<string>;
    date_not_contains?: Nullable<string>;
    date_not_contains_nocase?: Nullable<string>;
    date_starts_with?: Nullable<string>;
    date_starts_with_nocase?: Nullable<string>;
    date_not_starts_with?: Nullable<string>;
    date_not_starts_with_nocase?: Nullable<string>;
    date_ends_with?: Nullable<string>;
    date_ends_with_nocase?: Nullable<string>;
    date_not_ends_with?: Nullable<string>;
    date_not_ends_with_nocase?: Nullable<string>;
    dailyCash?: Nullable<BigDecimal>;
    dailyCash_not?: Nullable<BigDecimal>;
    dailyCash_gt?: Nullable<BigDecimal>;
    dailyCash_lt?: Nullable<BigDecimal>;
    dailyCash_gte?: Nullable<BigDecimal>;
    dailyCash_lte?: Nullable<BigDecimal>;
    dailyCash_in?: Nullable<BigDecimal[]>;
    dailyCash_not_in?: Nullable<BigDecimal[]>;
    dailyCashUSD?: Nullable<BigDecimal>;
    dailyCashUSD_not?: Nullable<BigDecimal>;
    dailyCashUSD_gt?: Nullable<BigDecimal>;
    dailyCashUSD_lt?: Nullable<BigDecimal>;
    dailyCashUSD_gte?: Nullable<BigDecimal>;
    dailyCashUSD_lte?: Nullable<BigDecimal>;
    dailyCashUSD_in?: Nullable<BigDecimal[]>;
    dailyCashUSD_not_in?: Nullable<BigDecimal[]>;
    dailyLiability?: Nullable<BigDecimal>;
    dailyLiability_not?: Nullable<BigDecimal>;
    dailyLiability_gt?: Nullable<BigDecimal>;
    dailyLiability_lt?: Nullable<BigDecimal>;
    dailyLiability_gte?: Nullable<BigDecimal>;
    dailyLiability_lte?: Nullable<BigDecimal>;
    dailyLiability_in?: Nullable<BigDecimal[]>;
    dailyLiability_not_in?: Nullable<BigDecimal[]>;
    dailyLiabilityUSD?: Nullable<BigDecimal>;
    dailyLiabilityUSD_not?: Nullable<BigDecimal>;
    dailyLiabilityUSD_gt?: Nullable<BigDecimal>;
    dailyLiabilityUSD_lt?: Nullable<BigDecimal>;
    dailyLiabilityUSD_gte?: Nullable<BigDecimal>;
    dailyLiabilityUSD_lte?: Nullable<BigDecimal>;
    dailyLiabilityUSD_in?: Nullable<BigDecimal[]>;
    dailyLiabilityUSD_not_in?: Nullable<BigDecimal[]>;
    dailyTradeVolume?: Nullable<BigDecimal>;
    dailyTradeVolume_not?: Nullable<BigDecimal>;
    dailyTradeVolume_gt?: Nullable<BigDecimal>;
    dailyTradeVolume_lt?: Nullable<BigDecimal>;
    dailyTradeVolume_gte?: Nullable<BigDecimal>;
    dailyTradeVolume_lte?: Nullable<BigDecimal>;
    dailyTradeVolume_in?: Nullable<BigDecimal[]>;
    dailyTradeVolume_not_in?: Nullable<BigDecimal[]>;
    dailyTradeVolumeUSD?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_not?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_gt?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_lt?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_gte?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_lte?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_in?: Nullable<BigDecimal[]>;
    dailyTradeVolumeUSD_not_in?: Nullable<BigDecimal[]>;
    dailyCollectedFee?: Nullable<BigDecimal>;
    dailyCollectedFee_not?: Nullable<BigDecimal>;
    dailyCollectedFee_gt?: Nullable<BigDecimal>;
    dailyCollectedFee_lt?: Nullable<BigDecimal>;
    dailyCollectedFee_gte?: Nullable<BigDecimal>;
    dailyCollectedFee_lte?: Nullable<BigDecimal>;
    dailyCollectedFee_in?: Nullable<BigDecimal[]>;
    dailyCollectedFee_not_in?: Nullable<BigDecimal[]>;
    dailyCollectedFeeUSD?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_not?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_gt?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_lt?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_gte?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_lte?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_in?: Nullable<BigDecimal[]>;
    dailyCollectedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    dailySharedFee?: Nullable<BigDecimal>;
    dailySharedFee_not?: Nullable<BigDecimal>;
    dailySharedFee_gt?: Nullable<BigDecimal>;
    dailySharedFee_lt?: Nullable<BigDecimal>;
    dailySharedFee_gte?: Nullable<BigDecimal>;
    dailySharedFee_lte?: Nullable<BigDecimal>;
    dailySharedFee_in?: Nullable<BigDecimal[]>;
    dailySharedFee_not_in?: Nullable<BigDecimal[]>;
    dailySharedFeeUSD?: Nullable<BigDecimal>;
    dailySharedFeeUSD_not?: Nullable<BigDecimal>;
    dailySharedFeeUSD_gt?: Nullable<BigDecimal>;
    dailySharedFeeUSD_lt?: Nullable<BigDecimal>;
    dailySharedFeeUSD_gte?: Nullable<BigDecimal>;
    dailySharedFeeUSD_lte?: Nullable<BigDecimal>;
    dailySharedFeeUSD_in?: Nullable<BigDecimal[]>;
    dailySharedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    dailyCovRatioSnapshot?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_not?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_gt?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_lt?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_gte?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_lte?: Nullable<BigDecimal>;
    dailyCovRatioSnapshot_in?: Nullable<BigDecimal[]>;
    dailyCovRatioSnapshot_not_in?: Nullable<BigDecimal[]>;
    dailyBaseApr?: Nullable<BigDecimal>;
    dailyBaseApr_not?: Nullable<BigDecimal>;
    dailyBaseApr_gt?: Nullable<BigDecimal>;
    dailyBaseApr_lt?: Nullable<BigDecimal>;
    dailyBaseApr_gte?: Nullable<BigDecimal>;
    dailyBaseApr_lte?: Nullable<BigDecimal>;
    dailyBaseApr_in?: Nullable<BigDecimal[]>;
    dailyBaseApr_not_in?: Nullable<BigDecimal[]>;
    dailyBonusApr?: Nullable<BigDecimal>;
    dailyBonusApr_not?: Nullable<BigDecimal>;
    dailyBonusApr_gt?: Nullable<BigDecimal>;
    dailyBonusApr_lt?: Nullable<BigDecimal>;
    dailyBonusApr_gte?: Nullable<BigDecimal>;
    dailyBonusApr_lte?: Nullable<BigDecimal>;
    dailyBonusApr_in?: Nullable<BigDecimal[]>;
    dailyBonusApr_not_in?: Nullable<BigDecimal[]>;
    dailyAvgBoostedApr?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_not?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_gt?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_lt?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_gte?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_lte?: Nullable<BigDecimal>;
    dailyAvgBoostedApr_in?: Nullable<BigDecimal[]>;
    dailyAvgBoostedApr_not_in?: Nullable<BigDecimal[]>;
    dailyBoostedTvlUSD?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_not?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_gt?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_lt?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_gte?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_lte?: Nullable<BigDecimal>;
    dailyBoostedTvlUSD_in?: Nullable<BigDecimal[]>;
    dailyBoostedTvlUSD_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<AssetDayData_filter>[]>;
    or?: Nullable<Nullable<AssetDayData_filter>[]>;
}

export class BoostedRewarder_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    asset?: Nullable<string>;
    asset_not?: Nullable<string>;
    asset_gt?: Nullable<string>;
    asset_lt?: Nullable<string>;
    asset_gte?: Nullable<string>;
    asset_lte?: Nullable<string>;
    asset_in?: Nullable<string[]>;
    asset_not_in?: Nullable<string[]>;
    asset_contains?: Nullable<string>;
    asset_contains_nocase?: Nullable<string>;
    asset_not_contains?: Nullable<string>;
    asset_not_contains_nocase?: Nullable<string>;
    asset_starts_with?: Nullable<string>;
    asset_starts_with_nocase?: Nullable<string>;
    asset_not_starts_with?: Nullable<string>;
    asset_not_starts_with_nocase?: Nullable<string>;
    asset_ends_with?: Nullable<string>;
    asset_ends_with_nocase?: Nullable<string>;
    asset_not_ends_with?: Nullable<string>;
    asset_not_ends_with_nocase?: Nullable<string>;
    asset_?: Nullable<Asset_filter>;
    rewardInfos?: Nullable<string[]>;
    rewardInfos_not?: Nullable<string[]>;
    rewardInfos_contains?: Nullable<string[]>;
    rewardInfos_contains_nocase?: Nullable<string[]>;
    rewardInfos_not_contains?: Nullable<string[]>;
    rewardInfos_not_contains_nocase?: Nullable<string[]>;
    rewardInfos_?: Nullable<BoostedRewardInfo_filter>;
    rewardLength?: Nullable<number>;
    rewardLength_not?: Nullable<number>;
    rewardLength_gt?: Nullable<number>;
    rewardLength_lt?: Nullable<number>;
    rewardLength_gte?: Nullable<number>;
    rewardLength_lte?: Nullable<number>;
    rewardLength_in?: Nullable<number[]>;
    rewardLength_not_in?: Nullable<number[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<BoostedRewarder_filter>[]>;
    or?: Nullable<Nullable<BoostedRewarder_filter>[]>;
}

export class BoostedRewardInfo_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    rewardToken?: Nullable<string>;
    rewardToken_not?: Nullable<string>;
    rewardToken_gt?: Nullable<string>;
    rewardToken_lt?: Nullable<string>;
    rewardToken_gte?: Nullable<string>;
    rewardToken_lte?: Nullable<string>;
    rewardToken_in?: Nullable<string[]>;
    rewardToken_not_in?: Nullable<string[]>;
    rewardToken_contains?: Nullable<string>;
    rewardToken_contains_nocase?: Nullable<string>;
    rewardToken_not_contains?: Nullable<string>;
    rewardToken_not_contains_nocase?: Nullable<string>;
    rewardToken_starts_with?: Nullable<string>;
    rewardToken_starts_with_nocase?: Nullable<string>;
    rewardToken_not_starts_with?: Nullable<string>;
    rewardToken_not_starts_with_nocase?: Nullable<string>;
    rewardToken_ends_with?: Nullable<string>;
    rewardToken_ends_with_nocase?: Nullable<string>;
    rewardToken_not_ends_with?: Nullable<string>;
    rewardToken_not_ends_with_nocase?: Nullable<string>;
    rewardToken_?: Nullable<Token_filter>;
    tokenPerSec?: Nullable<BigDecimal>;
    tokenPerSec_not?: Nullable<BigDecimal>;
    tokenPerSec_gt?: Nullable<BigDecimal>;
    tokenPerSec_lt?: Nullable<BigDecimal>;
    tokenPerSec_gte?: Nullable<BigDecimal>;
    tokenPerSec_lte?: Nullable<BigDecimal>;
    tokenPerSec_in?: Nullable<BigDecimal[]>;
    tokenPerSec_not_in?: Nullable<BigDecimal[]>;
    apr?: Nullable<BigDecimal>;
    apr_not?: Nullable<BigDecimal>;
    apr_gt?: Nullable<BigDecimal>;
    apr_lt?: Nullable<BigDecimal>;
    apr_gte?: Nullable<BigDecimal>;
    apr_lte?: Nullable<BigDecimal>;
    apr_in?: Nullable<BigDecimal[]>;
    apr_not_in?: Nullable<BigDecimal[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<BoostedRewardInfo_filter>[]>;
    or?: Nullable<Nullable<BoostedRewardInfo_filter>[]>;
}

export class Bribe_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    lpToken?: Nullable<string>;
    lpToken_not?: Nullable<string>;
    lpToken_gt?: Nullable<string>;
    lpToken_lt?: Nullable<string>;
    lpToken_gte?: Nullable<string>;
    lpToken_lte?: Nullable<string>;
    lpToken_in?: Nullable<string[]>;
    lpToken_not_in?: Nullable<string[]>;
    lpToken_contains?: Nullable<string>;
    lpToken_contains_nocase?: Nullable<string>;
    lpToken_not_contains?: Nullable<string>;
    lpToken_not_contains_nocase?: Nullable<string>;
    lpToken_starts_with?: Nullable<string>;
    lpToken_starts_with_nocase?: Nullable<string>;
    lpToken_not_starts_with?: Nullable<string>;
    lpToken_not_starts_with_nocase?: Nullable<string>;
    lpToken_ends_with?: Nullable<string>;
    lpToken_ends_with_nocase?: Nullable<string>;
    lpToken_not_ends_with?: Nullable<string>;
    lpToken_not_ends_with_nocase?: Nullable<string>;
    lpToken_?: Nullable<Asset_filter>;
    bribeInfoCount?: Nullable<number>;
    bribeInfoCount_not?: Nullable<number>;
    bribeInfoCount_gt?: Nullable<number>;
    bribeInfoCount_lt?: Nullable<number>;
    bribeInfoCount_gte?: Nullable<number>;
    bribeInfoCount_lte?: Nullable<number>;
    bribeInfoCount_in?: Nullable<number[]>;
    bribeInfoCount_not_in?: Nullable<number[]>;
    bribeInfos?: Nullable<string[]>;
    bribeInfos_not?: Nullable<string[]>;
    bribeInfos_contains?: Nullable<string[]>;
    bribeInfos_contains_nocase?: Nullable<string[]>;
    bribeInfos_not_contains?: Nullable<string[]>;
    bribeInfos_not_contains_nocase?: Nullable<string[]>;
    bribeInfos_?: Nullable<BribeInfo_filter>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Bribe_filter>[]>;
    or?: Nullable<Nullable<Bribe_filter>[]>;
}

export class BribeInfo_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    rewardToken?: Nullable<string>;
    rewardToken_not?: Nullable<string>;
    rewardToken_gt?: Nullable<string>;
    rewardToken_lt?: Nullable<string>;
    rewardToken_gte?: Nullable<string>;
    rewardToken_lte?: Nullable<string>;
    rewardToken_in?: Nullable<string[]>;
    rewardToken_not_in?: Nullable<string[]>;
    rewardToken_contains?: Nullable<string>;
    rewardToken_contains_nocase?: Nullable<string>;
    rewardToken_not_contains?: Nullable<string>;
    rewardToken_not_contains_nocase?: Nullable<string>;
    rewardToken_starts_with?: Nullable<string>;
    rewardToken_starts_with_nocase?: Nullable<string>;
    rewardToken_not_starts_with?: Nullable<string>;
    rewardToken_not_starts_with_nocase?: Nullable<string>;
    rewardToken_ends_with?: Nullable<string>;
    rewardToken_ends_with_nocase?: Nullable<string>;
    rewardToken_not_ends_with?: Nullable<string>;
    rewardToken_not_ends_with_nocase?: Nullable<string>;
    rewardToken_?: Nullable<Token_filter>;
    tokenPerSec?: Nullable<BigDecimal>;
    tokenPerSec_not?: Nullable<BigDecimal>;
    tokenPerSec_gt?: Nullable<BigDecimal>;
    tokenPerSec_lt?: Nullable<BigDecimal>;
    tokenPerSec_gte?: Nullable<BigDecimal>;
    tokenPerSec_lte?: Nullable<BigDecimal>;
    tokenPerSec_in?: Nullable<BigDecimal[]>;
    tokenPerSec_not_in?: Nullable<BigDecimal[]>;
    distributedAmount?: Nullable<BigDecimal>;
    distributedAmount_not?: Nullable<BigDecimal>;
    distributedAmount_gt?: Nullable<BigDecimal>;
    distributedAmount_lt?: Nullable<BigDecimal>;
    distributedAmount_gte?: Nullable<BigDecimal>;
    distributedAmount_lte?: Nullable<BigDecimal>;
    distributedAmount_in?: Nullable<BigDecimal[]>;
    distributedAmount_not_in?: Nullable<BigDecimal[]>;
    distributedAmountUSD?: Nullable<BigDecimal>;
    distributedAmountUSD_not?: Nullable<BigDecimal>;
    distributedAmountUSD_gt?: Nullable<BigDecimal>;
    distributedAmountUSD_lt?: Nullable<BigDecimal>;
    distributedAmountUSD_gte?: Nullable<BigDecimal>;
    distributedAmountUSD_lte?: Nullable<BigDecimal>;
    distributedAmountUSD_in?: Nullable<BigDecimal[]>;
    distributedAmountUSD_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<BribeInfo_filter>[]>;
    or?: Nullable<Nullable<BribeInfo_filter>[]>;
}

export class Burn_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Burn_filter>[]>;
    or?: Nullable<Nullable<Burn_filter>[]>;
}

export class Deposit_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    to?: Nullable<Bytes>;
    to_not?: Nullable<Bytes>;
    to_gt?: Nullable<Bytes>;
    to_lt?: Nullable<Bytes>;
    to_gte?: Nullable<Bytes>;
    to_lte?: Nullable<Bytes>;
    to_in?: Nullable<Bytes[]>;
    to_not_in?: Nullable<Bytes[]>;
    to_contains?: Nullable<Bytes>;
    to_not_contains?: Nullable<Bytes>;
    sentFrom?: Nullable<Bytes>;
    sentFrom_not?: Nullable<Bytes>;
    sentFrom_gt?: Nullable<Bytes>;
    sentFrom_lt?: Nullable<Bytes>;
    sentFrom_gte?: Nullable<Bytes>;
    sentFrom_lte?: Nullable<Bytes>;
    sentFrom_in?: Nullable<Bytes[]>;
    sentFrom_not_in?: Nullable<Bytes[]>;
    sentFrom_contains?: Nullable<Bytes>;
    sentFrom_not_contains?: Nullable<Bytes>;
    sentTo?: Nullable<Bytes>;
    sentTo_not?: Nullable<Bytes>;
    sentTo_gt?: Nullable<Bytes>;
    sentTo_lt?: Nullable<Bytes>;
    sentTo_gte?: Nullable<Bytes>;
    sentTo_lte?: Nullable<Bytes>;
    sentTo_in?: Nullable<Bytes[]>;
    sentTo_not_in?: Nullable<Bytes[]>;
    sentTo_contains?: Nullable<Bytes>;
    sentTo_not_contains?: Nullable<Bytes>;
    token?: Nullable<string>;
    token_not?: Nullable<string>;
    token_gt?: Nullable<string>;
    token_lt?: Nullable<string>;
    token_gte?: Nullable<string>;
    token_lte?: Nullable<string>;
    token_in?: Nullable<string[]>;
    token_not_in?: Nullable<string[]>;
    token_contains?: Nullable<string>;
    token_contains_nocase?: Nullable<string>;
    token_not_contains?: Nullable<string>;
    token_not_contains_nocase?: Nullable<string>;
    token_starts_with?: Nullable<string>;
    token_starts_with_nocase?: Nullable<string>;
    token_not_starts_with?: Nullable<string>;
    token_not_starts_with_nocase?: Nullable<string>;
    token_ends_with?: Nullable<string>;
    token_ends_with_nocase?: Nullable<string>;
    token_not_ends_with?: Nullable<string>;
    token_not_ends_with_nocase?: Nullable<string>;
    token_?: Nullable<Token_filter>;
    asset?: Nullable<string>;
    asset_not?: Nullable<string>;
    asset_gt?: Nullable<string>;
    asset_lt?: Nullable<string>;
    asset_gte?: Nullable<string>;
    asset_lte?: Nullable<string>;
    asset_in?: Nullable<string[]>;
    asset_not_in?: Nullable<string[]>;
    asset_contains?: Nullable<string>;
    asset_contains_nocase?: Nullable<string>;
    asset_not_contains?: Nullable<string>;
    asset_not_contains_nocase?: Nullable<string>;
    asset_starts_with?: Nullable<string>;
    asset_starts_with_nocase?: Nullable<string>;
    asset_not_starts_with?: Nullable<string>;
    asset_not_starts_with_nocase?: Nullable<string>;
    asset_ends_with?: Nullable<string>;
    asset_ends_with_nocase?: Nullable<string>;
    asset_not_ends_with?: Nullable<string>;
    asset_not_ends_with_nocase?: Nullable<string>;
    asset_?: Nullable<Asset_filter>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    amountUSD?: Nullable<BigDecimal>;
    amountUSD_not?: Nullable<BigDecimal>;
    amountUSD_gt?: Nullable<BigDecimal>;
    amountUSD_lt?: Nullable<BigDecimal>;
    amountUSD_gte?: Nullable<BigDecimal>;
    amountUSD_lte?: Nullable<BigDecimal>;
    amountUSD_in?: Nullable<BigDecimal[]>;
    amountUSD_not_in?: Nullable<BigDecimal[]>;
    liquidity?: Nullable<BigDecimal>;
    liquidity_not?: Nullable<BigDecimal>;
    liquidity_gt?: Nullable<BigDecimal>;
    liquidity_lt?: Nullable<BigDecimal>;
    liquidity_gte?: Nullable<BigDecimal>;
    liquidity_lte?: Nullable<BigDecimal>;
    liquidity_in?: Nullable<BigDecimal[]>;
    liquidity_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Deposit_filter>[]>;
    or?: Nullable<Nullable<Deposit_filter>[]>;
}

export class Enter_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    unlockTime?: Nullable<BigInt>;
    unlockTime_not?: Nullable<BigInt>;
    unlockTime_gt?: Nullable<BigInt>;
    unlockTime_lt?: Nullable<BigInt>;
    unlockTime_gte?: Nullable<BigInt>;
    unlockTime_lte?: Nullable<BigInt>;
    unlockTime_in?: Nullable<BigInt[]>;
    unlockTime_not_in?: Nullable<BigInt[]>;
    veWomAmount?: Nullable<BigDecimal>;
    veWomAmount_not?: Nullable<BigDecimal>;
    veWomAmount_gt?: Nullable<BigDecimal>;
    veWomAmount_lt?: Nullable<BigDecimal>;
    veWomAmount_gte?: Nullable<BigDecimal>;
    veWomAmount_lte?: Nullable<BigDecimal>;
    veWomAmount_in?: Nullable<BigDecimal[]>;
    veWomAmount_not_in?: Nullable<BigDecimal[]>;
    womAmount?: Nullable<BigDecimal>;
    womAmount_not?: Nullable<BigDecimal>;
    womAmount_gt?: Nullable<BigDecimal>;
    womAmount_lt?: Nullable<BigDecimal>;
    womAmount_gte?: Nullable<BigDecimal>;
    womAmount_lte?: Nullable<BigDecimal>;
    womAmount_in?: Nullable<BigDecimal[]>;
    womAmount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Enter_filter>[]>;
    or?: Nullable<Nullable<Enter_filter>[]>;
}

export class Exit_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    unlockTime?: Nullable<BigInt>;
    unlockTime_not?: Nullable<BigInt>;
    unlockTime_gt?: Nullable<BigInt>;
    unlockTime_lt?: Nullable<BigInt>;
    unlockTime_gte?: Nullable<BigInt>;
    unlockTime_lte?: Nullable<BigInt>;
    unlockTime_in?: Nullable<BigInt[]>;
    unlockTime_not_in?: Nullable<BigInt[]>;
    veWomAmount?: Nullable<BigDecimal>;
    veWomAmount_not?: Nullable<BigDecimal>;
    veWomAmount_gt?: Nullable<BigDecimal>;
    veWomAmount_lt?: Nullable<BigDecimal>;
    veWomAmount_gte?: Nullable<BigDecimal>;
    veWomAmount_lte?: Nullable<BigDecimal>;
    veWomAmount_in?: Nullable<BigDecimal[]>;
    veWomAmount_not_in?: Nullable<BigDecimal[]>;
    womAmount?: Nullable<BigDecimal>;
    womAmount_not?: Nullable<BigDecimal>;
    womAmount_gt?: Nullable<BigDecimal>;
    womAmount_lt?: Nullable<BigDecimal>;
    womAmount_gte?: Nullable<BigDecimal>;
    womAmount_lte?: Nullable<BigDecimal>;
    womAmount_in?: Nullable<BigDecimal[]>;
    womAmount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Exit_filter>[]>;
    or?: Nullable<Nullable<Exit_filter>[]>;
}

export class LiquidityPosition_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    tvl?: Nullable<BigDecimal>;
    tvl_not?: Nullable<BigDecimal>;
    tvl_gt?: Nullable<BigDecimal>;
    tvl_lt?: Nullable<BigDecimal>;
    tvl_gte?: Nullable<BigDecimal>;
    tvl_lte?: Nullable<BigDecimal>;
    tvl_in?: Nullable<BigDecimal[]>;
    tvl_not_in?: Nullable<BigDecimal[]>;
    tvlUSD?: Nullable<BigDecimal>;
    tvlUSD_not?: Nullable<BigDecimal>;
    tvlUSD_gt?: Nullable<BigDecimal>;
    tvlUSD_lt?: Nullable<BigDecimal>;
    tvlUSD_gte?: Nullable<BigDecimal>;
    tvlUSD_lte?: Nullable<BigDecimal>;
    tvlUSD_in?: Nullable<BigDecimal[]>;
    tvlUSD_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<LiquidityPosition_filter>[]>;
    or?: Nullable<Nullable<LiquidityPosition_filter>[]>;
}

export class Lock_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    enterTime?: Nullable<BigInt>;
    enterTime_not?: Nullable<BigInt>;
    enterTime_gt?: Nullable<BigInt>;
    enterTime_lt?: Nullable<BigInt>;
    enterTime_gte?: Nullable<BigInt>;
    enterTime_lte?: Nullable<BigInt>;
    enterTime_in?: Nullable<BigInt[]>;
    enterTime_not_in?: Nullable<BigInt[]>;
    unlockTime?: Nullable<BigInt>;
    unlockTime_not?: Nullable<BigInt>;
    unlockTime_gt?: Nullable<BigInt>;
    unlockTime_lt?: Nullable<BigInt>;
    unlockTime_gte?: Nullable<BigInt>;
    unlockTime_lte?: Nullable<BigInt>;
    unlockTime_in?: Nullable<BigInt[]>;
    unlockTime_not_in?: Nullable<BigInt[]>;
    duration?: Nullable<BigInt>;
    duration_not?: Nullable<BigInt>;
    duration_gt?: Nullable<BigInt>;
    duration_lt?: Nullable<BigInt>;
    duration_gte?: Nullable<BigInt>;
    duration_lte?: Nullable<BigInt>;
    duration_in?: Nullable<BigInt[]>;
    duration_not_in?: Nullable<BigInt[]>;
    womAmount?: Nullable<BigDecimal>;
    womAmount_not?: Nullable<BigDecimal>;
    womAmount_gt?: Nullable<BigDecimal>;
    womAmount_lt?: Nullable<BigDecimal>;
    womAmount_gte?: Nullable<BigDecimal>;
    womAmount_lte?: Nullable<BigDecimal>;
    womAmount_in?: Nullable<BigDecimal[]>;
    womAmount_not_in?: Nullable<BigDecimal[]>;
    veWomAmount?: Nullable<BigDecimal>;
    veWomAmount_not?: Nullable<BigDecimal>;
    veWomAmount_gt?: Nullable<BigDecimal>;
    veWomAmount_lt?: Nullable<BigDecimal>;
    veWomAmount_gte?: Nullable<BigDecimal>;
    veWomAmount_lte?: Nullable<BigDecimal>;
    veWomAmount_in?: Nullable<BigDecimal[]>;
    veWomAmount_not_in?: Nullable<BigDecimal[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Lock_filter>[]>;
    or?: Nullable<Nullable<Lock_filter>[]>;
}

export class MasterWombat_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    voter?: Nullable<Bytes>;
    voter_not?: Nullable<Bytes>;
    voter_gt?: Nullable<Bytes>;
    voter_lt?: Nullable<Bytes>;
    voter_gte?: Nullable<Bytes>;
    voter_lte?: Nullable<Bytes>;
    voter_in?: Nullable<Bytes[]>;
    voter_not_in?: Nullable<Bytes[]>;
    voter_contains?: Nullable<Bytes>;
    voter_not_contains?: Nullable<Bytes>;
    wom?: Nullable<Bytes>;
    wom_not?: Nullable<Bytes>;
    wom_gt?: Nullable<Bytes>;
    wom_lt?: Nullable<Bytes>;
    wom_gte?: Nullable<Bytes>;
    wom_lte?: Nullable<Bytes>;
    wom_in?: Nullable<Bytes[]>;
    wom_not_in?: Nullable<Bytes[]>;
    wom_contains?: Nullable<Bytes>;
    wom_not_contains?: Nullable<Bytes>;
    veWom?: Nullable<Bytes>;
    veWom_not?: Nullable<Bytes>;
    veWom_gt?: Nullable<Bytes>;
    veWom_lt?: Nullable<Bytes>;
    veWom_gte?: Nullable<Bytes>;
    veWom_lte?: Nullable<Bytes>;
    veWom_in?: Nullable<Bytes[]>;
    veWom_not_in?: Nullable<Bytes[]>;
    veWom_contains?: Nullable<Bytes>;
    veWom_not_contains?: Nullable<Bytes>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<MasterWombat_filter>[]>;
    or?: Nullable<Nullable<MasterWombat_filter>[]>;
}

export class Mint_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Mint_filter>[]>;
    or?: Nullable<Nullable<Mint_filter>[]>;
}

export class MwDeposit_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<MwDeposit_filter>[]>;
    or?: Nullable<Nullable<MwDeposit_filter>[]>;
}

export class MwDepositFor_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<MwDepositFor_filter>[]>;
    or?: Nullable<Nullable<MwDepositFor_filter>[]>;
}

export class MwHarvest_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<MwHarvest_filter>[]>;
    or?: Nullable<Nullable<MwHarvest_filter>[]>;
}

export class MwWithdraw_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    pid?: Nullable<BigInt>;
    pid_not?: Nullable<BigInt>;
    pid_gt?: Nullable<BigInt>;
    pid_lt?: Nullable<BigInt>;
    pid_gte?: Nullable<BigInt>;
    pid_lte?: Nullable<BigInt>;
    pid_in?: Nullable<BigInt[]>;
    pid_not_in?: Nullable<BigInt[]>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<MwWithdraw_filter>[]>;
    or?: Nullable<Nullable<MwWithdraw_filter>[]>;
}

export class Pair_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    ticker_id?: Nullable<string>;
    ticker_id_not?: Nullable<string>;
    ticker_id_gt?: Nullable<string>;
    ticker_id_lt?: Nullable<string>;
    ticker_id_gte?: Nullable<string>;
    ticker_id_lte?: Nullable<string>;
    ticker_id_in?: Nullable<string[]>;
    ticker_id_not_in?: Nullable<string[]>;
    ticker_id_contains?: Nullable<string>;
    ticker_id_contains_nocase?: Nullable<string>;
    ticker_id_not_contains?: Nullable<string>;
    ticker_id_not_contains_nocase?: Nullable<string>;
    ticker_id_starts_with?: Nullable<string>;
    ticker_id_starts_with_nocase?: Nullable<string>;
    ticker_id_not_starts_with?: Nullable<string>;
    ticker_id_not_starts_with_nocase?: Nullable<string>;
    ticker_id_ends_with?: Nullable<string>;
    ticker_id_ends_with_nocase?: Nullable<string>;
    ticker_id_not_ends_with?: Nullable<string>;
    ticker_id_not_ends_with_nocase?: Nullable<string>;
    base?: Nullable<string>;
    base_not?: Nullable<string>;
    base_gt?: Nullable<string>;
    base_lt?: Nullable<string>;
    base_gte?: Nullable<string>;
    base_lte?: Nullable<string>;
    base_in?: Nullable<string[]>;
    base_not_in?: Nullable<string[]>;
    base_contains?: Nullable<string>;
    base_contains_nocase?: Nullable<string>;
    base_not_contains?: Nullable<string>;
    base_not_contains_nocase?: Nullable<string>;
    base_starts_with?: Nullable<string>;
    base_starts_with_nocase?: Nullable<string>;
    base_not_starts_with?: Nullable<string>;
    base_not_starts_with_nocase?: Nullable<string>;
    base_ends_with?: Nullable<string>;
    base_ends_with_nocase?: Nullable<string>;
    base_not_ends_with?: Nullable<string>;
    base_not_ends_with_nocase?: Nullable<string>;
    target?: Nullable<string>;
    target_not?: Nullable<string>;
    target_gt?: Nullable<string>;
    target_lt?: Nullable<string>;
    target_gte?: Nullable<string>;
    target_lte?: Nullable<string>;
    target_in?: Nullable<string[]>;
    target_not_in?: Nullable<string[]>;
    target_contains?: Nullable<string>;
    target_contains_nocase?: Nullable<string>;
    target_not_contains?: Nullable<string>;
    target_not_contains_nocase?: Nullable<string>;
    target_starts_with?: Nullable<string>;
    target_starts_with_nocase?: Nullable<string>;
    target_not_starts_with?: Nullable<string>;
    target_not_starts_with_nocase?: Nullable<string>;
    target_ends_with?: Nullable<string>;
    target_ends_with_nocase?: Nullable<string>;
    target_not_ends_with?: Nullable<string>;
    target_not_ends_with_nocase?: Nullable<string>;
    pool_id?: Nullable<string[]>;
    pool_id_not?: Nullable<string[]>;
    pool_id_contains?: Nullable<string[]>;
    pool_id_contains_nocase?: Nullable<string[]>;
    pool_id_not_contains?: Nullable<string[]>;
    pool_id_not_contains_nocase?: Nullable<string[]>;
    createdTimestamp?: Nullable<BigInt>;
    createdTimestamp_not?: Nullable<BigInt>;
    createdTimestamp_gt?: Nullable<BigInt>;
    createdTimestamp_lt?: Nullable<BigInt>;
    createdTimestamp_gte?: Nullable<BigInt>;
    createdTimestamp_lte?: Nullable<BigInt>;
    createdTimestamp_in?: Nullable<BigInt[]>;
    createdTimestamp_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Pair_filter>[]>;
    or?: Nullable<Nullable<Pair_filter>[]>;
}

export class PairSwap_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    pair?: Nullable<string>;
    pair_not?: Nullable<string>;
    pair_gt?: Nullable<string>;
    pair_lt?: Nullable<string>;
    pair_gte?: Nullable<string>;
    pair_lte?: Nullable<string>;
    pair_in?: Nullable<string[]>;
    pair_not_in?: Nullable<string[]>;
    pair_contains?: Nullable<string>;
    pair_contains_nocase?: Nullable<string>;
    pair_not_contains?: Nullable<string>;
    pair_not_contains_nocase?: Nullable<string>;
    pair_starts_with?: Nullable<string>;
    pair_starts_with_nocase?: Nullable<string>;
    pair_not_starts_with?: Nullable<string>;
    pair_not_starts_with_nocase?: Nullable<string>;
    pair_ends_with?: Nullable<string>;
    pair_ends_with_nocase?: Nullable<string>;
    pair_not_ends_with?: Nullable<string>;
    pair_not_ends_with_nocase?: Nullable<string>;
    pair_?: Nullable<Pair_filter>;
    sender?: Nullable<Bytes>;
    sender_not?: Nullable<Bytes>;
    sender_gt?: Nullable<Bytes>;
    sender_lt?: Nullable<Bytes>;
    sender_gte?: Nullable<Bytes>;
    sender_lte?: Nullable<Bytes>;
    sender_in?: Nullable<Bytes[]>;
    sender_not_in?: Nullable<Bytes[]>;
    sender_contains?: Nullable<Bytes>;
    sender_not_contains?: Nullable<Bytes>;
    base_amount?: Nullable<BigDecimal>;
    base_amount_not?: Nullable<BigDecimal>;
    base_amount_gt?: Nullable<BigDecimal>;
    base_amount_lt?: Nullable<BigDecimal>;
    base_amount_gte?: Nullable<BigDecimal>;
    base_amount_lte?: Nullable<BigDecimal>;
    base_amount_in?: Nullable<BigDecimal[]>;
    base_amount_not_in?: Nullable<BigDecimal[]>;
    target_amount?: Nullable<BigDecimal>;
    target_amount_not?: Nullable<BigDecimal>;
    target_amount_gt?: Nullable<BigDecimal>;
    target_amount_lt?: Nullable<BigDecimal>;
    target_amount_gte?: Nullable<BigDecimal>;
    target_amount_lte?: Nullable<BigDecimal>;
    target_amount_in?: Nullable<BigDecimal[]>;
    target_amount_not_in?: Nullable<BigDecimal[]>;
    base_price?: Nullable<BigDecimal>;
    base_price_not?: Nullable<BigDecimal>;
    base_price_gt?: Nullable<BigDecimal>;
    base_price_lt?: Nullable<BigDecimal>;
    base_price_gte?: Nullable<BigDecimal>;
    base_price_lte?: Nullable<BigDecimal>;
    base_price_in?: Nullable<BigDecimal[]>;
    base_price_not_in?: Nullable<BigDecimal[]>;
    target_price?: Nullable<BigDecimal>;
    target_price_not?: Nullable<BigDecimal>;
    target_price_gt?: Nullable<BigDecimal>;
    target_price_lt?: Nullable<BigDecimal>;
    target_price_gte?: Nullable<BigDecimal>;
    target_price_lte?: Nullable<BigDecimal>;
    target_price_in?: Nullable<BigDecimal[]>;
    target_price_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<PairSwap_filter>[]>;
    or?: Nullable<Nullable<PairSwap_filter>[]>;
}

export class Pool_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    assets?: Nullable<string[]>;
    assets_not?: Nullable<string[]>;
    assets_contains?: Nullable<string[]>;
    assets_contains_nocase?: Nullable<string[]>;
    assets_not_contains?: Nullable<string[]>;
    assets_not_contains_nocase?: Nullable<string[]>;
    assets_?: Nullable<Asset_filter>;
    haircute?: Nullable<BigDecimal>;
    haircute_not?: Nullable<BigDecimal>;
    haircute_gt?: Nullable<BigDecimal>;
    haircute_lt?: Nullable<BigDecimal>;
    haircute_gte?: Nullable<BigDecimal>;
    haircute_lte?: Nullable<BigDecimal>;
    haircute_in?: Nullable<BigDecimal[]>;
    haircute_not_in?: Nullable<BigDecimal[]>;
    retentionRatio?: Nullable<BigDecimal>;
    retentionRatio_not?: Nullable<BigDecimal>;
    retentionRatio_gt?: Nullable<BigDecimal>;
    retentionRatio_lt?: Nullable<BigDecimal>;
    retentionRatio_gte?: Nullable<BigDecimal>;
    retentionRatio_lte?: Nullable<BigDecimal>;
    retentionRatio_in?: Nullable<BigDecimal[]>;
    retentionRatio_not_in?: Nullable<BigDecimal[]>;
    lpDividendRatio?: Nullable<BigDecimal>;
    lpDividendRatio_not?: Nullable<BigDecimal>;
    lpDividendRatio_gt?: Nullable<BigDecimal>;
    lpDividendRatio_lt?: Nullable<BigDecimal>;
    lpDividendRatio_gte?: Nullable<BigDecimal>;
    lpDividendRatio_lte?: Nullable<BigDecimal>;
    lpDividendRatio_in?: Nullable<BigDecimal[]>;
    lpDividendRatio_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    createdTimestamp?: Nullable<BigInt>;
    createdTimestamp_not?: Nullable<BigInt>;
    createdTimestamp_gt?: Nullable<BigInt>;
    createdTimestamp_lt?: Nullable<BigInt>;
    createdTimestamp_gte?: Nullable<BigInt>;
    createdTimestamp_lte?: Nullable<BigInt>;
    createdTimestamp_in?: Nullable<BigInt[]>;
    createdTimestamp_not_in?: Nullable<BigInt[]>;
    createdBlock?: Nullable<BigInt>;
    createdBlock_not?: Nullable<BigInt>;
    createdBlock_gt?: Nullable<BigInt>;
    createdBlock_lt?: Nullable<BigInt>;
    createdBlock_gte?: Nullable<BigInt>;
    createdBlock_lte?: Nullable<BigInt>;
    createdBlock_in?: Nullable<BigInt[]>;
    createdBlock_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Pool_filter>[]>;
    or?: Nullable<Nullable<Pool_filter>[]>;
}

export class Protocol_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    version?: Nullable<string>;
    version_not?: Nullable<string>;
    version_gt?: Nullable<string>;
    version_lt?: Nullable<string>;
    version_gte?: Nullable<string>;
    version_lte?: Nullable<string>;
    version_in?: Nullable<string[]>;
    version_not_in?: Nullable<string[]>;
    version_contains?: Nullable<string>;
    version_contains_nocase?: Nullable<string>;
    version_not_contains?: Nullable<string>;
    version_not_contains_nocase?: Nullable<string>;
    version_starts_with?: Nullable<string>;
    version_starts_with_nocase?: Nullable<string>;
    version_not_starts_with?: Nullable<string>;
    version_not_starts_with_nocase?: Nullable<string>;
    version_ends_with?: Nullable<string>;
    version_ends_with_nocase?: Nullable<string>;
    version_not_ends_with?: Nullable<string>;
    version_not_ends_with_nocase?: Nullable<string>;
    network?: Nullable<Network>;
    network_not?: Nullable<Network>;
    network_in?: Nullable<Network[]>;
    network_not_in?: Nullable<Network[]>;
    pools?: Nullable<string[]>;
    pools_not?: Nullable<string[]>;
    pools_contains?: Nullable<string[]>;
    pools_contains_nocase?: Nullable<string[]>;
    pools_not_contains?: Nullable<string[]>;
    pools_not_contains_nocase?: Nullable<string[]>;
    pools_?: Nullable<Pool_filter>;
    assets?: Nullable<string[]>;
    assets_not?: Nullable<string[]>;
    assets_contains?: Nullable<string[]>;
    assets_contains_nocase?: Nullable<string[]>;
    assets_not_contains?: Nullable<string[]>;
    assets_not_contains_nocase?: Nullable<string[]>;
    assets_?: Nullable<Asset_filter>;
    createdTimestamp?: Nullable<BigInt>;
    createdTimestamp_not?: Nullable<BigInt>;
    createdTimestamp_gt?: Nullable<BigInt>;
    createdTimestamp_lt?: Nullable<BigInt>;
    createdTimestamp_gte?: Nullable<BigInt>;
    createdTimestamp_lte?: Nullable<BigInt>;
    createdTimestamp_in?: Nullable<BigInt[]>;
    createdTimestamp_not_in?: Nullable<BigInt[]>;
    totalTradeVolumeUSD?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_not?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_gt?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_lt?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_gte?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_lte?: Nullable<BigDecimal>;
    totalTradeVolumeUSD_in?: Nullable<BigDecimal[]>;
    totalTradeVolumeUSD_not_in?: Nullable<BigDecimal[]>;
    totalCollectedFeeUSD?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_not?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_gt?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_lt?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_gte?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_lte?: Nullable<BigDecimal>;
    totalCollectedFeeUSD_in?: Nullable<BigDecimal[]>;
    totalCollectedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    totalSharedFeeUSD?: Nullable<BigDecimal>;
    totalSharedFeeUSD_not?: Nullable<BigDecimal>;
    totalSharedFeeUSD_gt?: Nullable<BigDecimal>;
    totalSharedFeeUSD_lt?: Nullable<BigDecimal>;
    totalSharedFeeUSD_gte?: Nullable<BigDecimal>;
    totalSharedFeeUSD_lte?: Nullable<BigDecimal>;
    totalSharedFeeUSD_in?: Nullable<BigDecimal[]>;
    totalSharedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    totalCashUSD?: Nullable<BigDecimal>;
    totalCashUSD_not?: Nullable<BigDecimal>;
    totalCashUSD_gt?: Nullable<BigDecimal>;
    totalCashUSD_lt?: Nullable<BigDecimal>;
    totalCashUSD_gte?: Nullable<BigDecimal>;
    totalCashUSD_lte?: Nullable<BigDecimal>;
    totalCashUSD_in?: Nullable<BigDecimal[]>;
    totalCashUSD_not_in?: Nullable<BigDecimal[]>;
    totalLiabilityUSD?: Nullable<BigDecimal>;
    totalLiabilityUSD_not?: Nullable<BigDecimal>;
    totalLiabilityUSD_gt?: Nullable<BigDecimal>;
    totalLiabilityUSD_lt?: Nullable<BigDecimal>;
    totalLiabilityUSD_gte?: Nullable<BigDecimal>;
    totalLiabilityUSD_lte?: Nullable<BigDecimal>;
    totalLiabilityUSD_in?: Nullable<BigDecimal[]>;
    totalLiabilityUSD_not_in?: Nullable<BigDecimal[]>;
    totalTvlUSD?: Nullable<BigDecimal>;
    totalTvlUSD_not?: Nullable<BigDecimal>;
    totalTvlUSD_gt?: Nullable<BigDecimal>;
    totalTvlUSD_lt?: Nullable<BigDecimal>;
    totalTvlUSD_gte?: Nullable<BigDecimal>;
    totalTvlUSD_lte?: Nullable<BigDecimal>;
    totalTvlUSD_in?: Nullable<BigDecimal[]>;
    totalTvlUSD_not_in?: Nullable<BigDecimal[]>;
    totalBoostedTvlUSD?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_not?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_gt?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_lt?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_gte?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_lte?: Nullable<BigDecimal>;
    totalBoostedTvlUSD_in?: Nullable<BigDecimal[]>;
    totalBoostedTvlUSD_not_in?: Nullable<BigDecimal[]>;
    boostedRatio?: Nullable<BigDecimal>;
    boostedRatio_not?: Nullable<BigDecimal>;
    boostedRatio_gt?: Nullable<BigDecimal>;
    boostedRatio_lt?: Nullable<BigDecimal>;
    boostedRatio_gte?: Nullable<BigDecimal>;
    boostedRatio_lte?: Nullable<BigDecimal>;
    boostedRatio_in?: Nullable<BigDecimal[]>;
    boostedRatio_not_in?: Nullable<BigDecimal[]>;
    totalBribeRevenueUSD?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_not?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_gt?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_lt?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_gte?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_lte?: Nullable<BigDecimal>;
    totalBribeRevenueUSD_in?: Nullable<BigDecimal[]>;
    totalBribeRevenueUSD_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Protocol_filter>[]>;
    or?: Nullable<Nullable<Protocol_filter>[]>;
}

export class ProtocolDayData_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    dayID?: Nullable<BigInt>;
    dayID_not?: Nullable<BigInt>;
    dayID_gt?: Nullable<BigInt>;
    dayID_lt?: Nullable<BigInt>;
    dayID_gte?: Nullable<BigInt>;
    dayID_lte?: Nullable<BigInt>;
    dayID_in?: Nullable<BigInt[]>;
    dayID_not_in?: Nullable<BigInt[]>;
    date?: Nullable<string>;
    date_not?: Nullable<string>;
    date_gt?: Nullable<string>;
    date_lt?: Nullable<string>;
    date_gte?: Nullable<string>;
    date_lte?: Nullable<string>;
    date_in?: Nullable<string[]>;
    date_not_in?: Nullable<string[]>;
    date_contains?: Nullable<string>;
    date_contains_nocase?: Nullable<string>;
    date_not_contains?: Nullable<string>;
    date_not_contains_nocase?: Nullable<string>;
    date_starts_with?: Nullable<string>;
    date_starts_with_nocase?: Nullable<string>;
    date_not_starts_with?: Nullable<string>;
    date_not_starts_with_nocase?: Nullable<string>;
    date_ends_with?: Nullable<string>;
    date_ends_with_nocase?: Nullable<string>;
    date_not_ends_with?: Nullable<string>;
    date_not_ends_with_nocase?: Nullable<string>;
    dailyTradeVolumeUSD?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_not?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_gt?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_lt?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_gte?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_lte?: Nullable<BigDecimal>;
    dailyTradeVolumeUSD_in?: Nullable<BigDecimal[]>;
    dailyTradeVolumeUSD_not_in?: Nullable<BigDecimal[]>;
    dailyCollectedFeeUSD?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_not?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_gt?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_lt?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_gte?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_lte?: Nullable<BigDecimal>;
    dailyCollectedFeeUSD_in?: Nullable<BigDecimal[]>;
    dailyCollectedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    dailySharedFeeUSD?: Nullable<BigDecimal>;
    dailySharedFeeUSD_not?: Nullable<BigDecimal>;
    dailySharedFeeUSD_gt?: Nullable<BigDecimal>;
    dailySharedFeeUSD_lt?: Nullable<BigDecimal>;
    dailySharedFeeUSD_gte?: Nullable<BigDecimal>;
    dailySharedFeeUSD_lte?: Nullable<BigDecimal>;
    dailySharedFeeUSD_in?: Nullable<BigDecimal[]>;
    dailySharedFeeUSD_not_in?: Nullable<BigDecimal[]>;
    dailyCashUSD?: Nullable<BigDecimal>;
    dailyCashUSD_not?: Nullable<BigDecimal>;
    dailyCashUSD_gt?: Nullable<BigDecimal>;
    dailyCashUSD_lt?: Nullable<BigDecimal>;
    dailyCashUSD_gte?: Nullable<BigDecimal>;
    dailyCashUSD_lte?: Nullable<BigDecimal>;
    dailyCashUSD_in?: Nullable<BigDecimal[]>;
    dailyCashUSD_not_in?: Nullable<BigDecimal[]>;
    dailyLiabilityUSD?: Nullable<BigDecimal>;
    dailyLiabilityUSD_not?: Nullable<BigDecimal>;
    dailyLiabilityUSD_gt?: Nullable<BigDecimal>;
    dailyLiabilityUSD_lt?: Nullable<BigDecimal>;
    dailyLiabilityUSD_gte?: Nullable<BigDecimal>;
    dailyLiabilityUSD_lte?: Nullable<BigDecimal>;
    dailyLiabilityUSD_in?: Nullable<BigDecimal[]>;
    dailyLiabilityUSD_not_in?: Nullable<BigDecimal[]>;
    dailyActiveUser?: Nullable<BigInt>;
    dailyActiveUser_not?: Nullable<BigInt>;
    dailyActiveUser_gt?: Nullable<BigInt>;
    dailyActiveUser_lt?: Nullable<BigInt>;
    dailyActiveUser_gte?: Nullable<BigInt>;
    dailyActiveUser_lte?: Nullable<BigInt>;
    dailyActiveUser_in?: Nullable<BigInt[]>;
    dailyActiveUser_not_in?: Nullable<BigInt[]>;
    dailyBribeRevenueUSD?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_not?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_gt?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_lt?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_gte?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_lte?: Nullable<BigDecimal>;
    dailyBribeRevenueUSD_in?: Nullable<BigDecimal[]>;
    dailyBribeRevenueUSD_not_in?: Nullable<BigDecimal[]>;
    dailyBribeRevenueUSDSnapshot?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_not?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_gt?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_lt?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_gte?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_lte?: Nullable<BigDecimal>;
    dailyBribeRevenueUSDSnapshot_in?: Nullable<BigDecimal[]>;
    dailyBribeRevenueUSDSnapshot_not_in?: Nullable<BigDecimal[]>;
    userLists?: Nullable<string[]>;
    userLists_not?: Nullable<string[]>;
    userLists_contains?: Nullable<string[]>;
    userLists_contains_nocase?: Nullable<string[]>;
    userLists_not_contains?: Nullable<string[]>;
    userLists_not_contains_nocase?: Nullable<string[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<ProtocolDayData_filter>[]>;
    or?: Nullable<Nullable<ProtocolDayData_filter>[]>;
}

export class Rewarder_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    asset?: Nullable<string>;
    asset_not?: Nullable<string>;
    asset_gt?: Nullable<string>;
    asset_lt?: Nullable<string>;
    asset_gte?: Nullable<string>;
    asset_lte?: Nullable<string>;
    asset_in?: Nullable<string[]>;
    asset_not_in?: Nullable<string[]>;
    asset_contains?: Nullable<string>;
    asset_contains_nocase?: Nullable<string>;
    asset_not_contains?: Nullable<string>;
    asset_not_contains_nocase?: Nullable<string>;
    asset_starts_with?: Nullable<string>;
    asset_starts_with_nocase?: Nullable<string>;
    asset_not_starts_with?: Nullable<string>;
    asset_not_starts_with_nocase?: Nullable<string>;
    asset_ends_with?: Nullable<string>;
    asset_ends_with_nocase?: Nullable<string>;
    asset_not_ends_with?: Nullable<string>;
    asset_not_ends_with_nocase?: Nullable<string>;
    asset_?: Nullable<Asset_filter>;
    rewardInfo?: Nullable<string[]>;
    rewardInfo_not?: Nullable<string[]>;
    rewardInfo_contains?: Nullable<string[]>;
    rewardInfo_contains_nocase?: Nullable<string[]>;
    rewardInfo_not_contains?: Nullable<string[]>;
    rewardInfo_not_contains_nocase?: Nullable<string[]>;
    rewardInfo_?: Nullable<RewardInfo_filter>;
    rewardInfoLength?: Nullable<number>;
    rewardInfoLength_not?: Nullable<number>;
    rewardInfoLength_gt?: Nullable<number>;
    rewardInfoLength_lt?: Nullable<number>;
    rewardInfoLength_gte?: Nullable<number>;
    rewardInfoLength_lte?: Nullable<number>;
    rewardInfoLength_in?: Nullable<number[]>;
    rewardInfoLength_not_in?: Nullable<number[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Rewarder_filter>[]>;
    or?: Nullable<Nullable<Rewarder_filter>[]>;
}

export class RewardInfo_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    rewardToken?: Nullable<string>;
    rewardToken_not?: Nullable<string>;
    rewardToken_gt?: Nullable<string>;
    rewardToken_lt?: Nullable<string>;
    rewardToken_gte?: Nullable<string>;
    rewardToken_lte?: Nullable<string>;
    rewardToken_in?: Nullable<string[]>;
    rewardToken_not_in?: Nullable<string[]>;
    rewardToken_contains?: Nullable<string>;
    rewardToken_contains_nocase?: Nullable<string>;
    rewardToken_not_contains?: Nullable<string>;
    rewardToken_not_contains_nocase?: Nullable<string>;
    rewardToken_starts_with?: Nullable<string>;
    rewardToken_starts_with_nocase?: Nullable<string>;
    rewardToken_not_starts_with?: Nullable<string>;
    rewardToken_not_starts_with_nocase?: Nullable<string>;
    rewardToken_ends_with?: Nullable<string>;
    rewardToken_ends_with_nocase?: Nullable<string>;
    rewardToken_not_ends_with?: Nullable<string>;
    rewardToken_not_ends_with_nocase?: Nullable<string>;
    rewardToken_?: Nullable<Token_filter>;
    tokenPerSec?: Nullable<BigDecimal>;
    tokenPerSec_not?: Nullable<BigDecimal>;
    tokenPerSec_gt?: Nullable<BigDecimal>;
    tokenPerSec_lt?: Nullable<BigDecimal>;
    tokenPerSec_gte?: Nullable<BigDecimal>;
    tokenPerSec_lte?: Nullable<BigDecimal>;
    tokenPerSec_in?: Nullable<BigDecimal[]>;
    tokenPerSec_not_in?: Nullable<BigDecimal[]>;
    apr?: Nullable<BigDecimal>;
    apr_not?: Nullable<BigDecimal>;
    apr_gt?: Nullable<BigDecimal>;
    apr_lt?: Nullable<BigDecimal>;
    apr_gte?: Nullable<BigDecimal>;
    apr_lte?: Nullable<BigDecimal>;
    apr_in?: Nullable<BigDecimal[]>;
    apr_not_in?: Nullable<BigDecimal[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<RewardInfo_filter>[]>;
    or?: Nullable<Nullable<RewardInfo_filter>[]>;
}

export class Swap_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    to?: Nullable<Bytes>;
    to_not?: Nullable<Bytes>;
    to_gt?: Nullable<Bytes>;
    to_lt?: Nullable<Bytes>;
    to_gte?: Nullable<Bytes>;
    to_lte?: Nullable<Bytes>;
    to_in?: Nullable<Bytes[]>;
    to_not_in?: Nullable<Bytes[]>;
    to_contains?: Nullable<Bytes>;
    to_not_contains?: Nullable<Bytes>;
    sentFrom?: Nullable<Bytes>;
    sentFrom_not?: Nullable<Bytes>;
    sentFrom_gt?: Nullable<Bytes>;
    sentFrom_lt?: Nullable<Bytes>;
    sentFrom_gte?: Nullable<Bytes>;
    sentFrom_lte?: Nullable<Bytes>;
    sentFrom_in?: Nullable<Bytes[]>;
    sentFrom_not_in?: Nullable<Bytes[]>;
    sentFrom_contains?: Nullable<Bytes>;
    sentFrom_not_contains?: Nullable<Bytes>;
    sentTo?: Nullable<Bytes>;
    sentTo_not?: Nullable<Bytes>;
    sentTo_gt?: Nullable<Bytes>;
    sentTo_lt?: Nullable<Bytes>;
    sentTo_gte?: Nullable<Bytes>;
    sentTo_lte?: Nullable<Bytes>;
    sentTo_in?: Nullable<Bytes[]>;
    sentTo_not_in?: Nullable<Bytes[]>;
    sentTo_contains?: Nullable<Bytes>;
    sentTo_not_contains?: Nullable<Bytes>;
    fromToken?: Nullable<string>;
    fromToken_not?: Nullable<string>;
    fromToken_gt?: Nullable<string>;
    fromToken_lt?: Nullable<string>;
    fromToken_gte?: Nullable<string>;
    fromToken_lte?: Nullable<string>;
    fromToken_in?: Nullable<string[]>;
    fromToken_not_in?: Nullable<string[]>;
    fromToken_contains?: Nullable<string>;
    fromToken_contains_nocase?: Nullable<string>;
    fromToken_not_contains?: Nullable<string>;
    fromToken_not_contains_nocase?: Nullable<string>;
    fromToken_starts_with?: Nullable<string>;
    fromToken_starts_with_nocase?: Nullable<string>;
    fromToken_not_starts_with?: Nullable<string>;
    fromToken_not_starts_with_nocase?: Nullable<string>;
    fromToken_ends_with?: Nullable<string>;
    fromToken_ends_with_nocase?: Nullable<string>;
    fromToken_not_ends_with?: Nullable<string>;
    fromToken_not_ends_with_nocase?: Nullable<string>;
    fromToken_?: Nullable<Token_filter>;
    fromTokenFee?: Nullable<BigDecimal>;
    fromTokenFee_not?: Nullable<BigDecimal>;
    fromTokenFee_gt?: Nullable<BigDecimal>;
    fromTokenFee_lt?: Nullable<BigDecimal>;
    fromTokenFee_gte?: Nullable<BigDecimal>;
    fromTokenFee_lte?: Nullable<BigDecimal>;
    fromTokenFee_in?: Nullable<BigDecimal[]>;
    fromTokenFee_not_in?: Nullable<BigDecimal[]>;
    toToken?: Nullable<string>;
    toToken_not?: Nullable<string>;
    toToken_gt?: Nullable<string>;
    toToken_lt?: Nullable<string>;
    toToken_gte?: Nullable<string>;
    toToken_lte?: Nullable<string>;
    toToken_in?: Nullable<string[]>;
    toToken_not_in?: Nullable<string[]>;
    toToken_contains?: Nullable<string>;
    toToken_contains_nocase?: Nullable<string>;
    toToken_not_contains?: Nullable<string>;
    toToken_not_contains_nocase?: Nullable<string>;
    toToken_starts_with?: Nullable<string>;
    toToken_starts_with_nocase?: Nullable<string>;
    toToken_not_starts_with?: Nullable<string>;
    toToken_not_starts_with_nocase?: Nullable<string>;
    toToken_ends_with?: Nullable<string>;
    toToken_ends_with_nocase?: Nullable<string>;
    toToken_not_ends_with?: Nullable<string>;
    toToken_not_ends_with_nocase?: Nullable<string>;
    toToken_?: Nullable<Token_filter>;
    toTokenFee?: Nullable<BigDecimal>;
    toTokenFee_not?: Nullable<BigDecimal>;
    toTokenFee_gt?: Nullable<BigDecimal>;
    toTokenFee_lt?: Nullable<BigDecimal>;
    toTokenFee_gte?: Nullable<BigDecimal>;
    toTokenFee_lte?: Nullable<BigDecimal>;
    toTokenFee_in?: Nullable<BigDecimal[]>;
    toTokenFee_not_in?: Nullable<BigDecimal[]>;
    fromAsset?: Nullable<string>;
    fromAsset_not?: Nullable<string>;
    fromAsset_gt?: Nullable<string>;
    fromAsset_lt?: Nullable<string>;
    fromAsset_gte?: Nullable<string>;
    fromAsset_lte?: Nullable<string>;
    fromAsset_in?: Nullable<string[]>;
    fromAsset_not_in?: Nullable<string[]>;
    fromAsset_contains?: Nullable<string>;
    fromAsset_contains_nocase?: Nullable<string>;
    fromAsset_not_contains?: Nullable<string>;
    fromAsset_not_contains_nocase?: Nullable<string>;
    fromAsset_starts_with?: Nullable<string>;
    fromAsset_starts_with_nocase?: Nullable<string>;
    fromAsset_not_starts_with?: Nullable<string>;
    fromAsset_not_starts_with_nocase?: Nullable<string>;
    fromAsset_ends_with?: Nullable<string>;
    fromAsset_ends_with_nocase?: Nullable<string>;
    fromAsset_not_ends_with?: Nullable<string>;
    fromAsset_not_ends_with_nocase?: Nullable<string>;
    fromAsset_?: Nullable<Asset_filter>;
    toAsset?: Nullable<string>;
    toAsset_not?: Nullable<string>;
    toAsset_gt?: Nullable<string>;
    toAsset_lt?: Nullable<string>;
    toAsset_gte?: Nullable<string>;
    toAsset_lte?: Nullable<string>;
    toAsset_in?: Nullable<string[]>;
    toAsset_not_in?: Nullable<string[]>;
    toAsset_contains?: Nullable<string>;
    toAsset_contains_nocase?: Nullable<string>;
    toAsset_not_contains?: Nullable<string>;
    toAsset_not_contains_nocase?: Nullable<string>;
    toAsset_starts_with?: Nullable<string>;
    toAsset_starts_with_nocase?: Nullable<string>;
    toAsset_not_starts_with?: Nullable<string>;
    toAsset_not_starts_with_nocase?: Nullable<string>;
    toAsset_ends_with?: Nullable<string>;
    toAsset_ends_with_nocase?: Nullable<string>;
    toAsset_not_ends_with?: Nullable<string>;
    toAsset_not_ends_with_nocase?: Nullable<string>;
    toAsset_?: Nullable<Asset_filter>;
    fromAmount?: Nullable<BigDecimal>;
    fromAmount_not?: Nullable<BigDecimal>;
    fromAmount_gt?: Nullable<BigDecimal>;
    fromAmount_lt?: Nullable<BigDecimal>;
    fromAmount_gte?: Nullable<BigDecimal>;
    fromAmount_lte?: Nullable<BigDecimal>;
    fromAmount_in?: Nullable<BigDecimal[]>;
    fromAmount_not_in?: Nullable<BigDecimal[]>;
    fromAmountUSD?: Nullable<BigDecimal>;
    fromAmountUSD_not?: Nullable<BigDecimal>;
    fromAmountUSD_gt?: Nullable<BigDecimal>;
    fromAmountUSD_lt?: Nullable<BigDecimal>;
    fromAmountUSD_gte?: Nullable<BigDecimal>;
    fromAmountUSD_lte?: Nullable<BigDecimal>;
    fromAmountUSD_in?: Nullable<BigDecimal[]>;
    fromAmountUSD_not_in?: Nullable<BigDecimal[]>;
    toAmount?: Nullable<BigDecimal>;
    toAmount_not?: Nullable<BigDecimal>;
    toAmount_gt?: Nullable<BigDecimal>;
    toAmount_lt?: Nullable<BigDecimal>;
    toAmount_gte?: Nullable<BigDecimal>;
    toAmount_lte?: Nullable<BigDecimal>;
    toAmount_in?: Nullable<BigDecimal[]>;
    toAmount_not_in?: Nullable<BigDecimal[]>;
    toAmountUSD?: Nullable<BigDecimal>;
    toAmountUSD_not?: Nullable<BigDecimal>;
    toAmountUSD_gt?: Nullable<BigDecimal>;
    toAmountUSD_lt?: Nullable<BigDecimal>;
    toAmountUSD_gte?: Nullable<BigDecimal>;
    toAmountUSD_lte?: Nullable<BigDecimal>;
    toAmountUSD_in?: Nullable<BigDecimal[]>;
    toAmountUSD_not_in?: Nullable<BigDecimal[]>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    amountUSD?: Nullable<BigDecimal>;
    amountUSD_not?: Nullable<BigDecimal>;
    amountUSD_gt?: Nullable<BigDecimal>;
    amountUSD_lt?: Nullable<BigDecimal>;
    amountUSD_gte?: Nullable<BigDecimal>;
    amountUSD_lte?: Nullable<BigDecimal>;
    amountUSD_in?: Nullable<BigDecimal[]>;
    amountUSD_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Swap_filter>[]>;
    or?: Nullable<Nullable<Swap_filter>[]>;
}

export class SwapCreditForTokens_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    to?: Nullable<Bytes>;
    to_not?: Nullable<Bytes>;
    to_gt?: Nullable<Bytes>;
    to_lt?: Nullable<Bytes>;
    to_gte?: Nullable<Bytes>;
    to_lte?: Nullable<Bytes>;
    to_in?: Nullable<Bytes[]>;
    to_not_in?: Nullable<Bytes[]>;
    to_contains?: Nullable<Bytes>;
    to_not_contains?: Nullable<Bytes>;
    sentTo?: Nullable<Bytes>;
    sentTo_not?: Nullable<Bytes>;
    sentTo_gt?: Nullable<Bytes>;
    sentTo_lt?: Nullable<Bytes>;
    sentTo_gte?: Nullable<Bytes>;
    sentTo_lte?: Nullable<Bytes>;
    sentTo_in?: Nullable<Bytes[]>;
    sentTo_not_in?: Nullable<Bytes[]>;
    sentTo_contains?: Nullable<Bytes>;
    sentTo_not_contains?: Nullable<Bytes>;
    toToken?: Nullable<string>;
    toToken_not?: Nullable<string>;
    toToken_gt?: Nullable<string>;
    toToken_lt?: Nullable<string>;
    toToken_gte?: Nullable<string>;
    toToken_lte?: Nullable<string>;
    toToken_in?: Nullable<string[]>;
    toToken_not_in?: Nullable<string[]>;
    toToken_contains?: Nullable<string>;
    toToken_contains_nocase?: Nullable<string>;
    toToken_not_contains?: Nullable<string>;
    toToken_not_contains_nocase?: Nullable<string>;
    toToken_starts_with?: Nullable<string>;
    toToken_starts_with_nocase?: Nullable<string>;
    toToken_not_starts_with?: Nullable<string>;
    toToken_not_starts_with_nocase?: Nullable<string>;
    toToken_ends_with?: Nullable<string>;
    toToken_ends_with_nocase?: Nullable<string>;
    toToken_not_ends_with?: Nullable<string>;
    toToken_not_ends_with_nocase?: Nullable<string>;
    toToken_?: Nullable<Token_filter>;
    toTokenFee?: Nullable<BigDecimal>;
    toTokenFee_not?: Nullable<BigDecimal>;
    toTokenFee_gt?: Nullable<BigDecimal>;
    toTokenFee_lt?: Nullable<BigDecimal>;
    toTokenFee_gte?: Nullable<BigDecimal>;
    toTokenFee_lte?: Nullable<BigDecimal>;
    toTokenFee_in?: Nullable<BigDecimal[]>;
    toTokenFee_not_in?: Nullable<BigDecimal[]>;
    toAsset?: Nullable<string>;
    toAsset_not?: Nullable<string>;
    toAsset_gt?: Nullable<string>;
    toAsset_lt?: Nullable<string>;
    toAsset_gte?: Nullable<string>;
    toAsset_lte?: Nullable<string>;
    toAsset_in?: Nullable<string[]>;
    toAsset_not_in?: Nullable<string[]>;
    toAsset_contains?: Nullable<string>;
    toAsset_contains_nocase?: Nullable<string>;
    toAsset_not_contains?: Nullable<string>;
    toAsset_not_contains_nocase?: Nullable<string>;
    toAsset_starts_with?: Nullable<string>;
    toAsset_starts_with_nocase?: Nullable<string>;
    toAsset_not_starts_with?: Nullable<string>;
    toAsset_not_starts_with_nocase?: Nullable<string>;
    toAsset_ends_with?: Nullable<string>;
    toAsset_ends_with_nocase?: Nullable<string>;
    toAsset_not_ends_with?: Nullable<string>;
    toAsset_not_ends_with_nocase?: Nullable<string>;
    toAsset_?: Nullable<Asset_filter>;
    toAmount?: Nullable<BigDecimal>;
    toAmount_not?: Nullable<BigDecimal>;
    toAmount_gt?: Nullable<BigDecimal>;
    toAmount_lt?: Nullable<BigDecimal>;
    toAmount_gte?: Nullable<BigDecimal>;
    toAmount_lte?: Nullable<BigDecimal>;
    toAmount_in?: Nullable<BigDecimal[]>;
    toAmount_not_in?: Nullable<BigDecimal[]>;
    toAmountUSD?: Nullable<BigDecimal>;
    toAmountUSD_not?: Nullable<BigDecimal>;
    toAmountUSD_gt?: Nullable<BigDecimal>;
    toAmountUSD_lt?: Nullable<BigDecimal>;
    toAmountUSD_gte?: Nullable<BigDecimal>;
    toAmountUSD_lte?: Nullable<BigDecimal>;
    toAmountUSD_in?: Nullable<BigDecimal[]>;
    toAmountUSD_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<SwapCreditForTokens_filter>[]>;
    or?: Nullable<Nullable<SwapCreditForTokens_filter>[]>;
}

export class SwapTokensForCredit_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    to?: Nullable<Bytes>;
    to_not?: Nullable<Bytes>;
    to_gt?: Nullable<Bytes>;
    to_lt?: Nullable<Bytes>;
    to_gte?: Nullable<Bytes>;
    to_lte?: Nullable<Bytes>;
    to_in?: Nullable<Bytes[]>;
    to_not_in?: Nullable<Bytes[]>;
    to_contains?: Nullable<Bytes>;
    to_not_contains?: Nullable<Bytes>;
    sentFrom?: Nullable<Bytes>;
    sentFrom_not?: Nullable<Bytes>;
    sentFrom_gt?: Nullable<Bytes>;
    sentFrom_lt?: Nullable<Bytes>;
    sentFrom_gte?: Nullable<Bytes>;
    sentFrom_lte?: Nullable<Bytes>;
    sentFrom_in?: Nullable<Bytes[]>;
    sentFrom_not_in?: Nullable<Bytes[]>;
    sentFrom_contains?: Nullable<Bytes>;
    sentFrom_not_contains?: Nullable<Bytes>;
    fromToken?: Nullable<string>;
    fromToken_not?: Nullable<string>;
    fromToken_gt?: Nullable<string>;
    fromToken_lt?: Nullable<string>;
    fromToken_gte?: Nullable<string>;
    fromToken_lte?: Nullable<string>;
    fromToken_in?: Nullable<string[]>;
    fromToken_not_in?: Nullable<string[]>;
    fromToken_contains?: Nullable<string>;
    fromToken_contains_nocase?: Nullable<string>;
    fromToken_not_contains?: Nullable<string>;
    fromToken_not_contains_nocase?: Nullable<string>;
    fromToken_starts_with?: Nullable<string>;
    fromToken_starts_with_nocase?: Nullable<string>;
    fromToken_not_starts_with?: Nullable<string>;
    fromToken_not_starts_with_nocase?: Nullable<string>;
    fromToken_ends_with?: Nullable<string>;
    fromToken_ends_with_nocase?: Nullable<string>;
    fromToken_not_ends_with?: Nullable<string>;
    fromToken_not_ends_with_nocase?: Nullable<string>;
    fromToken_?: Nullable<Token_filter>;
    fromTokenFee?: Nullable<BigDecimal>;
    fromTokenFee_not?: Nullable<BigDecimal>;
    fromTokenFee_gt?: Nullable<BigDecimal>;
    fromTokenFee_lt?: Nullable<BigDecimal>;
    fromTokenFee_gte?: Nullable<BigDecimal>;
    fromTokenFee_lte?: Nullable<BigDecimal>;
    fromTokenFee_in?: Nullable<BigDecimal[]>;
    fromTokenFee_not_in?: Nullable<BigDecimal[]>;
    fromAsset?: Nullable<string>;
    fromAsset_not?: Nullable<string>;
    fromAsset_gt?: Nullable<string>;
    fromAsset_lt?: Nullable<string>;
    fromAsset_gte?: Nullable<string>;
    fromAsset_lte?: Nullable<string>;
    fromAsset_in?: Nullable<string[]>;
    fromAsset_not_in?: Nullable<string[]>;
    fromAsset_contains?: Nullable<string>;
    fromAsset_contains_nocase?: Nullable<string>;
    fromAsset_not_contains?: Nullable<string>;
    fromAsset_not_contains_nocase?: Nullable<string>;
    fromAsset_starts_with?: Nullable<string>;
    fromAsset_starts_with_nocase?: Nullable<string>;
    fromAsset_not_starts_with?: Nullable<string>;
    fromAsset_not_starts_with_nocase?: Nullable<string>;
    fromAsset_ends_with?: Nullable<string>;
    fromAsset_ends_with_nocase?: Nullable<string>;
    fromAsset_not_ends_with?: Nullable<string>;
    fromAsset_not_ends_with_nocase?: Nullable<string>;
    fromAsset_?: Nullable<Asset_filter>;
    fromAmount?: Nullable<BigDecimal>;
    fromAmount_not?: Nullable<BigDecimal>;
    fromAmount_gt?: Nullable<BigDecimal>;
    fromAmount_lt?: Nullable<BigDecimal>;
    fromAmount_gte?: Nullable<BigDecimal>;
    fromAmount_lte?: Nullable<BigDecimal>;
    fromAmount_in?: Nullable<BigDecimal[]>;
    fromAmount_not_in?: Nullable<BigDecimal[]>;
    fromAmountUSD?: Nullable<BigDecimal>;
    fromAmountUSD_not?: Nullable<BigDecimal>;
    fromAmountUSD_gt?: Nullable<BigDecimal>;
    fromAmountUSD_lt?: Nullable<BigDecimal>;
    fromAmountUSD_gte?: Nullable<BigDecimal>;
    fromAmountUSD_lte?: Nullable<BigDecimal>;
    fromAmountUSD_in?: Nullable<BigDecimal[]>;
    fromAmountUSD_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<SwapTokensForCredit_filter>[]>;
    or?: Nullable<Nullable<SwapTokensForCredit_filter>[]>;
}

export class Ticker_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    ticker_id?: Nullable<string>;
    ticker_id_not?: Nullable<string>;
    ticker_id_gt?: Nullable<string>;
    ticker_id_lt?: Nullable<string>;
    ticker_id_gte?: Nullable<string>;
    ticker_id_lte?: Nullable<string>;
    ticker_id_in?: Nullable<string[]>;
    ticker_id_not_in?: Nullable<string[]>;
    ticker_id_contains?: Nullable<string>;
    ticker_id_contains_nocase?: Nullable<string>;
    ticker_id_not_contains?: Nullable<string>;
    ticker_id_not_contains_nocase?: Nullable<string>;
    ticker_id_starts_with?: Nullable<string>;
    ticker_id_starts_with_nocase?: Nullable<string>;
    ticker_id_not_starts_with?: Nullable<string>;
    ticker_id_not_starts_with_nocase?: Nullable<string>;
    ticker_id_ends_with?: Nullable<string>;
    ticker_id_ends_with_nocase?: Nullable<string>;
    ticker_id_not_ends_with?: Nullable<string>;
    ticker_id_not_ends_with_nocase?: Nullable<string>;
    base_currency?: Nullable<string>;
    base_currency_not?: Nullable<string>;
    base_currency_gt?: Nullable<string>;
    base_currency_lt?: Nullable<string>;
    base_currency_gte?: Nullable<string>;
    base_currency_lte?: Nullable<string>;
    base_currency_in?: Nullable<string[]>;
    base_currency_not_in?: Nullable<string[]>;
    base_currency_contains?: Nullable<string>;
    base_currency_contains_nocase?: Nullable<string>;
    base_currency_not_contains?: Nullable<string>;
    base_currency_not_contains_nocase?: Nullable<string>;
    base_currency_starts_with?: Nullable<string>;
    base_currency_starts_with_nocase?: Nullable<string>;
    base_currency_not_starts_with?: Nullable<string>;
    base_currency_not_starts_with_nocase?: Nullable<string>;
    base_currency_ends_with?: Nullable<string>;
    base_currency_ends_with_nocase?: Nullable<string>;
    base_currency_not_ends_with?: Nullable<string>;
    base_currency_not_ends_with_nocase?: Nullable<string>;
    target_currency?: Nullable<string>;
    target_currency_not?: Nullable<string>;
    target_currency_gt?: Nullable<string>;
    target_currency_lt?: Nullable<string>;
    target_currency_gte?: Nullable<string>;
    target_currency_lte?: Nullable<string>;
    target_currency_in?: Nullable<string[]>;
    target_currency_not_in?: Nullable<string[]>;
    target_currency_contains?: Nullable<string>;
    target_currency_contains_nocase?: Nullable<string>;
    target_currency_not_contains?: Nullable<string>;
    target_currency_not_contains_nocase?: Nullable<string>;
    target_currency_starts_with?: Nullable<string>;
    target_currency_starts_with_nocase?: Nullable<string>;
    target_currency_not_starts_with?: Nullable<string>;
    target_currency_not_starts_with_nocase?: Nullable<string>;
    target_currency_ends_with?: Nullable<string>;
    target_currency_ends_with_nocase?: Nullable<string>;
    target_currency_not_ends_with?: Nullable<string>;
    target_currency_not_ends_with_nocase?: Nullable<string>;
    base_volume?: Nullable<BigDecimal>;
    base_volume_not?: Nullable<BigDecimal>;
    base_volume_gt?: Nullable<BigDecimal>;
    base_volume_lt?: Nullable<BigDecimal>;
    base_volume_gte?: Nullable<BigDecimal>;
    base_volume_lte?: Nullable<BigDecimal>;
    base_volume_in?: Nullable<BigDecimal[]>;
    base_volume_not_in?: Nullable<BigDecimal[]>;
    target_volume?: Nullable<BigDecimal>;
    target_volume_not?: Nullable<BigDecimal>;
    target_volume_gt?: Nullable<BigDecimal>;
    target_volume_lt?: Nullable<BigDecimal>;
    target_volume_gte?: Nullable<BigDecimal>;
    target_volume_lte?: Nullable<BigDecimal>;
    target_volume_in?: Nullable<BigDecimal[]>;
    target_volume_not_in?: Nullable<BigDecimal[]>;
    last_price?: Nullable<BigDecimal>;
    last_price_not?: Nullable<BigDecimal>;
    last_price_gt?: Nullable<BigDecimal>;
    last_price_lt?: Nullable<BigDecimal>;
    last_price_gte?: Nullable<BigDecimal>;
    last_price_lte?: Nullable<BigDecimal>;
    last_price_in?: Nullable<BigDecimal[]>;
    last_price_not_in?: Nullable<BigDecimal[]>;
    pool_id?: Nullable<string[]>;
    pool_id_not?: Nullable<string[]>;
    pool_id_contains?: Nullable<string[]>;
    pool_id_contains_nocase?: Nullable<string[]>;
    pool_id_not_contains?: Nullable<string[]>;
    pool_id_not_contains_nocase?: Nullable<string[]>;
    last_update?: Nullable<BigInt>;
    last_update_not?: Nullable<BigInt>;
    last_update_gt?: Nullable<BigInt>;
    last_update_lt?: Nullable<BigInt>;
    last_update_gte?: Nullable<BigInt>;
    last_update_lte?: Nullable<BigInt>;
    last_update_in?: Nullable<BigInt[]>;
    last_update_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Ticker_filter>[]>;
    or?: Nullable<Nullable<Ticker_filter>[]>;
}

export class Timer_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    currentTimestamp?: Nullable<BigInt>;
    currentTimestamp_not?: Nullable<BigInt>;
    currentTimestamp_gt?: Nullable<BigInt>;
    currentTimestamp_lt?: Nullable<BigInt>;
    currentTimestamp_gte?: Nullable<BigInt>;
    currentTimestamp_lte?: Nullable<BigInt>;
    currentTimestamp_in?: Nullable<BigInt[]>;
    currentTimestamp_not_in?: Nullable<BigInt[]>;
    interval?: Nullable<BigInt>;
    interval_not?: Nullable<BigInt>;
    interval_gt?: Nullable<BigInt>;
    interval_lt?: Nullable<BigInt>;
    interval_gte?: Nullable<BigInt>;
    interval_lte?: Nullable<BigInt>;
    interval_in?: Nullable<BigInt[]>;
    interval_not_in?: Nullable<BigInt[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Timer_filter>[]>;
    or?: Nullable<Nullable<Timer_filter>[]>;
}

export class Token_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    symbol?: Nullable<string>;
    symbol_not?: Nullable<string>;
    symbol_gt?: Nullable<string>;
    symbol_lt?: Nullable<string>;
    symbol_gte?: Nullable<string>;
    symbol_lte?: Nullable<string>;
    symbol_in?: Nullable<string[]>;
    symbol_not_in?: Nullable<string[]>;
    symbol_contains?: Nullable<string>;
    symbol_contains_nocase?: Nullable<string>;
    symbol_not_contains?: Nullable<string>;
    symbol_not_contains_nocase?: Nullable<string>;
    symbol_starts_with?: Nullable<string>;
    symbol_starts_with_nocase?: Nullable<string>;
    symbol_not_starts_with?: Nullable<string>;
    symbol_not_starts_with_nocase?: Nullable<string>;
    symbol_ends_with?: Nullable<string>;
    symbol_ends_with_nocase?: Nullable<string>;
    symbol_not_ends_with?: Nullable<string>;
    symbol_not_ends_with_nocase?: Nullable<string>;
    name?: Nullable<string>;
    name_not?: Nullable<string>;
    name_gt?: Nullable<string>;
    name_lt?: Nullable<string>;
    name_gte?: Nullable<string>;
    name_lte?: Nullable<string>;
    name_in?: Nullable<string[]>;
    name_not_in?: Nullable<string[]>;
    name_contains?: Nullable<string>;
    name_contains_nocase?: Nullable<string>;
    name_not_contains?: Nullable<string>;
    name_not_contains_nocase?: Nullable<string>;
    name_starts_with?: Nullable<string>;
    name_starts_with_nocase?: Nullable<string>;
    name_not_starts_with?: Nullable<string>;
    name_not_starts_with_nocase?: Nullable<string>;
    name_ends_with?: Nullable<string>;
    name_ends_with_nocase?: Nullable<string>;
    name_not_ends_with?: Nullable<string>;
    name_not_ends_with_nocase?: Nullable<string>;
    decimals?: Nullable<number>;
    decimals_not?: Nullable<number>;
    decimals_gt?: Nullable<number>;
    decimals_lt?: Nullable<number>;
    decimals_gte?: Nullable<number>;
    decimals_lte?: Nullable<number>;
    decimals_in?: Nullable<number[]>;
    decimals_not_in?: Nullable<number[]>;
    price?: Nullable<BigDecimal>;
    price_not?: Nullable<BigDecimal>;
    price_gt?: Nullable<BigDecimal>;
    price_lt?: Nullable<BigDecimal>;
    price_gte?: Nullable<BigDecimal>;
    price_lte?: Nullable<BigDecimal>;
    price_in?: Nullable<BigDecimal[]>;
    price_not_in?: Nullable<BigDecimal[]>;
    priceType?: Nullable<PriceType>;
    priceType_not?: Nullable<PriceType>;
    priceType_in?: Nullable<PriceType[]>;
    priceType_not_in?: Nullable<PriceType[]>;
    orcale?: Nullable<string>;
    orcale_not?: Nullable<string>;
    orcale_gt?: Nullable<string>;
    orcale_lt?: Nullable<string>;
    orcale_gte?: Nullable<string>;
    orcale_lte?: Nullable<string>;
    orcale_in?: Nullable<string[]>;
    orcale_not_in?: Nullable<string[]>;
    orcale_contains?: Nullable<string>;
    orcale_contains_nocase?: Nullable<string>;
    orcale_not_contains?: Nullable<string>;
    orcale_not_contains_nocase?: Nullable<string>;
    orcale_starts_with?: Nullable<string>;
    orcale_starts_with_nocase?: Nullable<string>;
    orcale_not_starts_with?: Nullable<string>;
    orcale_not_starts_with_nocase?: Nullable<string>;
    orcale_ends_with?: Nullable<string>;
    orcale_ends_with_nocase?: Nullable<string>;
    orcale_not_ends_with?: Nullable<string>;
    orcale_not_ends_with_nocase?: Nullable<string>;
    createdTimestamp?: Nullable<BigInt>;
    createdTimestamp_not?: Nullable<BigInt>;
    createdTimestamp_gt?: Nullable<BigInt>;
    createdTimestamp_lt?: Nullable<BigInt>;
    createdTimestamp_gte?: Nullable<BigInt>;
    createdTimestamp_lte?: Nullable<BigInt>;
    createdTimestamp_in?: Nullable<BigInt[]>;
    createdTimestamp_not_in?: Nullable<BigInt[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Token_filter>[]>;
    or?: Nullable<Nullable<Token_filter>[]>;
}

export class User_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    stakedWom?: Nullable<BigDecimal>;
    stakedWom_not?: Nullable<BigDecimal>;
    stakedWom_gt?: Nullable<BigDecimal>;
    stakedWom_lt?: Nullable<BigDecimal>;
    stakedWom_gte?: Nullable<BigDecimal>;
    stakedWom_lte?: Nullable<BigDecimal>;
    stakedWom_in?: Nullable<BigDecimal[]>;
    stakedWom_not_in?: Nullable<BigDecimal[]>;
    womBalance?: Nullable<BigDecimal>;
    womBalance_not?: Nullable<BigDecimal>;
    womBalance_gt?: Nullable<BigDecimal>;
    womBalance_lt?: Nullable<BigDecimal>;
    womBalance_gte?: Nullable<BigDecimal>;
    womBalance_lte?: Nullable<BigDecimal>;
    womBalance_in?: Nullable<BigDecimal[]>;
    womBalance_not_in?: Nullable<BigDecimal[]>;
    veWomBalance?: Nullable<BigDecimal>;
    veWomBalance_not?: Nullable<BigDecimal>;
    veWomBalance_gt?: Nullable<BigDecimal>;
    veWomBalance_lt?: Nullable<BigDecimal>;
    veWomBalance_gte?: Nullable<BigDecimal>;
    veWomBalance_lte?: Nullable<BigDecimal>;
    veWomBalance_in?: Nullable<BigDecimal[]>;
    veWomBalance_not_in?: Nullable<BigDecimal[]>;
    lockCount?: Nullable<BigInt>;
    lockCount_not?: Nullable<BigInt>;
    lockCount_gt?: Nullable<BigInt>;
    lockCount_lt?: Nullable<BigInt>;
    lockCount_gte?: Nullable<BigInt>;
    lockCount_lte?: Nullable<BigInt>;
    lockCount_in?: Nullable<BigInt[]>;
    lockCount_not_in?: Nullable<BigInt[]>;
    boosted?: Nullable<boolean>;
    boosted_not?: Nullable<boolean>;
    boosted_in?: Nullable<boolean[]>;
    boosted_not_in?: Nullable<boolean[]>;
    totalTvlUSD?: Nullable<BigDecimal>;
    totalTvlUSD_not?: Nullable<BigDecimal>;
    totalTvlUSD_gt?: Nullable<BigDecimal>;
    totalTvlUSD_lt?: Nullable<BigDecimal>;
    totalTvlUSD_gte?: Nullable<BigDecimal>;
    totalTvlUSD_lte?: Nullable<BigDecimal>;
    totalTvlUSD_in?: Nullable<BigDecimal[]>;
    totalTvlUSD_not_in?: Nullable<BigDecimal[]>;
    locks?: Nullable<string[]>;
    locks_not?: Nullable<string[]>;
    locks_contains?: Nullable<string[]>;
    locks_contains_nocase?: Nullable<string[]>;
    locks_not_contains?: Nullable<string[]>;
    locks_not_contains_nocase?: Nullable<string[]>;
    locks_?: Nullable<Lock_filter>;
    lp?: Nullable<string[]>;
    lp_not?: Nullable<string[]>;
    lp_contains?: Nullable<string[]>;
    lp_contains_nocase?: Nullable<string[]>;
    lp_not_contains?: Nullable<string[]>;
    lp_not_contains_nocase?: Nullable<string[]>;
    lp_?: Nullable<LiquidityPosition_filter>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<User_filter>[]>;
    or?: Nullable<Nullable<User_filter>[]>;
}

export class VeWom_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    currentTotalSupply?: Nullable<BigDecimal>;
    currentTotalSupply_not?: Nullable<BigDecimal>;
    currentTotalSupply_gt?: Nullable<BigDecimal>;
    currentTotalSupply_lt?: Nullable<BigDecimal>;
    currentTotalSupply_gte?: Nullable<BigDecimal>;
    currentTotalSupply_lte?: Nullable<BigDecimal>;
    currentTotalSupply_in?: Nullable<BigDecimal[]>;
    currentTotalSupply_not_in?: Nullable<BigDecimal[]>;
    currentTotalWomLocked?: Nullable<BigDecimal>;
    currentTotalWomLocked_not?: Nullable<BigDecimal>;
    currentTotalWomLocked_gt?: Nullable<BigDecimal>;
    currentTotalWomLocked_lt?: Nullable<BigDecimal>;
    currentTotalWomLocked_gte?: Nullable<BigDecimal>;
    currentTotalWomLocked_lte?: Nullable<BigDecimal>;
    currentTotalWomLocked_in?: Nullable<BigDecimal[]>;
    currentTotalWomLocked_not_in?: Nullable<BigDecimal[]>;
    cumulativeTotalWomLocked?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_not?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_gt?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_lt?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_gte?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_lte?: Nullable<BigDecimal>;
    cumulativeTotalWomLocked_in?: Nullable<BigDecimal[]>;
    cumulativeTotalWomLocked_not_in?: Nullable<BigDecimal[]>;
    cumulativeTotalVeWomMinted?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_not?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_gt?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_lt?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_gte?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_lte?: Nullable<BigDecimal>;
    cumulativeTotalVeWomMinted_in?: Nullable<BigDecimal[]>;
    cumulativeTotalVeWomMinted_not_in?: Nullable<BigDecimal[]>;
    cumulativeTotalLockedTime?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_not?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_gt?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_lt?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_gte?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_lte?: Nullable<BigDecimal>;
    cumulativeTotalLockedTime_in?: Nullable<BigDecimal[]>;
    cumulativeTotalLockedTime_not_in?: Nullable<BigDecimal[]>;
    lockCount?: Nullable<BigInt>;
    lockCount_not?: Nullable<BigInt>;
    lockCount_gt?: Nullable<BigInt>;
    lockCount_lt?: Nullable<BigInt>;
    lockCount_gte?: Nullable<BigInt>;
    lockCount_lte?: Nullable<BigInt>;
    lockCount_in?: Nullable<BigInt[]>;
    lockCount_not_in?: Nullable<BigInt[]>;
    userCount?: Nullable<BigInt>;
    userCount_not?: Nullable<BigInt>;
    userCount_gt?: Nullable<BigInt>;
    userCount_lt?: Nullable<BigInt>;
    userCount_gte?: Nullable<BigInt>;
    userCount_lte?: Nullable<BigInt>;
    userCount_in?: Nullable<BigInt[]>;
    userCount_not_in?: Nullable<BigInt[]>;
    avgWomLocked?: Nullable<BigDecimal>;
    avgWomLocked_not?: Nullable<BigDecimal>;
    avgWomLocked_gt?: Nullable<BigDecimal>;
    avgWomLocked_lt?: Nullable<BigDecimal>;
    avgWomLocked_gte?: Nullable<BigDecimal>;
    avgWomLocked_lte?: Nullable<BigDecimal>;
    avgWomLocked_in?: Nullable<BigDecimal[]>;
    avgWomLocked_not_in?: Nullable<BigDecimal[]>;
    avgVeWomReward?: Nullable<BigDecimal>;
    avgVeWomReward_not?: Nullable<BigDecimal>;
    avgVeWomReward_gt?: Nullable<BigDecimal>;
    avgVeWomReward_lt?: Nullable<BigDecimal>;
    avgVeWomReward_gte?: Nullable<BigDecimal>;
    avgVeWomReward_lte?: Nullable<BigDecimal>;
    avgVeWomReward_in?: Nullable<BigDecimal[]>;
    avgVeWomReward_not_in?: Nullable<BigDecimal[]>;
    avgLockTime?: Nullable<BigDecimal>;
    avgLockTime_not?: Nullable<BigDecimal>;
    avgLockTime_gt?: Nullable<BigDecimal>;
    avgLockTime_lt?: Nullable<BigDecimal>;
    avgLockTime_gte?: Nullable<BigDecimal>;
    avgLockTime_lte?: Nullable<BigDecimal>;
    avgLockTime_in?: Nullable<BigDecimal[]>;
    avgLockTime_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<VeWom_filter>[]>;
    or?: Nullable<Nullable<VeWom_filter>[]>;
}

export class VeWomDayData_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    dayID?: Nullable<BigInt>;
    dayID_not?: Nullable<BigInt>;
    dayID_gt?: Nullable<BigInt>;
    dayID_lt?: Nullable<BigInt>;
    dayID_gte?: Nullable<BigInt>;
    dayID_lte?: Nullable<BigInt>;
    dayID_in?: Nullable<BigInt[]>;
    dayID_not_in?: Nullable<BigInt[]>;
    date?: Nullable<string>;
    date_not?: Nullable<string>;
    date_gt?: Nullable<string>;
    date_lt?: Nullable<string>;
    date_gte?: Nullable<string>;
    date_lte?: Nullable<string>;
    date_in?: Nullable<string[]>;
    date_not_in?: Nullable<string[]>;
    date_contains?: Nullable<string>;
    date_contains_nocase?: Nullable<string>;
    date_not_contains?: Nullable<string>;
    date_not_contains_nocase?: Nullable<string>;
    date_starts_with?: Nullable<string>;
    date_starts_with_nocase?: Nullable<string>;
    date_not_starts_with?: Nullable<string>;
    date_not_starts_with_nocase?: Nullable<string>;
    date_ends_with?: Nullable<string>;
    date_ends_with_nocase?: Nullable<string>;
    date_not_ends_with?: Nullable<string>;
    date_not_ends_with_nocase?: Nullable<string>;
    dailyTotalSupply?: Nullable<BigDecimal>;
    dailyTotalSupply_not?: Nullable<BigDecimal>;
    dailyTotalSupply_gt?: Nullable<BigDecimal>;
    dailyTotalSupply_lt?: Nullable<BigDecimal>;
    dailyTotalSupply_gte?: Nullable<BigDecimal>;
    dailyTotalSupply_lte?: Nullable<BigDecimal>;
    dailyTotalSupply_in?: Nullable<BigDecimal[]>;
    dailyTotalSupply_not_in?: Nullable<BigDecimal[]>;
    dailyMinted?: Nullable<BigDecimal>;
    dailyMinted_not?: Nullable<BigDecimal>;
    dailyMinted_gt?: Nullable<BigDecimal>;
    dailyMinted_lt?: Nullable<BigDecimal>;
    dailyMinted_gte?: Nullable<BigDecimal>;
    dailyMinted_lte?: Nullable<BigDecimal>;
    dailyMinted_in?: Nullable<BigDecimal[]>;
    dailyMinted_not_in?: Nullable<BigDecimal[]>;
    dailyBurned?: Nullable<BigDecimal>;
    dailyBurned_not?: Nullable<BigDecimal>;
    dailyBurned_gt?: Nullable<BigDecimal>;
    dailyBurned_lt?: Nullable<BigDecimal>;
    dailyBurned_gte?: Nullable<BigDecimal>;
    dailyBurned_lte?: Nullable<BigDecimal>;
    dailyBurned_in?: Nullable<BigDecimal[]>;
    dailyBurned_not_in?: Nullable<BigDecimal[]>;
    dailyStaked?: Nullable<BigDecimal>;
    dailyStaked_not?: Nullable<BigDecimal>;
    dailyStaked_gt?: Nullable<BigDecimal>;
    dailyStaked_lt?: Nullable<BigDecimal>;
    dailyStaked_gte?: Nullable<BigDecimal>;
    dailyStaked_lte?: Nullable<BigDecimal>;
    dailyStaked_in?: Nullable<BigDecimal[]>;
    dailyStaked_not_in?: Nullable<BigDecimal[]>;
    dailyUnstaked?: Nullable<BigDecimal>;
    dailyUnstaked_not?: Nullable<BigDecimal>;
    dailyUnstaked_gt?: Nullable<BigDecimal>;
    dailyUnstaked_lt?: Nullable<BigDecimal>;
    dailyUnstaked_gte?: Nullable<BigDecimal>;
    dailyUnstaked_lte?: Nullable<BigDecimal>;
    dailyUnstaked_in?: Nullable<BigDecimal[]>;
    dailyUnstaked_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<VeWomDayData_filter>[]>;
    or?: Nullable<Nullable<VeWomDayData_filter>[]>;
}

export class Voter_filter {
    id?: Nullable<Bytes>;
    id_not?: Nullable<Bytes>;
    id_gt?: Nullable<Bytes>;
    id_lt?: Nullable<Bytes>;
    id_gte?: Nullable<Bytes>;
    id_lte?: Nullable<Bytes>;
    id_in?: Nullable<Bytes[]>;
    id_not_in?: Nullable<Bytes[]>;
    id_contains?: Nullable<Bytes>;
    id_not_contains?: Nullable<Bytes>;
    totalAllocPoint?: Nullable<BigDecimal>;
    totalAllocPoint_not?: Nullable<BigDecimal>;
    totalAllocPoint_gt?: Nullable<BigDecimal>;
    totalAllocPoint_lt?: Nullable<BigDecimal>;
    totalAllocPoint_gte?: Nullable<BigDecimal>;
    totalAllocPoint_lte?: Nullable<BigDecimal>;
    totalAllocPoint_in?: Nullable<BigDecimal[]>;
    totalAllocPoint_not_in?: Nullable<BigDecimal[]>;
    totalWeight?: Nullable<BigDecimal>;
    totalWeight_not?: Nullable<BigDecimal>;
    totalWeight_gt?: Nullable<BigDecimal>;
    totalWeight_lt?: Nullable<BigDecimal>;
    totalWeight_gte?: Nullable<BigDecimal>;
    totalWeight_lte?: Nullable<BigDecimal>;
    totalWeight_in?: Nullable<BigDecimal[]>;
    totalWeight_not_in?: Nullable<BigDecimal[]>;
    basePartition?: Nullable<number>;
    basePartition_not?: Nullable<number>;
    basePartition_gt?: Nullable<number>;
    basePartition_lt?: Nullable<number>;
    basePartition_gte?: Nullable<number>;
    basePartition_lte?: Nullable<number>;
    basePartition_in?: Nullable<number[]>;
    basePartition_not_in?: Nullable<number[]>;
    womPerSec?: Nullable<BigDecimal>;
    womPerSec_not?: Nullable<BigDecimal>;
    womPerSec_gt?: Nullable<BigDecimal>;
    womPerSec_lt?: Nullable<BigDecimal>;
    womPerSec_gte?: Nullable<BigDecimal>;
    womPerSec_lte?: Nullable<BigDecimal>;
    womPerSec_in?: Nullable<BigDecimal[]>;
    womPerSec_not_in?: Nullable<BigDecimal[]>;
    lastUpdate?: Nullable<BigInt>;
    lastUpdate_not?: Nullable<BigInt>;
    lastUpdate_gt?: Nullable<BigInt>;
    lastUpdate_lt?: Nullable<BigInt>;
    lastUpdate_gte?: Nullable<BigInt>;
    lastUpdate_lte?: Nullable<BigInt>;
    lastUpdate_in?: Nullable<BigInt[]>;
    lastUpdate_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Voter_filter>[]>;
    or?: Nullable<Nullable<Voter_filter>[]>;
}

export class Withdraw_filter {
    id?: Nullable<string>;
    id_not?: Nullable<string>;
    id_gt?: Nullable<string>;
    id_lt?: Nullable<string>;
    id_gte?: Nullable<string>;
    id_lte?: Nullable<string>;
    id_in?: Nullable<string[]>;
    id_not_in?: Nullable<string[]>;
    from?: Nullable<Bytes>;
    from_not?: Nullable<Bytes>;
    from_gt?: Nullable<Bytes>;
    from_lt?: Nullable<Bytes>;
    from_gte?: Nullable<Bytes>;
    from_lte?: Nullable<Bytes>;
    from_in?: Nullable<Bytes[]>;
    from_not_in?: Nullable<Bytes[]>;
    from_contains?: Nullable<Bytes>;
    from_not_contains?: Nullable<Bytes>;
    to?: Nullable<Bytes>;
    to_not?: Nullable<Bytes>;
    to_gt?: Nullable<Bytes>;
    to_lt?: Nullable<Bytes>;
    to_gte?: Nullable<Bytes>;
    to_lte?: Nullable<Bytes>;
    to_in?: Nullable<Bytes[]>;
    to_not_in?: Nullable<Bytes[]>;
    to_contains?: Nullable<Bytes>;
    to_not_contains?: Nullable<Bytes>;
    sentFrom?: Nullable<Bytes>;
    sentFrom_not?: Nullable<Bytes>;
    sentFrom_gt?: Nullable<Bytes>;
    sentFrom_lt?: Nullable<Bytes>;
    sentFrom_gte?: Nullable<Bytes>;
    sentFrom_lte?: Nullable<Bytes>;
    sentFrom_in?: Nullable<Bytes[]>;
    sentFrom_not_in?: Nullable<Bytes[]>;
    sentFrom_contains?: Nullable<Bytes>;
    sentFrom_not_contains?: Nullable<Bytes>;
    sentTo?: Nullable<Bytes>;
    sentTo_not?: Nullable<Bytes>;
    sentTo_gt?: Nullable<Bytes>;
    sentTo_lt?: Nullable<Bytes>;
    sentTo_gte?: Nullable<Bytes>;
    sentTo_lte?: Nullable<Bytes>;
    sentTo_in?: Nullable<Bytes[]>;
    sentTo_not_in?: Nullable<Bytes[]>;
    sentTo_contains?: Nullable<Bytes>;
    sentTo_not_contains?: Nullable<Bytes>;
    token?: Nullable<string>;
    token_not?: Nullable<string>;
    token_gt?: Nullable<string>;
    token_lt?: Nullable<string>;
    token_gte?: Nullable<string>;
    token_lte?: Nullable<string>;
    token_in?: Nullable<string[]>;
    token_not_in?: Nullable<string[]>;
    token_contains?: Nullable<string>;
    token_contains_nocase?: Nullable<string>;
    token_not_contains?: Nullable<string>;
    token_not_contains_nocase?: Nullable<string>;
    token_starts_with?: Nullable<string>;
    token_starts_with_nocase?: Nullable<string>;
    token_not_starts_with?: Nullable<string>;
    token_not_starts_with_nocase?: Nullable<string>;
    token_ends_with?: Nullable<string>;
    token_ends_with_nocase?: Nullable<string>;
    token_not_ends_with?: Nullable<string>;
    token_not_ends_with_nocase?: Nullable<string>;
    token_?: Nullable<Token_filter>;
    asset?: Nullable<string>;
    asset_not?: Nullable<string>;
    asset_gt?: Nullable<string>;
    asset_lt?: Nullable<string>;
    asset_gte?: Nullable<string>;
    asset_lte?: Nullable<string>;
    asset_in?: Nullable<string[]>;
    asset_not_in?: Nullable<string[]>;
    asset_contains?: Nullable<string>;
    asset_contains_nocase?: Nullable<string>;
    asset_not_contains?: Nullable<string>;
    asset_not_contains_nocase?: Nullable<string>;
    asset_starts_with?: Nullable<string>;
    asset_starts_with_nocase?: Nullable<string>;
    asset_not_starts_with?: Nullable<string>;
    asset_not_starts_with_nocase?: Nullable<string>;
    asset_ends_with?: Nullable<string>;
    asset_ends_with_nocase?: Nullable<string>;
    asset_not_ends_with?: Nullable<string>;
    asset_not_ends_with_nocase?: Nullable<string>;
    asset_?: Nullable<Asset_filter>;
    amount?: Nullable<BigDecimal>;
    amount_not?: Nullable<BigDecimal>;
    amount_gt?: Nullable<BigDecimal>;
    amount_lt?: Nullable<BigDecimal>;
    amount_gte?: Nullable<BigDecimal>;
    amount_lte?: Nullable<BigDecimal>;
    amount_in?: Nullable<BigDecimal[]>;
    amount_not_in?: Nullable<BigDecimal[]>;
    amountUSD?: Nullable<BigDecimal>;
    amountUSD_not?: Nullable<BigDecimal>;
    amountUSD_gt?: Nullable<BigDecimal>;
    amountUSD_lt?: Nullable<BigDecimal>;
    amountUSD_gte?: Nullable<BigDecimal>;
    amountUSD_lte?: Nullable<BigDecimal>;
    amountUSD_in?: Nullable<BigDecimal[]>;
    amountUSD_not_in?: Nullable<BigDecimal[]>;
    liquidity?: Nullable<BigDecimal>;
    liquidity_not?: Nullable<BigDecimal>;
    liquidity_gt?: Nullable<BigDecimal>;
    liquidity_lt?: Nullable<BigDecimal>;
    liquidity_gte?: Nullable<BigDecimal>;
    liquidity_lte?: Nullable<BigDecimal>;
    liquidity_in?: Nullable<BigDecimal[]>;
    liquidity_not_in?: Nullable<BigDecimal[]>;
    timestamp?: Nullable<BigInt>;
    timestamp_not?: Nullable<BigInt>;
    timestamp_gt?: Nullable<BigInt>;
    timestamp_lt?: Nullable<BigInt>;
    timestamp_gte?: Nullable<BigInt>;
    timestamp_lte?: Nullable<BigInt>;
    timestamp_in?: Nullable<BigInt[]>;
    timestamp_not_in?: Nullable<BigInt[]>;
    blockNumber?: Nullable<BigInt>;
    blockNumber_not?: Nullable<BigInt>;
    blockNumber_gt?: Nullable<BigInt>;
    blockNumber_lt?: Nullable<BigInt>;
    blockNumber_gte?: Nullable<BigInt>;
    blockNumber_lte?: Nullable<BigInt>;
    blockNumber_in?: Nullable<BigInt[]>;
    blockNumber_not_in?: Nullable<BigInt[]>;
    _change_block?: Nullable<BlockChangedFilter>;
    and?: Nullable<Nullable<Withdraw_filter>[]>;
    or?: Nullable<Nullable<Withdraw_filter>[]>;
}

export abstract class IQuery {
    __typename?: 'IQuery';

    abstract cats(): Nullable<Nullable<Cat>[]> | Promise<Nullable<Nullable<Cat>[]>>;

    abstract cat(id: string): Nullable<Cat> | Promise<Nullable<Cat>>;

    abstract block(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Block> | Promise<Nullable<Block>>;

    abstract blocks(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Block_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Block_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Block[] | Promise<Block[]>;

    abstract _meta(block?: Nullable<Block_height>): Nullable<_Meta_> | Promise<Nullable<_Meta_>>;

    abstract protocol(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Protocol> | Promise<Nullable<Protocol>>;

    abstract protocols(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Protocol_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Protocol_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Protocol[] | Promise<Protocol[]>;

    abstract pool(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Pool> | Promise<Nullable<Pool>>;

    abstract pools(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Pool_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Pool_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Pool[] | Promise<Pool[]>;

    abstract token(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Token> | Promise<Nullable<Token>>;

    abstract tokens(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Token_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Token_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Token[] | Promise<Token[]>;

    abstract asset(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Asset> | Promise<Nullable<Asset>>;

    abstract assets(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Asset_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Asset_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Asset[] | Promise<Asset[]>;

    abstract deposit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Deposit> | Promise<Nullable<Deposit>>;

    abstract deposits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Deposit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Deposit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Deposit[] | Promise<Deposit[]>;

    abstract withdraw(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Withdraw> | Promise<Nullable<Withdraw>>;

    abstract withdraws(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Withdraw_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Withdraw_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Withdraw[] | Promise<Withdraw[]>;

    abstract swap(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Swap> | Promise<Nullable<Swap>>;

    abstract swaps(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Swap_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Swap_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Swap[] | Promise<Swap[]>;

    abstract swapTokensForCredit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<SwapTokensForCredit> | Promise<Nullable<SwapTokensForCredit>>;

    abstract swapTokensForCredits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<SwapTokensForCredit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<SwapTokensForCredit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): SwapTokensForCredit[] | Promise<SwapTokensForCredit[]>;

    abstract swapCreditForTokens(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<SwapCreditForTokens_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<SwapCreditForTokens_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): SwapCreditForTokens[] | Promise<SwapCreditForTokens[]>;

    abstract assetDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<AssetDayData> | Promise<Nullable<AssetDayData>>;

    abstract assetDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<AssetDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<AssetDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): AssetDayData[] | Promise<AssetDayData[]>;

    abstract protocolDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<ProtocolDayData> | Promise<Nullable<ProtocolDayData>>;

    abstract protocolDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<ProtocolDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<ProtocolDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): ProtocolDayData[] | Promise<ProtocolDayData[]>;

    abstract user(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<User> | Promise<Nullable<User>>;

    abstract users(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<User_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<User_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): User[] | Promise<User[]>;

    abstract masterWombat(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MasterWombat> | Promise<Nullable<MasterWombat>>;

    abstract masterWombats(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MasterWombat_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MasterWombat_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MasterWombat[] | Promise<MasterWombat[]>;

    abstract liquidityPosition(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<LiquidityPosition> | Promise<Nullable<LiquidityPosition>>;

    abstract liquidityPositions(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<LiquidityPosition_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<LiquidityPosition_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): LiquidityPosition[] | Promise<LiquidityPosition[]>;

    abstract bribe(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Bribe> | Promise<Nullable<Bribe>>;

    abstract bribes(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Bribe_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Bribe_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Bribe[] | Promise<Bribe[]>;

    abstract bribeInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BribeInfo> | Promise<Nullable<BribeInfo>>;

    abstract bribeInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BribeInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BribeInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BribeInfo[] | Promise<BribeInfo[]>;

    abstract voter(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Voter> | Promise<Nullable<Voter>>;

    abstract voters(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Voter_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Voter_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Voter[] | Promise<Voter[]>;

    abstract rewarder(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Rewarder> | Promise<Nullable<Rewarder>>;

    abstract rewarders(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Rewarder_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Rewarder_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Rewarder[] | Promise<Rewarder[]>;

    abstract rewardInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<RewardInfo> | Promise<Nullable<RewardInfo>>;

    abstract rewardInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<RewardInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<RewardInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): RewardInfo[] | Promise<RewardInfo[]>;

    abstract boostedRewarder(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BoostedRewarder> | Promise<Nullable<BoostedRewarder>>;

    abstract boostedRewarders(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BoostedRewarder_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BoostedRewarder_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BoostedRewarder[] | Promise<BoostedRewarder[]>;

    abstract boostedRewardInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BoostedRewardInfo> | Promise<Nullable<BoostedRewardInfo>>;

    abstract boostedRewardInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BoostedRewardInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BoostedRewardInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BoostedRewardInfo[] | Promise<BoostedRewardInfo[]>;

    abstract veWom(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<VeWom> | Promise<Nullable<VeWom>>;

    abstract veWoms(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<VeWom_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<VeWom_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): VeWom[] | Promise<VeWom[]>;

    abstract veWomDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<VeWomDayData> | Promise<Nullable<VeWomDayData>>;

    abstract veWomDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<VeWomDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<VeWomDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): VeWomDayData[] | Promise<VeWomDayData[]>;

    abstract mint(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Mint> | Promise<Nullable<Mint>>;

    abstract mints(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Mint_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Mint_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Mint[] | Promise<Mint[]>;

    abstract burn(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Burn> | Promise<Nullable<Burn>>;

    abstract burns(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Burn_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Burn_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Burn[] | Promise<Burn[]>;

    abstract enter(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Enter> | Promise<Nullable<Enter>>;

    abstract enters(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Enter_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Enter_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Enter[] | Promise<Enter[]>;

    abstract exit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Exit> | Promise<Nullable<Exit>>;

    abstract exits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Exit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Exit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Exit[] | Promise<Exit[]>;

    abstract lock(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Lock> | Promise<Nullable<Lock>>;

    abstract locks(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Lock_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Lock_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Lock[] | Promise<Lock[]>;

    abstract pair(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Pair> | Promise<Nullable<Pair>>;

    abstract pairs(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Pair_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Pair_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Pair[] | Promise<Pair[]>;

    abstract ticker(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Ticker> | Promise<Nullable<Ticker>>;

    abstract tickers(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Ticker_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Ticker_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Ticker[] | Promise<Ticker[]>;

    abstract pairSwap(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<PairSwap> | Promise<Nullable<PairSwap>>;

    abstract pairSwaps(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<PairSwap_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<PairSwap_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): PairSwap[] | Promise<PairSwap[]>;

    abstract mwDeposit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwDeposit> | Promise<Nullable<MwDeposit>>;

    abstract mwDeposits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwDeposit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwDeposit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwDeposit[] | Promise<MwDeposit[]>;

    abstract mwDepositFor(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwDepositFor> | Promise<Nullable<MwDepositFor>>;

    abstract mwDepositFors(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwDepositFor_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwDepositFor_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwDepositFor[] | Promise<MwDepositFor[]>;

    abstract mwWithdraw(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwWithdraw> | Promise<Nullable<MwWithdraw>>;

    abstract mwWithdraws(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwWithdraw_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwWithdraw_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwWithdraw[] | Promise<MwWithdraw[]>;

    abstract mwHarvest(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwHarvest> | Promise<Nullable<MwHarvest>>;

    abstract mwHarvests(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwHarvest_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwHarvest_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwHarvest[] | Promise<MwHarvest[]>;

    abstract timer(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Timer> | Promise<Nullable<Timer>>;

    abstract timers(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Timer_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Timer_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Timer[] | Promise<Timer[]>;
}

export abstract class IMutation {
    __typename?: 'IMutation';

    abstract createCat(createCatInput?: Nullable<CreateCatInput>): Nullable<Cat> | Promise<Nullable<Cat>>;
}

export abstract class ISubscription {
    __typename?: 'ISubscription';

    abstract catCreated(): Nullable<Cat> | Promise<Nullable<Cat>>;

    abstract block(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Block> | Promise<Nullable<Block>>;

    abstract blocks(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Block_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Block_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Block[] | Promise<Block[]>;

    abstract _meta(block?: Nullable<Block_height>): Nullable<_Meta_> | Promise<Nullable<_Meta_>>;

    abstract protocol(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Protocol> | Promise<Nullable<Protocol>>;

    abstract protocols(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Protocol_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Protocol_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Protocol[] | Promise<Protocol[]>;

    abstract pool(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Pool> | Promise<Nullable<Pool>>;

    abstract pools(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Pool_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Pool_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Pool[] | Promise<Pool[]>;

    abstract token(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Token> | Promise<Nullable<Token>>;

    abstract tokens(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Token_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Token_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Token[] | Promise<Token[]>;

    abstract asset(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Asset> | Promise<Nullable<Asset>>;

    abstract assets(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Asset_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Asset_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Asset[] | Promise<Asset[]>;

    abstract deposit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Deposit> | Promise<Nullable<Deposit>>;

    abstract deposits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Deposit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Deposit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Deposit[] | Promise<Deposit[]>;

    abstract withdraw(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Withdraw> | Promise<Nullable<Withdraw>>;

    abstract withdraws(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Withdraw_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Withdraw_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Withdraw[] | Promise<Withdraw[]>;

    abstract swap(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Swap> | Promise<Nullable<Swap>>;

    abstract swaps(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Swap_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Swap_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Swap[] | Promise<Swap[]>;

    abstract swapTokensForCredit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<SwapTokensForCredit> | Promise<Nullable<SwapTokensForCredit>>;

    abstract swapTokensForCredits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<SwapTokensForCredit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<SwapTokensForCredit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): SwapTokensForCredit[] | Promise<SwapTokensForCredit[]>;

    abstract swapCreditForTokens(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<SwapCreditForTokens_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<SwapCreditForTokens_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): SwapCreditForTokens[] | Promise<SwapCreditForTokens[]>;

    abstract assetDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<AssetDayData> | Promise<Nullable<AssetDayData>>;

    abstract assetDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<AssetDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<AssetDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): AssetDayData[] | Promise<AssetDayData[]>;

    abstract protocolDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<ProtocolDayData> | Promise<Nullable<ProtocolDayData>>;

    abstract protocolDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<ProtocolDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<ProtocolDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): ProtocolDayData[] | Promise<ProtocolDayData[]>;

    abstract user(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<User> | Promise<Nullable<User>>;

    abstract users(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<User_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<User_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): User[] | Promise<User[]>;

    abstract masterWombat(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MasterWombat> | Promise<Nullable<MasterWombat>>;

    abstract masterWombats(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MasterWombat_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MasterWombat_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MasterWombat[] | Promise<MasterWombat[]>;

    abstract liquidityPosition(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<LiquidityPosition> | Promise<Nullable<LiquidityPosition>>;

    abstract liquidityPositions(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<LiquidityPosition_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<LiquidityPosition_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): LiquidityPosition[] | Promise<LiquidityPosition[]>;

    abstract bribe(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Bribe> | Promise<Nullable<Bribe>>;

    abstract bribes(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Bribe_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Bribe_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Bribe[] | Promise<Bribe[]>;

    abstract bribeInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BribeInfo> | Promise<Nullable<BribeInfo>>;

    abstract bribeInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BribeInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BribeInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BribeInfo[] | Promise<BribeInfo[]>;

    abstract voter(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Voter> | Promise<Nullable<Voter>>;

    abstract voters(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Voter_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Voter_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Voter[] | Promise<Voter[]>;

    abstract rewarder(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Rewarder> | Promise<Nullable<Rewarder>>;

    abstract rewarders(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Rewarder_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Rewarder_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Rewarder[] | Promise<Rewarder[]>;

    abstract rewardInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<RewardInfo> | Promise<Nullable<RewardInfo>>;

    abstract rewardInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<RewardInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<RewardInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): RewardInfo[] | Promise<RewardInfo[]>;

    abstract boostedRewarder(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BoostedRewarder> | Promise<Nullable<BoostedRewarder>>;

    abstract boostedRewarders(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BoostedRewarder_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BoostedRewarder_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BoostedRewarder[] | Promise<BoostedRewarder[]>;

    abstract boostedRewardInfo(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<BoostedRewardInfo> | Promise<Nullable<BoostedRewardInfo>>;

    abstract boostedRewardInfos(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<BoostedRewardInfo_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<BoostedRewardInfo_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): BoostedRewardInfo[] | Promise<BoostedRewardInfo[]>;

    abstract veWom(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<VeWom> | Promise<Nullable<VeWom>>;

    abstract veWoms(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<VeWom_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<VeWom_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): VeWom[] | Promise<VeWom[]>;

    abstract veWomDayData(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<VeWomDayData> | Promise<Nullable<VeWomDayData>>;

    abstract veWomDayDatas(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<VeWomDayData_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<VeWomDayData_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): VeWomDayData[] | Promise<VeWomDayData[]>;

    abstract mint(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Mint> | Promise<Nullable<Mint>>;

    abstract mints(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Mint_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Mint_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Mint[] | Promise<Mint[]>;

    abstract burn(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Burn> | Promise<Nullable<Burn>>;

    abstract burns(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Burn_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Burn_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Burn[] | Promise<Burn[]>;

    abstract enter(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Enter> | Promise<Nullable<Enter>>;

    abstract enters(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Enter_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Enter_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Enter[] | Promise<Enter[]>;

    abstract exit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Exit> | Promise<Nullable<Exit>>;

    abstract exits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Exit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Exit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Exit[] | Promise<Exit[]>;

    abstract lock(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Lock> | Promise<Nullable<Lock>>;

    abstract locks(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Lock_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Lock_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Lock[] | Promise<Lock[]>;

    abstract pair(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Pair> | Promise<Nullable<Pair>>;

    abstract pairs(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Pair_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Pair_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Pair[] | Promise<Pair[]>;

    abstract ticker(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Ticker> | Promise<Nullable<Ticker>>;

    abstract tickers(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Ticker_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Ticker_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Ticker[] | Promise<Ticker[]>;

    abstract pairSwap(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<PairSwap> | Promise<Nullable<PairSwap>>;

    abstract pairSwaps(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<PairSwap_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<PairSwap_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): PairSwap[] | Promise<PairSwap[]>;

    abstract mwDeposit(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwDeposit> | Promise<Nullable<MwDeposit>>;

    abstract mwDeposits(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwDeposit_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwDeposit_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwDeposit[] | Promise<MwDeposit[]>;

    abstract mwDepositFor(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwDepositFor> | Promise<Nullable<MwDepositFor>>;

    abstract mwDepositFors(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwDepositFor_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwDepositFor_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwDepositFor[] | Promise<MwDepositFor[]>;

    abstract mwWithdraw(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwWithdraw> | Promise<Nullable<MwWithdraw>>;

    abstract mwWithdraws(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwWithdraw_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwWithdraw_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwWithdraw[] | Promise<MwWithdraw[]>;

    abstract mwHarvest(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<MwHarvest> | Promise<Nullable<MwHarvest>>;

    abstract mwHarvests(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<MwHarvest_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<MwHarvest_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): MwHarvest[] | Promise<MwHarvest[]>;

    abstract timer(id: string, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Nullable<Timer> | Promise<Nullable<Timer>>;

    abstract timers(skip?: Nullable<number>, first?: Nullable<number>, orderBy?: Nullable<Timer_orderBy>, orderDirection?: Nullable<OrderDirection>, where?: Nullable<Timer_filter>, block?: Nullable<Block_height>, subgraphError?: _SubgraphErrorPolicy_): Timer[] | Promise<Timer[]>;
}

export class Owner {
    __typename?: 'Owner';
    id: number;
    name: string;
    age?: Nullable<number>;
    cats?: Nullable<Cat[]>;
}

export class Cat {
    __typename?: 'Cat';
    id?: Nullable<number>;
    name?: Nullable<string>;
    age?: Nullable<number>;
    owner?: Nullable<Owner>;
}

export class _Block_ {
    __typename?: '_Block_';
    hash?: Nullable<Bytes>;
    number: number;
    timestamp?: Nullable<number>;
}

export class _Meta_ {
    __typename?: '_Meta_';
    block: _Block_;
    deployment: string;
    hasIndexingErrors: boolean;
}

export class Block {
    __typename?: 'Block';
    id: string;
    number: BigInt;
    timestamp: BigInt;
    parentHash?: Nullable<string>;
    author?: Nullable<string>;
    difficulty?: Nullable<BigInt>;
    totalDifficulty?: Nullable<BigInt>;
    gasUsed?: Nullable<BigInt>;
    gasLimit?: Nullable<BigInt>;
    receiptsRoot?: Nullable<string>;
    transactionsRoot?: Nullable<string>;
    stateRoot?: Nullable<string>;
    size?: Nullable<BigInt>;
    unclesHash?: Nullable<string>;
}

export class Asset {
    __typename?: 'Asset';
    id: Bytes;
    name: string;
    symbol: string;
    underlyingToken: Token;
    cash: BigDecimal;
    cashUSD: BigDecimal;
    liability: BigDecimal;
    liabilityUSD: BigDecimal;
    totalTradeVolume: BigDecimal;
    totalTradeVolumeUSD: BigDecimal;
    totalCollectedFee: BigDecimal;
    totalCollectedFeeUSD: BigDecimal;
    totalSharedFee: BigDecimal;
    totalSharedFeeUSD: BigDecimal;
    coverageRatio: BigDecimal;
    createdTimestamp: BigInt;
    lastUpdate: BigInt;
    poolAddress?: Nullable<string>;
    pool?: Nullable<Pool>;
    masterWombat: MasterWombat;
    pid: BigInt;
    rewardRate?: Nullable<BigDecimal>;
    tvl: BigDecimal;
    tvlUSD: BigDecimal;
    boostedTvl: BigDecimal;
    boostedTvlUSD: BigDecimal;
    boostedRatio?: Nullable<BigDecimal>;
    womBaseApr?: Nullable<BigDecimal>;
    avgBoostedApr?: Nullable<BigDecimal>;
    voter?: Nullable<Voter>;
    voteWeight?: Nullable<BigDecimal>;
    allocPoint?: Nullable<BigDecimal>;
    bribe?: Nullable<Bribe>;
    rewarder?: Nullable<Rewarder>;
    totalBonusTokenApr?: Nullable<BigDecimal>;
    totalBoostedBonusTokenApr?: Nullable<BigDecimal>;
    isPaused?: Nullable<boolean>;
}

export class AssetDayData {
    __typename?: 'AssetDayData';
    id: string;
    asset: Asset;
    dayID: BigInt;
    date: string;
    dailyCash: BigDecimal;
    dailyCashUSD: BigDecimal;
    dailyLiability: BigDecimal;
    dailyLiabilityUSD: BigDecimal;
    dailyTradeVolume: BigDecimal;
    dailyTradeVolumeUSD: BigDecimal;
    dailyCollectedFee: BigDecimal;
    dailyCollectedFeeUSD: BigDecimal;
    dailySharedFee: BigDecimal;
    dailySharedFeeUSD: BigDecimal;
    dailyCovRatioSnapshot: BigDecimal;
    dailyBaseApr: BigDecimal;
    dailyBonusApr: BigDecimal;
    dailyAvgBoostedApr: BigDecimal;
    dailyBoostedTvlUSD: BigDecimal;
    lastUpdate: BigInt;
    blockNumber: BigInt;
}

export class BoostedRewarder {
    __typename?: 'BoostedRewarder';
    id: Bytes;
    asset: Asset;
    rewardInfos?: BoostedRewardInfo[];
    rewardLength: number;
}

export class BoostedRewardInfo {
    __typename?: 'BoostedRewardInfo';
    id: string;
    rewardToken: Token;
    tokenPerSec: BigDecimal;
    apr: BigDecimal;
}

export class Bribe {
    __typename?: 'Bribe';
    id: Bytes;
    lpToken: Asset;
    bribeInfoCount?: Nullable<number>;
    bribeInfos?: Nullable<BribeInfo[]>;
}

export class BribeInfo {
    __typename?: 'BribeInfo';
    id: string;
    rewardToken: Token;
    tokenPerSec: BigDecimal;
    distributedAmount: BigDecimal;
    distributedAmountUSD: BigDecimal;
    lastUpdate?: Nullable<BigInt>;
}

export class Burn {
    __typename?: 'Burn';
    id: string;
    sender: Bytes;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class Deposit {
    __typename?: 'Deposit';
    id: string;
    from: Bytes;
    to?: Nullable<Bytes>;
    sentFrom: Bytes;
    sentTo: Bytes;
    token: Token;
    asset: Asset;
    amount: BigDecimal;
    amountUSD: BigDecimal;
    liquidity: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class Enter {
    __typename?: 'Enter';
    id: string;
    sender: Bytes;
    unlockTime: BigInt;
    veWomAmount: BigDecimal;
    womAmount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class Exit {
    __typename?: 'Exit';
    id: string;
    sender: Bytes;
    unlockTime: BigInt;
    veWomAmount: BigDecimal;
    womAmount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class LiquidityPosition {
    __typename?: 'LiquidityPosition';
    id: string;
    pid: BigInt;
    tvl: BigDecimal;
    tvlUSD: BigDecimal;
    lastUpdate?: Nullable<BigInt>;
}

export class Lock {
    __typename?: 'Lock';
    id: string;
    sender: Bytes;
    enterTime: BigInt;
    unlockTime: BigInt;
    duration: BigInt;
    womAmount: BigDecimal;
    veWomAmount: BigDecimal;
}

export class MasterWombat {
    __typename?: 'MasterWombat';
    id: Bytes;
    voter: Bytes;
    wom: Bytes;
    veWom: Bytes;
}

export class Mint {
    __typename?: 'Mint';
    id: string;
    sender: Bytes;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class MwDeposit {
    __typename?: 'MwDeposit';
    id: string;
    from: Bytes;
    sender: Bytes;
    pid: BigInt;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class MwDepositFor {
    __typename?: 'MwDepositFor';
    id: string;
    from: Bytes;
    sender: Bytes;
    pid: BigInt;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class MwHarvest {
    __typename?: 'MwHarvest';
    id: string;
    from: Bytes;
    sender: Bytes;
    pid: BigInt;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class MwWithdraw {
    __typename?: 'MwWithdraw';
    id: string;
    from: Bytes;
    sender: Bytes;
    pid: BigInt;
    amount: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class Pair {
    __typename?: 'Pair';
    id: string;
    ticker_id: string;
    base: string;
    target: string;
    pool_id: string[];
    createdTimestamp: BigInt;
}

export class PairSwap {
    __typename?: 'PairSwap';
    id: string;
    pair: Pair;
    sender: Bytes;
    base_amount: BigDecimal;
    target_amount: BigDecimal;
    base_price: BigDecimal;
    target_price: BigDecimal;
    timestamp: BigInt;
}

export class Pool {
    __typename?: 'Pool';
    id: Bytes;
    assets?: Nullable<Asset[]>;
    haircute: BigDecimal;
    retentionRatio: BigDecimal;
    lpDividendRatio: BigDecimal;
    lastUpdate: BigInt;
    createdTimestamp: BigInt;
    createdBlock: BigInt;
}

export class Protocol {
    __typename?: 'Protocol';
    id: string;
    version: string;
    network: Network;
    pools?: Nullable<Pool[]>;
    assets?: Nullable<Asset[]>;
    createdTimestamp: BigInt;
    totalTradeVolumeUSD: BigDecimal;
    totalCollectedFeeUSD: BigDecimal;
    totalSharedFeeUSD: BigDecimal;
    totalCashUSD: BigDecimal;
    totalLiabilityUSD: BigDecimal;
    totalTvlUSD: BigDecimal;
    totalBoostedTvlUSD: BigDecimal;
    boostedRatio: BigDecimal;
    totalBribeRevenueUSD: BigDecimal;
    lastUpdate: BigInt;
}

export class ProtocolDayData {
    __typename?: 'ProtocolDayData';
    id: string;
    dayID: BigInt;
    date: string;
    dailyTradeVolumeUSD: BigDecimal;
    dailyCollectedFeeUSD: BigDecimal;
    dailySharedFeeUSD: BigDecimal;
    dailyCashUSD: BigDecimal;
    dailyLiabilityUSD: BigDecimal;
    dailyActiveUser: BigInt;
    dailyBribeRevenueUSD: BigDecimal;
    dailyBribeRevenueUSDSnapshot: BigDecimal;
    userLists: string[];
    lastUpdate: BigInt;
    blockNumber: BigInt;
}

export class Rewarder {
    __typename?: 'Rewarder';
    id: Bytes;
    asset?: Nullable<Asset>;
    rewardInfo?: Nullable<RewardInfo[]>;
    rewardInfoLength: number;
}

export class RewardInfo {
    __typename?: 'RewardInfo';
    id: string;
    rewardToken?: Nullable<Token>;
    tokenPerSec?: Nullable<BigDecimal>;
    apr?: Nullable<BigDecimal>;
}

export class Swap {
    __typename?: 'Swap';
    id: string;
    from: Bytes;
    to?: Nullable<Bytes>;
    sentFrom: Bytes;
    sentTo: Bytes;
    fromToken: Token;
    fromTokenFee?: Nullable<BigDecimal>;
    toToken: Token;
    toTokenFee?: Nullable<BigDecimal>;
    fromAsset: Asset;
    toAsset: Asset;
    fromAmount: BigDecimal;
    fromAmountUSD: BigDecimal;
    toAmount: BigDecimal;
    toAmountUSD: BigDecimal;
    amount: BigDecimal;
    amountUSD: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class SwapCreditForTokens {
    __typename?: 'SwapCreditForTokens';
    id: string;
    from: Bytes;
    to?: Nullable<Bytes>;
    sentTo: Bytes;
    toToken: Token;
    toTokenFee?: Nullable<BigDecimal>;
    toAsset: Asset;
    toAmount: BigDecimal;
    toAmountUSD: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class SwapTokensForCredit {
    __typename?: 'SwapTokensForCredit';
    id: string;
    from: Bytes;
    to?: Nullable<Bytes>;
    sentFrom: Bytes;
    fromToken: Token;
    fromTokenFee?: Nullable<BigDecimal>;
    fromAsset: Asset;
    fromAmount: BigDecimal;
    fromAmountUSD: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export class Ticker {
    __typename?: 'Ticker';
    id: string;
    ticker_id: string;
    base_currency: string;
    target_currency: string;
    base_volume: BigDecimal;
    target_volume: BigDecimal;
    last_price: BigDecimal;
    pool_id: string[];
    last_update: BigInt;
}

export class Timer {
    __typename?: 'Timer';
    id: string;
    currentTimestamp?: Nullable<BigInt>;
    interval?: Nullable<BigInt>;
    lastUpdate?: Nullable<BigInt>;
}

export class Token {
    __typename?: 'Token';
    id: Bytes;
    symbol: string;
    name: string;
    decimals: number;
    price: BigDecimal;
    priceType?: Nullable<PriceType>;
    orcale?: Nullable<string>;
    createdTimestamp: BigInt;
    lastUpdate: BigInt;
}

export class User {
    __typename?: 'User';
    id: Bytes;
    stakedWom: BigDecimal;
    womBalance: BigDecimal;
    veWomBalance: BigDecimal;
    lockCount: BigInt;
    boosted: boolean;
    totalTvlUSD: BigDecimal;
    locks?: Lock[];
    lp?: LiquidityPosition[];
    lastUpdate?: Nullable<BigInt>;
}

export class VeWom {
    __typename?: 'VeWom';
    id: Bytes;
    currentTotalSupply: BigDecimal;
    currentTotalWomLocked: BigDecimal;
    cumulativeTotalWomLocked: BigDecimal;
    cumulativeTotalVeWomMinted: BigDecimal;
    cumulativeTotalLockedTime: BigDecimal;
    lockCount: BigInt;
    userCount: BigInt;
    avgWomLocked: BigDecimal;
    avgVeWomReward: BigDecimal;
    avgLockTime: BigDecimal;
    lastUpdate: BigInt;
}

export class VeWomDayData {
    __typename?: 'VeWomDayData';
    id: string;
    dayID: BigInt;
    date: string;
    dailyTotalSupply: BigDecimal;
    dailyMinted: BigDecimal;
    dailyBurned: BigDecimal;
    dailyStaked: BigDecimal;
    dailyUnstaked: BigDecimal;
    lastUpdate: BigInt;
    blockNumber: BigInt;
}

export class Voter {
    __typename?: 'Voter';
    id: Bytes;
    totalAllocPoint: BigDecimal;
    totalWeight: BigDecimal;
    basePartition: number;
    womPerSec: BigDecimal;
    lastUpdate: BigInt;
}

export class Withdraw {
    __typename?: 'Withdraw';
    id: string;
    from: Bytes;
    to?: Nullable<Bytes>;
    sentFrom: Bytes;
    sentTo: Bytes;
    token: Token;
    asset: Asset;
    amount: BigDecimal;
    amountUSD: BigDecimal;
    liquidity: BigDecimal;
    timestamp: BigInt;
    blockNumber: BigInt;
}

export type BigDecimal = any;
export type BigInt = any;
export type Bytes = any;
export type Int8 = any;
type Nullable<T> = T | null;
