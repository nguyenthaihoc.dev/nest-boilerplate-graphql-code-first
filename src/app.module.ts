import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigurationModule } from '@config/config.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';
import { CRAWLER_MODULES, MODULES } from './modules';
import { ConsoleModule } from 'nestjs-console';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmOptions } from '@config/database.config';
import { LoggingModule } from '@shared/modules/loggers/logger.module';
import { ModelModule } from '@models/model.module';
const configService = new ConfigService();

let IMPORTS = [
  ModelModule,
  LoggingModule,
  ConfigurationModule,
  ConsoleModule,
  ...MODULES,
  TypeOrmModule.forRootAsync(typeOrmOptions),
];

if (configService.get(EEnvKey.IS_RUN_API) === 'true') {
  IMPORTS.push(
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      typePaths: ['./**/*.graphql'],
      playground: new ConfigService().get(EEnvKey.NODE_ENV) !== 'production',
      includeStacktraceInErrorResponses:
        new ConfigService().get(EEnvKey.NODE_ENV) !== 'production',
    }),
  );
} else {
  IMPORTS = [...IMPORTS, ...CRAWLER_MODULES];
}

@Module({
  imports: [...IMPORTS],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
