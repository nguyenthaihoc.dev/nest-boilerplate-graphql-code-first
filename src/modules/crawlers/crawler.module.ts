import { Module } from '@nestjs/common';

import { ProviderConsole } from '@modules/crawlers/provider.console';

@Module({
  controllers: [],
  providers: [ProviderConsole],
  exports: [],
})
export class CrawlerModule {}
