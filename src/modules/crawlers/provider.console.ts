import { Injectable } from '@nestjs/common';
import { Command, Console } from 'nestjs-console';

@Console()
@Injectable()
export class ProviderConsole {
  @Command({
    command: 'provider-job',
    description: 'provider-job',
  })
  async handle(): Promise<void> {
    console.log('Start PROVIDER ');
  }
}
