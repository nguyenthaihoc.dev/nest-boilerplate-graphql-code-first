import { Module } from '@nestjs/common';
import { OwnersModule } from '@modules/example-owners/owners.module';
import { CatOwnerResolver } from './cat-owner.resolver';
import { CatsResolver } from './cats.resolver';
import { CatsService } from './cats.service';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';

const PROVIDERS: Provider[] = [CatsService, CatsResolver, CatOwnerResolver];

@Module({
  imports: [OwnersModule],
  providers: [...PROVIDERS],
})
export class CatsModule {}
