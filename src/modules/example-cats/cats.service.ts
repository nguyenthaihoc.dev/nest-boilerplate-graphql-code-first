import { Injectable } from '@nestjs/common';
import { Cat } from '../../graphql';
import { CatRepository } from '@models/repositories/Cat.repository';

@Injectable()
export class CatsService {
  constructor(private catRepository: CatRepository) {}

  private readonly cats: Array<Cat & { ownerId?: number }> = [
    { id: 1, name: 'Cat', age: 5, ownerId: 1 },
  ];

  async create(cat: Cat): Promise<Cat> {
    cat.id = this.cats.length + 1;
    this.cats.push(cat);
    await this.catRepository.create({
      name: cat.name,
      age: cat.age,
    });
    return cat;
  }

  async findAll(): Promise<Cat[]> {
    console.log(await this.catRepository.findAll());
    return this.cats;
  }

  findOneById(id: number): Cat {
    return this.cats.find((cat) => cat.id === id);
  }
}
