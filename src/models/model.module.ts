import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { entities } from '@models/entities';
import { CatRepository } from '@models/repositories/Cat.repository';

const repositories = [CatRepository];

@Global()
@Module({
  imports: [TypeOrmModule.forFeature(entities)],
  controllers: [],
  providers: [...repositories],
  exports: [...repositories, TypeOrmModule],
})
export class ModelModule {}
