import { InjectRepository } from '@nestjs/typeorm';
import { CatEntity } from '@models/entities/Cat.entity';
import { Repository } from 'typeorm';

export class CatRepository {
  constructor(
    @InjectRepository(CatEntity)
    private merchantRepository: Repository<CatEntity>,
  ) {}

  async findAll(): Promise<CatEntity[]> {
    return this.merchantRepository.find();
  }

  async create(cat: Partial<CatEntity>): Promise<CatEntity> {
    return this.merchantRepository.save(cat);
  }
}
