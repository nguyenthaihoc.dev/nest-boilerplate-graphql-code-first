import * as dotenv from 'dotenv';
import { BootstrapConsole } from 'nestjs-console';

import { AppModule } from './app.module';
import { LoggerService } from '@shared/modules/loggers/logger.service';

dotenv.config();

const loggerService = new LoggerService();
const logger = loggerService.getLogger('console');
logger.info('Start console');
const bootstrap = new BootstrapConsole({
  module: AppModule,
  useDecorators: true,
  contextOptions: {
    logger,
  },
});
bootstrap.init().then(async (app) => {
  try {
    await app.init();
    await bootstrap.boot();
    await app.close();
    process.exit(0);
  } catch (e) {
    console.error(e);
    await app.close();
    process.exit(1);
  }
});
