import { Global, Module } from '@nestjs/common';
import * as GQL from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';

@Global()
@Module({
  imports: [
    GQL.GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      typePaths: ['./**/*.graphql'],
      playground: new ConfigService().get(EEnvKey.NODE_ENV) !== 'production',
      includeStacktraceInErrorResponses:
        new ConfigService().get(EEnvKey.NODE_ENV) !== 'production',
    }),
  ],
  providers: [],
  exports: [],
})
export class GraphModule {}
