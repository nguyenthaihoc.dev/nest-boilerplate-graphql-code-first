import { CatsModule } from '@modules/example-cats/cats.module';
import { OwnersModule } from '@modules/example-owners/owners.module';
import { CrawlerModule } from '@modules/crawlers/crawler.module';
import { Type } from '@nestjs/common/interfaces/type.interface';
import { DynamicModule } from '@nestjs/common/interfaces/modules/dynamic-module.interface';
import { ForwardReference } from '@nestjs/common/interfaces/modules/forward-reference.interface';
type Module = Array<
  Type<any> | DynamicModule | Promise<DynamicModule> | ForwardReference
>;
export const MODULES: Module = [CatsModule, OwnersModule];
export const CRAWLER_MODULES: Module = [CrawlerModule];

export type {};
