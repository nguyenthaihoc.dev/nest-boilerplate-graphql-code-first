FROM node:18 AS builder
WORKDIR /app
COPY ["package.json", "./"]
COPY ["yarn.lock", "./"]
RUN yarn
COPY . .
RUN yarn build


FROM node:18-alpine as runner
RUN apk --no-cache add curl
RUN mkdir -p /app && chown -R node:node /app
WORKDIR /app
USER node
COPY --from=builder /app/dist ./dist
COPY --from=builder ["/app/*.graphql", "./"]
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/.env ./.env