import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

// Note: after gen, change subgraphError: => subgraphError?: for all in file graphql.ts
const definitionsFactory = new GraphQLDefinitionsFactory();
console.log(join(process.cwd(), 'src/graphql.ts'))
definitionsFactory.generate({
    typePaths: ['*.graphql'],
    path: join(process.cwd(), 'src/graphql.ts'),
    outputAs: 'class',
    emitTypenameField: true,
});